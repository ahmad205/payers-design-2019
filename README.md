# Payers
payers is an online bank that enables customers to trade their money easily.

# Getting Started
We will begin by explaining all parts of the website and the technology used in it and the importance of each part.

# Prerequisites

* Ruby version

 we use ruby version 2.5.1 on rails version 5.2.1 .

* Gems

 gem 'rails-i18n' version 5.1.1 to customize and control the locales of the app.

 gem 'rspec-rails' version 3.8.0 to make an effective test in some functions.

 gem 'mailgun-ruby' version 1.1.10 to help us in our api connection with the mailgun service.

 gem 'country_select' version 3.1.1 to make it easy to choose a specific country from all world countries.

 gem 'clearance' Used for Authentication

 gem 'mini_magick'  Used for ActiveStorage variant

 gem 'device_detector' Used to detect devise details

 gem 'maxminddb' Used to read IP details

 gem 'active_model_otp' Used to implement Google's MFA authenticator

 gem 'rqrcode' Used to implement QR Code

 gem 'jwt' Used for Authentice user via token

 gem 'shoulda-matchers' ~> 3.1 Test 

 gem 'factory_bot_rails' ~> 4.0 Test

 gem 'faker' Test

 gem 'database_cleaner' Test

 gem 'whenever' version 0.9.4 to provides a clear syntax for defining cron jobs.
# Main Programming Parts

* Addresses

It contains a list of users addresses and the user must have at least one default address.

Source code documentation link https://payers-card-doc.herokuapp.com/docs/AddressesController

API documentation link https://payers.herokuapp.com/#api-Addresses

* Cards

It contains a list of money cards used by the customer to charge his wallet balance in our website.

Source code documentation link https://payers-card-doc.herokuapp.com/docs/CardsController

API documentation link https://payers.herokuapp.com/#api-Cards

* Cards Categories

This is the front page of cards that appear to the user to choose from the card to be charged.

Source code documentation link https://payers-card-doc.herokuapp.com/docs/CardsCategoriesController

API documentation link https://payers.herokuapp.com/#api-Cards_Categories

* Cards Logs

This part contains all the operations performed on the cards and every card status.

Source code documentation link https://payers-card-doc.herokuapp.com/docs/CardsLogsController

API documentation link https://payers.herokuapp.com/#api-Cards_Logs

* Media

This contain a list of api connections with social media (facebook,twitter,...).

Source code documentation link https://payers-card-doc.herokuapp.com/docs/MediaController

* Money Ops

It contains and control all the money operations that are carried out on the user account (withdraw,transfer,recharge balance).

Source code documentation link https://payers-card-doc.herokuapp.com/docs/MoneyOpsController

API documentation link https://payers.herokuapp.com/#api-Money_Operations

* Notifications

Contains the notifications that appear to the customer as a result of all the transactions and actions taken on his account.

Source code documentation link https://payers-card-doc.herokuapp.com/docs/NotificationsController

API documentation link https://payers.herokuapp.com/#api-Notifications

* Notifications Settings

Make the customer able to control the notifications that appear to him from all sections of the website.

Source code documentation link https://payers-card-doc.herokuapp.com/docs/NotificationsSettingsController

API documentation link https://payers.herokuapp.com/#api-Notifications_Settings

* User Wallets

It contains a list of all users wallets and their balances in our website.

Source code documentation link https://payers-card-doc.herokuapp.com/docs/UserWalletsController

API documentation link https://payers.herokuapp.com/#api-Users_Wallets

* Wallets Transfer Ratios

It control the transfer balance operation between users (min_transfer,max_transfer).

Source code documentation link https://payers-card-doc.herokuapp.com/docs/WalletsTransferRatiosController

* Pages

It contains all the external interface pages and a sitemap to this app.

Source code documentation link https://payers-card-doc.herokuapp.com/docs/PagesController

API documentation link https://payers.herokuapp.com/#api-Pages

* SMS Logs

It contains all SMSs sent to users from our app.

Source code documentation link https://payers-card-doc.herokuapp.com/docs/SmsLogsController

API documentation link https://payers.herokuapp.com/#api-SMS_Logs

* Uploads

Control and edit every picture in the app and save it using active storage.

Source code documentation link https://payers-card-doc.herokuapp.com/docs/UploadsController

* Translations

Control and edit all the locales file this app and save it in our database.

Source code documentation link https://payers-card-doc.herokuapp.com/docs/TranslationsController

API documentation link https://payers.herokuapp.com/#api-Translations

* Sign Up

 Users sign up using Valid email, username and password. 

 API documentation link:  https://payers-userpanel.herokuapp.com/api/index.html#api-User-postUser

 Code documentation Link: http://0.0.0.0:8808/docs/UsersController#create-instance_method

* Confirm user mail after registeration.

 API documentation link:  https://payers-userpanel.herokuapp.com/api/index.html#api-User-POSTconfirmmail

 Code documentation Link: http://0.0.0.0:8808/docs/UsersController#confirmmail-instance_method

* Sign In

 Users can sign in using Valid email and password.

 API documentation link:  https://payers-userpanel.herokuapp.com/api/index.html#api-Authentication-AuthenticateUser

 Code documentation Link: http://0.0.0.0:8808/docs/Clearance/SessionsController#create-instance_method

* confirm sign in via two factor authentication

 Code documentation Link: http://0.0.0.0:8808/docs/UsersController#get_two_factor-instance_method

 Code documentation Link: http://0.0.0.0:8808/docs/UsersController#send_confirmation_email-instance_method

 Code documentation Link: http://0.0.0.0:8808/docs/UsersController#post_two_factor-instance_method

* Send email to Reset Password.

Code documentation Link: http://0.0.0.0:8808/docs/Clearance/PasswordsController#create-instance_method

* Reset User's Password.

Code documentation Link: http://0.0.0.0:8808/docs/Clearance/PasswordsController#update-instance_method

* UnLock User account

 UnLock account after Number of failed attempts.

 API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-Authentication-unlockaccount

 Code documentation Link: http://0.0.0.0:8808/docs/UsersController#unlockaccount-instance_method

* Show user profile 

 API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-User-GetUser

 Code documentation Link: http://0.0.0.0:8808/docs/UsersController#show-instance_method

* Send Invitation Email

 API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-AffilateProgram-postAffilateProgram

* Edit user profile

 API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-User-PutUser

 Code documentation Link: http://0.0.0.0:8808/docs/UsersController#edit-instance_method

 Code documentation Link: http://0.0.0.0:8808/docs/UsersController#update-instance_method

* Choose another two factor authentication 

Code documentation Link: http://0.0.0.0:8808/docs/UsersController#generate_qr_code-instance_method

Code documentation Link: http://0.0.0.0:8808/docs/UsersController#confirm_google_code-instance_method

* Show User Log

 API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-Log-Getuserlog

 Code documentation Link: http://0.0.0.0:8808/docs/WatchdogsController#user_log-instance_method

* Show User's Active Sessions

API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-Log-Getactivesessions

Code documentation Link: http://0.0.0.0:8808/docs/LoginsController#active_sessions-instance_method

* Upload National ID Verification Documents

Code documentation Link: http://0.0.0.0:8808/docs/NationalidVerificationsController#create-instance_method

* Upload Address Verification Documents

Code documentation Link: http://0.0.0.0:8808/docs/AddressVerificationsController#create-instance_method

* Upload Selfie Verification

Code documentation Link: http://0.0.0.0:8808/docs/SelfieVerificationsController#create-instance_method

* Show Country Details

API documentation link: https://payers-userpanel.herokuapp.com/api/index.html#api-Countries-GetCountry

Code documentation Link: http://0.0.0.0:8808/docs/CountriesController#show-instance_method

* List All Countries

API documentation link:  https://payers-userpanel.herokuapp.com/api/index.html#api-Countries-GetCountries

Code documentation Link: http://0.0.0.0:8808/docs/CountriesController#index-instance_method

* Sign Out

Code documentation Link: http://0.0.0.0:8808/docs/Clearance/SessionsController#destroy-instance_method

# Programming Documentation
In this part you will find an explanation of the source code and every function and how it works.

* Visit this link https://payers-card-doc.herokuapp.com/docs

# API Documentation
In this part you will find an explanation of our api connection and how it works.

* Visit this link https://payers.herokuapp.com/

# Test Code
This application also contains a test code in some important functions especially financial functions.

* User Wallets Test

Tests the process of creating wallets and transferring money between users and ensuring that no mistakes are made.

* Pages Test

Tests the process of create and delete pages without any mistake.

* Cards Test

Test the validation of some card parameters (card number must be equal 16 digits and letters).

* (Create,Authenticate,list all) User Test

Tests the process of create , authenticate and get list of all users without any mistake.

* Country Test

Tests the process of create and get list of all countries as well as test display and edit every country details.

# Todo

* Merge between user model and money_ops , translation and uploads models.
