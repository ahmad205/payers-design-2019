  require 'rails_helper'

RSpec.describe 'Authentication', type: :request do
  # Authentication test suite
  describe 'POST /api/v1/auth_user' do
    # Create test country
  let(:country) { Country.create(short_code: 'EG', Phone_code: '057', Full_Name: 'EGYPT', Currency: 'EGP', language: 'Arabic', active: '1') }
  let(:country_id) { country.id }
  # Create test user
  let(:user) { User.create(username: 'Amira' ,password: 'Amira123456', email: 'amira@yahoo.com', status: '1', roleid: '1', active_otp: '1' , country_id: country_id) }
  
    # set headers for authorization
    let(:headers) { valid_headers.except('Authorization') }
    # set test valid and invalid credentials
    let(:valid_credentials) do
      {
        email: user.email,
        password: user.password
      }.to_json
    end
    let(:invalid_credentials) do
      {
        email: 'Faker::Internet.email',
        password: Faker::Internet.password
      }.to_json
    end

    # set request.headers to our custon headers
    # before { allow(request).to receive(:headers).and_return(headers) }

    # returns auth token when request is valid
    context 'When request is valid' do
      before { post '/api/v1/auth_user', params: valid_credentials, headers: headers }

      it 'returns an authentication token' do
        expect(json['auth_token']).not_to be_nil
      end
    end

    # returns failure message when request is invalid
    context 'When request is invalid' do
      before { post '/api/v1/auth_user', params: invalid_credentials, headers: headers }

      it 'returns a failure message' do
        expect(json['message']).to match(/Invalid Email/)
      end
    end
  end
end