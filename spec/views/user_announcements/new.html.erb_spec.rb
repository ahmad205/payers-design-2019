require 'rails_helper'

RSpec.describe "user_announcements/new", type: :view do
  before(:each) do
    assign(:user_announcement, UserAnnouncement.new(
      :user_id => 1,
      :active => 1,
      :slug => "MyString",
      :keyword => "MyString",
      :title => "MyString",
      :description => "MyString"
    ))
  end

  it "renders new user_announcement form" do
    render

    assert_select "form[action=?][method=?]", user_announcements_path, "post" do

      assert_select "input[name=?]", "user_announcement[user_id]"

      assert_select "input[name=?]", "user_announcement[active]"

      assert_select "input[name=?]", "user_announcement[slug]"

      assert_select "input[name=?]", "user_announcement[keyword]"

      assert_select "input[name=?]", "user_announcement[title]"

      assert_select "input[name=?]", "user_announcement[description]"
    end
  end
end
