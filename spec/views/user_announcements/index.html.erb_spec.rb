require 'rails_helper'

RSpec.describe "user_announcements/index", type: :view do
  before(:each) do
    assign(:user_announcements, [
      UserAnnouncement.create!(
        :user_id => 2,
        :active => 3,
        :slug => "Slug",
        :keyword => "Keyword",
        :title => "Title",
        :description => "Description"
      ),
      UserAnnouncement.create!(
        :user_id => 2,
        :active => 3,
        :slug => "Slug",
        :keyword => "Keyword",
        :title => "Title",
        :description => "Description"
      )
    ])
  end

  it "renders a list of user_announcements" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => "Slug".to_s, :count => 2
    assert_select "tr>td", :text => "Keyword".to_s, :count => 2
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
  end
end
