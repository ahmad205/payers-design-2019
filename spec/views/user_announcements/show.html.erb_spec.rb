require 'rails_helper'

RSpec.describe "user_announcements/show", type: :view do
  before(:each) do
    @user_announcement = assign(:user_announcement, UserAnnouncement.create!(
      :user_id => 2,
      :active => 3,
      :slug => "Slug",
      :keyword => "Keyword",
      :title => "Title",
      :description => "Description"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/Slug/)
    expect(rendered).to match(/Keyword/)
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/Description/)
  end
end
