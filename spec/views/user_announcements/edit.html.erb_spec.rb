require 'rails_helper'

RSpec.describe "user_announcements/edit", type: :view do
  before(:each) do
    @user_announcement = assign(:user_announcement, UserAnnouncement.create!(
      :user_id => 1,
      :active => 1,
      :slug => "MyString",
      :keyword => "MyString",
      :title => "MyString",
      :description => "MyString"
    ))
  end

  it "renders the edit user_announcement form" do
    render

    assert_select "form[action=?][method=?]", user_announcement_path(@user_announcement), "post" do

      assert_select "input[name=?]", "user_announcement[user_id]"

      assert_select "input[name=?]", "user_announcement[active]"

      assert_select "input[name=?]", "user_announcement[slug]"

      assert_select "input[name=?]", "user_announcement[keyword]"

      assert_select "input[name=?]", "user_announcement[title]"

      assert_select "input[name=?]", "user_announcement[description]"
    end
  end
end
