require 'rails_helper'

RSpec.describe "audit_logs/index", type: :view do
  before(:each) do
    assign(:audit_logs, [
      AuditLog.create!(
        :user_id => 2,
        :user_name => "User Name",
        :trace_id => "Trace",
        :action_type => "Action Type",
        :action_meta => "Action Meta",
        :ip => "Ip"
      ),
      AuditLog.create!(
        :user_id => 2,
        :user_name => "User Name",
        :trace_id => "Trace",
        :action_type => "Action Type",
        :action_meta => "Action Meta",
        :ip => "Ip"
      )
    ])
  end

  it "renders a list of audit_logs" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "User Name".to_s, :count => 2
    assert_select "tr>td", :text => "Trace".to_s, :count => 2
    assert_select "tr>td", :text => "Action Type".to_s, :count => 2
    assert_select "tr>td", :text => "Action Meta".to_s, :count => 2
    assert_select "tr>td", :text => "Ip".to_s, :count => 2
  end
end
