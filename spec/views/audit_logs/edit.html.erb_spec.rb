require 'rails_helper'

RSpec.describe "audit_logs/edit", type: :view do
  before(:each) do
    @audit_log = assign(:audit_log, AuditLog.create!(
      :user_id => 1,
      :user_name => "MyString",
      :trace_id => "MyString",
      :action_type => "MyString",
      :action_meta => "MyString",
      :ip => "MyString"
    ))
  end

  it "renders the edit audit_log form" do
    render

    assert_select "form[action=?][method=?]", audit_log_path(@audit_log), "post" do

      assert_select "input[name=?]", "audit_log[user_id]"

      assert_select "input[name=?]", "audit_log[user_name]"

      assert_select "input[name=?]", "audit_log[trace_id]"

      assert_select "input[name=?]", "audit_log[action_type]"

      assert_select "input[name=?]", "audit_log[action_meta]"

      assert_select "input[name=?]", "audit_log[ip]"
    end
  end
end
