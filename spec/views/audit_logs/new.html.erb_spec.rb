require 'rails_helper'

RSpec.describe "audit_logs/new", type: :view do
  before(:each) do
    assign(:audit_log, AuditLog.new(
      :user_id => 1,
      :user_name => "MyString",
      :trace_id => "MyString",
      :action_type => "MyString",
      :action_meta => "MyString",
      :ip => "MyString"
    ))
  end

  it "renders new audit_log form" do
    render

    assert_select "form[action=?][method=?]", audit_logs_path, "post" do

      assert_select "input[name=?]", "audit_log[user_id]"

      assert_select "input[name=?]", "audit_log[user_name]"

      assert_select "input[name=?]", "audit_log[trace_id]"

      assert_select "input[name=?]", "audit_log[action_type]"

      assert_select "input[name=?]", "audit_log[action_meta]"

      assert_select "input[name=?]", "audit_log[ip]"
    end
  end
end
