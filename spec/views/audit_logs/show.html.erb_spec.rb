require 'rails_helper'

RSpec.describe "audit_logs/show", type: :view do
  before(:each) do
    @audit_log = assign(:audit_log, AuditLog.create!(
      :user_id => 2,
      :user_name => "User Name",
      :trace_id => "Trace",
      :action_type => "Action Type",
      :action_meta => "Action Meta",
      :ip => "Ip"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/User Name/)
    expect(rendered).to match(/Trace/)
    expect(rendered).to match(/Action Type/)
    expect(rendered).to match(/Action Meta/)
    expect(rendered).to match(/Ip/)
  end
end
