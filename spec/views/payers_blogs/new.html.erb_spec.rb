require 'rails_helper'

RSpec.describe "payers_blogs/new", type: :view do
  before(:each) do
    assign(:payers_blog, PayersBlog.new(
      :author_id => 1,
      :title => "MyString",
      :slug => "MyString",
      :content => "MyString",
      :keywords => "MyString",
      :category_id => 1
    ))
  end

  it "renders new payers_blog form" do
    render

    assert_select "form[action=?][method=?]", payers_blogs_path, "post" do

      assert_select "input[name=?]", "payers_blog[author_id]"

      assert_select "input[name=?]", "payers_blog[title]"

      assert_select "input[name=?]", "payers_blog[slug]"

      assert_select "input[name=?]", "payers_blog[content]"

      assert_select "input[name=?]", "payers_blog[keywords]"

      assert_select "input[name=?]", "payers_blog[category_id]"
    end
  end
end
