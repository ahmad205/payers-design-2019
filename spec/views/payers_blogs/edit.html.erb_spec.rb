require 'rails_helper'

RSpec.describe "payers_blogs/edit", type: :view do
  before(:each) do
    @payers_blog = assign(:payers_blog, PayersBlog.create!(
      :author_id => 1,
      :title => "MyString",
      :slug => "MyString",
      :content => "MyString",
      :keywords => "MyString",
      :category_id => 1
    ))
  end

  it "renders the edit payers_blog form" do
    render

    assert_select "form[action=?][method=?]", payers_blog_path(@payers_blog), "post" do

      assert_select "input[name=?]", "payers_blog[author_id]"

      assert_select "input[name=?]", "payers_blog[title]"

      assert_select "input[name=?]", "payers_blog[slug]"

      assert_select "input[name=?]", "payers_blog[content]"

      assert_select "input[name=?]", "payers_blog[keywords]"

      assert_select "input[name=?]", "payers_blog[category_id]"
    end
  end
end
