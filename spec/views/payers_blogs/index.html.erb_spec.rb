require 'rails_helper'

RSpec.describe "payers_blogs/index", type: :view do
  before(:each) do
    assign(:payers_blogs, [
      PayersBlog.create!(
        :author_id => 2,
        :title => "Title",
        :slug => "Slug",
        :content => "Content",
        :keywords => "Keywords",
        :category_id => 3
      ),
      PayersBlog.create!(
        :author_id => 2,
        :title => "Title",
        :slug => "Slug",
        :content => "Content",
        :keywords => "Keywords",
        :category_id => 3
      )
    ])
  end

  it "renders a list of payers_blogs" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Slug".to_s, :count => 2
    assert_select "tr>td", :text => "Content".to_s, :count => 2
    assert_select "tr>td", :text => "Keywords".to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
  end
end
