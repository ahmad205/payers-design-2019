require 'rails_helper'

RSpec.describe "payers_blogs/show", type: :view do
  before(:each) do
    @payers_blog = assign(:payers_blog, PayersBlog.create!(
      :author_id => 2,
      :title => "Title",
      :slug => "Slug",
      :content => "Content",
      :keywords => "Keywords",
      :category_id => 3
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/Slug/)
    expect(rendered).to match(/Content/)
    expect(rendered).to match(/Keywords/)
    expect(rendered).to match(/3/)
  end
end
