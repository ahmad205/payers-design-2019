require 'rails_helper'

RSpec.describe "errors_logs/index", type: :view do
  before(:each) do
    assign(:errors_logs, [
      ErrorsLog.create!(
        :user_id => 2,
        :user_type => 3,
        :code => "Code",
        :message => "Message"
      ),
      ErrorsLog.create!(
        :user_id => 2,
        :user_type => 3,
        :code => "Code",
        :message => "Message"
      )
    ])
  end

  it "renders a list of errors_logs" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => "Code".to_s, :count => 2
    assert_select "tr>td", :text => "Message".to_s, :count => 2
  end
end
