require 'rails_helper'

RSpec.describe "errors_logs/show", type: :view do
  before(:each) do
    @errors_log = assign(:errors_log, ErrorsLog.create!(
      :user_id => 2,
      :user_type => 3,
      :code => "Code",
      :message => "Message"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/Code/)
    expect(rendered).to match(/Message/)
  end
end
