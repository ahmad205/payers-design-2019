require 'rails_helper'

RSpec.describe "errors_logs/edit", type: :view do
  before(:each) do
    @errors_log = assign(:errors_log, ErrorsLog.create!(
      :user_id => 1,
      :user_type => 1,
      :code => "MyString",
      :message => "MyString"
    ))
  end

  it "renders the edit errors_log form" do
    render

    assert_select "form[action=?][method=?]", errors_log_path(@errors_log), "post" do

      assert_select "input[name=?]", "errors_log[user_id]"

      assert_select "input[name=?]", "errors_log[user_type]"

      assert_select "input[name=?]", "errors_log[code]"

      assert_select "input[name=?]", "errors_log[message]"
    end
  end
end
