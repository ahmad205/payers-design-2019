require 'rails_helper'

RSpec.describe "errors_logs/new", type: :view do
  before(:each) do
    assign(:errors_log, ErrorsLog.new(
      :user_id => 1,
      :user_type => 1,
      :code => "MyString",
      :message => "MyString"
    ))
  end

  it "renders new errors_log form" do
    render

    assert_select "form[action=?][method=?]", errors_logs_path, "post" do

      assert_select "input[name=?]", "errors_log[user_id]"

      assert_select "input[name=?]", "errors_log[user_type]"

      assert_select "input[name=?]", "errors_log[code]"

      assert_select "input[name=?]", "errors_log[message]"
    end
  end
end
