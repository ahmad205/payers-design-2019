require 'rails_helper'

RSpec.describe "banks/index", type: :view do
  before(:each) do
    assign(:banks, [
      Bank.create!(
        :bank_name => "Bank Name",
        :branch_name => "Branch Name",
        :bank_code => "Bank Code",
        :country_id => "Country",
        :is_iban => "Is Iban",
        :verified => "Verified"
      ),
      Bank.create!(
        :bank_name => "Bank Name",
        :branch_name => "Branch Name",
        :bank_code => "Bank Code",
        :country_id => "Country",
        :is_iban => "Is Iban",
        :verified => "Verified"
      )
    ])
  end

  it "renders a list of banks" do
    render
    assert_select "tr>td", :text => "Bank Name".to_s, :count => 2
    assert_select "tr>td", :text => "Branch Name".to_s, :count => 2
    assert_select "tr>td", :text => "Bank Code".to_s, :count => 2
    assert_select "tr>td", :text => "Country".to_s, :count => 2
    assert_select "tr>td", :text => "Is Iban".to_s, :count => 2
    assert_select "tr>td", :text => "Verified".to_s, :count => 2
  end
end
