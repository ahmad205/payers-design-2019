require 'rails_helper'

RSpec.describe "banks/edit", type: :view do
  before(:each) do
    @bank = assign(:bank, Bank.create!(
      :bank_name => "MyString",
      :branch_name => "MyString",
      :bank_code => "MyString",
      :country_id => "MyString",
      :is_iban => "MyString",
      :verified => "MyString"
    ))
  end

  it "renders the edit bank form" do
    render

    assert_select "form[action=?][method=?]", bank_path(@bank), "post" do

      assert_select "input[name=?]", "bank[bank_name]"

      assert_select "input[name=?]", "bank[branch_name]"

      assert_select "input[name=?]", "bank[bank_code]"

      assert_select "input[name=?]", "bank[country_id]"

      assert_select "input[name=?]", "bank[is_iban]"

      assert_select "input[name=?]", "bank[verified]"
    end
  end
end
