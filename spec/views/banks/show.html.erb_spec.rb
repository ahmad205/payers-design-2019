require 'rails_helper'

RSpec.describe "banks/show", type: :view do
  before(:each) do
    @bank = assign(:bank, Bank.create!(
      :bank_name => "Bank Name",
      :branch_name => "Branch Name",
      :bank_code => "Bank Code",
      :country_id => "Country",
      :is_iban => "Is Iban",
      :verified => "Verified"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Bank Name/)
    expect(rendered).to match(/Branch Name/)
    expect(rendered).to match(/Bank Code/)
    expect(rendered).to match(/Country/)
    expect(rendered).to match(/Is Iban/)
    expect(rendered).to match(/Verified/)
  end
end
