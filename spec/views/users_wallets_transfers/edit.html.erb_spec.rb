require 'rails_helper'

RSpec.describe "users_wallets_transfers/edit", type: :view do
  before(:each) do
    @users_wallets_transfer = assign(:users_wallets_transfer, UsersWalletsTransfer.create!(
      :user_id => 1,
      :user_to_id => 1,
      :transfer_method => 1,
      :transfer_type => 1,
      :hold_period => 1,
      :amount => 1.5,
      :ratio => 1.5,
      :approve => 1,
      :note => "MyString"
    ))
  end

  it "renders the edit users_wallets_transfer form" do
    render

    assert_select "form[action=?][method=?]", users_wallets_transfer_path(@users_wallets_transfer), "post" do

      assert_select "input[name=?]", "users_wallets_transfer[user_id]"

      assert_select "input[name=?]", "users_wallets_transfer[user_to_id]"

      assert_select "input[name=?]", "users_wallets_transfer[transfer_method]"

      assert_select "input[name=?]", "users_wallets_transfer[transfer_type]"

      assert_select "input[name=?]", "users_wallets_transfer[hold_period]"

      assert_select "input[name=?]", "users_wallets_transfer[amount]"

      assert_select "input[name=?]", "users_wallets_transfer[ratio]"

      assert_select "input[name=?]", "users_wallets_transfer[approve]"

      assert_select "input[name=?]", "users_wallets_transfer[note]"
    end
  end
end
