require 'rails_helper'

RSpec.describe "users_banks/new", type: :view do
  before(:each) do
    assign(:users_bank, UsersBank.new(
      :bank_id => 1,
      :user_id => 1,
      :account_name => "MyString",
      :account_number => "MyString",
      :active => false,
      :is_iban => false
    ))
  end

  it "renders new users_bank form" do
    render

    assert_select "form[action=?][method=?]", users_banks_path, "post" do

      assert_select "input[name=?]", "users_bank[bank_id]"

      assert_select "input[name=?]", "users_bank[user_id]"

      assert_select "input[name=?]", "users_bank[account_name]"

      assert_select "input[name=?]", "users_bank[account_number]"

      assert_select "input[name=?]", "users_bank[active]"

      assert_select "input[name=?]", "users_bank[is_iban]"
    end
  end
end
