require 'rails_helper'

RSpec.describe "users_banks/show", type: :view do
  before(:each) do
    @users_bank = assign(:users_bank, UsersBank.create!(
      :bank_id => 2,
      :user_id => 3,
      :account_name => "Account Name",
      :account_number => "Account Number",
      :active => false,
      :is_iban => false
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/Account Name/)
    expect(rendered).to match(/Account Number/)
    expect(rendered).to match(/false/)
    expect(rendered).to match(/false/)
  end
end
