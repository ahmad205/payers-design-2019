require 'rails_helper'

RSpec.describe "users_banks/index", type: :view do
  before(:each) do
    assign(:users_banks, [
      UsersBank.create!(
        :bank_id => 2,
        :user_id => 3,
        :account_name => "Account Name",
        :account_number => "Account Number",
        :active => false,
        :is_iban => false
      ),
      UsersBank.create!(
        :bank_id => 2,
        :user_id => 3,
        :account_name => "Account Name",
        :account_number => "Account Number",
        :active => false,
        :is_iban => false
      )
    ])
  end

  it "renders a list of users_banks" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => "Account Name".to_s, :count => 2
    assert_select "tr>td", :text => "Account Number".to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
    assert_select "tr>td", :text => false.to_s, :count => 2
  end
end
