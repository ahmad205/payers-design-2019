require 'rails_helper'

RSpec.describe "blogs_categories/new", type: :view do
  before(:each) do
    assign(:blogs_category, BlogsCategory.new(
      :name => "MyString",
      :description => "MyString"
    ))
  end

  it "renders new blogs_category form" do
    render

    assert_select "form[action=?][method=?]", blogs_categories_path, "post" do

      assert_select "input[name=?]", "blogs_category[name]"

      assert_select "input[name=?]", "blogs_category[description]"
    end
  end
end
