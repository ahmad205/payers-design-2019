require 'rails_helper'

RSpec.describe "blogs_categories/index", type: :view do
  before(:each) do
    assign(:blogs_categories, [
      BlogsCategory.create!(
        :name => "Name",
        :description => "Description"
      ),
      BlogsCategory.create!(
        :name => "Name",
        :description => "Description"
      )
    ])
  end

  it "renders a list of blogs_categories" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
  end
end
