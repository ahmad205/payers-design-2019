require 'rails_helper'

RSpec.describe "blogs_categories/show", type: :view do
  before(:each) do
    @blogs_category = assign(:blogs_category, BlogsCategory.create!(
      :name => "Name",
      :description => "Description"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Description/)
  end
end
