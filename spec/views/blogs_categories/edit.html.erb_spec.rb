require 'rails_helper'

RSpec.describe "blogs_categories/edit", type: :view do
  before(:each) do
    @blogs_category = assign(:blogs_category, BlogsCategory.create!(
      :name => "MyString",
      :description => "MyString"
    ))
  end

  it "renders the edit blogs_category form" do
    render

    assert_select "form[action=?][method=?]", blogs_category_path(@blogs_category), "post" do

      assert_select "input[name=?]", "blogs_category[name]"

      assert_select "input[name=?]", "blogs_category[description]"
    end
  end
end
