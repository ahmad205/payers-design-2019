require 'rails_helper'

RSpec.describe "withdraws/index", type: :view do
  before(:each) do
    assign(:withdraws, [
      Withdraw.create!(
        :user_id => 2,
        :bank_id => 3,
        :operation_id => 4,
        :amount => 5.5,
        :fees => 6.5,
        :status => "Status",
        :withdraw_type => "Withdraw Type",
        :note => "Note"
      ),
      Withdraw.create!(
        :user_id => 2,
        :bank_id => 3,
        :operation_id => 4,
        :amount => 5.5,
        :fees => 6.5,
        :status => "Status",
        :withdraw_type => "Withdraw Type",
        :note => "Note"
      )
    ])
  end

  it "renders a list of withdraws" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => 5.5.to_s, :count => 2
    assert_select "tr>td", :text => 6.5.to_s, :count => 2
    assert_select "tr>td", :text => "Status".to_s, :count => 2
    assert_select "tr>td", :text => "Withdraw Type".to_s, :count => 2
    assert_select "tr>td", :text => "Note".to_s, :count => 2
  end
end
