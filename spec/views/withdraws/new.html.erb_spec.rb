require 'rails_helper'

RSpec.describe "withdraws/new", type: :view do
  before(:each) do
    assign(:withdraw, Withdraw.new(
      :user_id => 1,
      :bank_id => 1,
      :operation_id => 1,
      :amount => 1.5,
      :fees => 1.5,
      :status => "MyString",
      :withdraw_type => "MyString",
      :note => "MyString"
    ))
  end

  it "renders new withdraw form" do
    render

    assert_select "form[action=?][method=?]", withdraws_path, "post" do

      assert_select "input[name=?]", "withdraw[user_id]"

      assert_select "input[name=?]", "withdraw[bank_id]"

      assert_select "input[name=?]", "withdraw[operation_id]"

      assert_select "input[name=?]", "withdraw[amount]"

      assert_select "input[name=?]", "withdraw[fees]"

      assert_select "input[name=?]", "withdraw[status]"

      assert_select "input[name=?]", "withdraw[withdraw_type]"

      assert_select "input[name=?]", "withdraw[note]"
    end
  end
end
