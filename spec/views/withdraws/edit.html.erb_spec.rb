require 'rails_helper'

RSpec.describe "withdraws/edit", type: :view do
  before(:each) do
    @withdraw = assign(:withdraw, Withdraw.create!(
      :user_id => 1,
      :bank_id => 1,
      :operation_id => 1,
      :amount => 1.5,
      :fees => 1.5,
      :status => "MyString",
      :withdraw_type => "MyString",
      :note => "MyString"
    ))
  end

  it "renders the edit withdraw form" do
    render

    assert_select "form[action=?][method=?]", withdraw_path(@withdraw), "post" do

      assert_select "input[name=?]", "withdraw[user_id]"

      assert_select "input[name=?]", "withdraw[bank_id]"

      assert_select "input[name=?]", "withdraw[operation_id]"

      assert_select "input[name=?]", "withdraw[amount]"

      assert_select "input[name=?]", "withdraw[fees]"

      assert_select "input[name=?]", "withdraw[status]"

      assert_select "input[name=?]", "withdraw[withdraw_type]"

      assert_select "input[name=?]", "withdraw[note]"
    end
  end
end
