require 'rails_helper'

RSpec.describe "withdraws/show", type: :view do
  before(:each) do
    @withdraw = assign(:withdraw, Withdraw.create!(
      :user_id => 2,
      :bank_id => 3,
      :operation_id => 4,
      :amount => 5.5,
      :fees => 6.5,
      :status => "Status",
      :withdraw_type => "Withdraw Type",
      :note => "Note"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/4/)
    expect(rendered).to match(/5.5/)
    expect(rendered).to match(/6.5/)
    expect(rendered).to match(/Status/)
    expect(rendered).to match(/Withdraw Type/)
    expect(rendered).to match(/Note/)
  end
end
