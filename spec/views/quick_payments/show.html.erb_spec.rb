require 'rails_helper'

RSpec.describe "quick_payments/show", type: :view do
  before(:each) do
    @quick_payment = assign(:quick_payment, QuickPayment.create!(
      :user_id => 2,
      :active => 3,
      :availability => 4,
      :title => "Title",
      :description => "Description"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/2/)
    expect(rendered).to match(/3/)
    expect(rendered).to match(/4/)
    expect(rendered).to match(/Title/)
    expect(rendered).to match(/Description/)
  end
end
