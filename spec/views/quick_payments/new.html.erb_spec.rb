require 'rails_helper'

RSpec.describe "quick_payments/new", type: :view do
  before(:each) do
    assign(:quick_payment, QuickPayment.new(
      :user_id => 1,
      :active => 1,
      :availability => 1,
      :title => "MyString",
      :description => "MyString"
    ))
  end

  it "renders new quick_payment form" do
    render

    assert_select "form[action=?][method=?]", quick_payments_path, "post" do

      assert_select "input[name=?]", "quick_payment[user_id]"

      assert_select "input[name=?]", "quick_payment[active]"

      assert_select "input[name=?]", "quick_payment[availability]"

      assert_select "input[name=?]", "quick_payment[title]"

      assert_select "input[name=?]", "quick_payment[description]"
    end
  end
end
