require 'rails_helper'

RSpec.describe "quick_payments/edit", type: :view do
  before(:each) do
    @quick_payment = assign(:quick_payment, QuickPayment.create!(
      :user_id => 1,
      :active => 1,
      :availability => 1,
      :title => "MyString",
      :description => "MyString"
    ))
  end

  it "renders the edit quick_payment form" do
    render

    assert_select "form[action=?][method=?]", quick_payment_path(@quick_payment), "post" do

      assert_select "input[name=?]", "quick_payment[user_id]"

      assert_select "input[name=?]", "quick_payment[active]"

      assert_select "input[name=?]", "quick_payment[availability]"

      assert_select "input[name=?]", "quick_payment[title]"

      assert_select "input[name=?]", "quick_payment[description]"
    end
  end
end
