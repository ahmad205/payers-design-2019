require 'rails_helper'

RSpec.describe "quick_payments/index", type: :view do
  before(:each) do
    assign(:quick_payments, [
      QuickPayment.create!(
        :user_id => 2,
        :active => 3,
        :availability => 4,
        :title => "Title",
        :description => "Description"
      ),
      QuickPayment.create!(
        :user_id => 2,
        :active => 3,
        :availability => 4,
        :title => "Title",
        :description => "Description"
      )
    ])
  end

  it "renders a list of quick_payments" do
    render
    assert_select "tr>td", :text => 2.to_s, :count => 2
    assert_select "tr>td", :text => 3.to_s, :count => 2
    assert_select "tr>td", :text => 4.to_s, :count => 2
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Description".to_s, :count => 2
  end
end
