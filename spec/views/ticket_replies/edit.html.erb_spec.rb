require 'rails_helper'

RSpec.describe "ticket_replies/edit", type: :view do
  before(:each) do
    @ticket_reply = assign(:ticket_reply, TicketReply.create!(
      :content => "MyString"
    ))
  end

  it "renders the edit ticket_reply form" do
    render

    assert_select "form[action=?][method=?]", ticket_reply_path(@ticket_reply), "post" do

      assert_select "input[name=?]", "ticket_reply[content]"
    end
  end
end
