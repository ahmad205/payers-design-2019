FactoryBot.define do
    factory :user do
        username { Faker::Lorem.word }
        email 'foo@bar.com'
        password 'Amira12345698r'
        uuid { Faker::Number.number(3) }
        roleid 1
        country_id country_id
    end
end
