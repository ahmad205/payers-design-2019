require "rails_helper"

RSpec.describe QuickPaymentsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/quick_payments").to route_to("quick_payments#index")
    end

    it "routes to #new" do
      expect(:get => "/quick_payments/new").to route_to("quick_payments#new")
    end

    it "routes to #show" do
      expect(:get => "/quick_payments/1").to route_to("quick_payments#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/quick_payments/1/edit").to route_to("quick_payments#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/quick_payments").to route_to("quick_payments#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/quick_payments/1").to route_to("quick_payments#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/quick_payments/1").to route_to("quick_payments#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/quick_payments/1").to route_to("quick_payments#destroy", :id => "1")
    end
  end
end
