require "rails_helper"

RSpec.describe ErrorsLogsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/errors_logs").to route_to("errors_logs#index")
    end

    it "routes to #new" do
      expect(:get => "/errors_logs/new").to route_to("errors_logs#new")
    end

    it "routes to #show" do
      expect(:get => "/errors_logs/1").to route_to("errors_logs#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/errors_logs/1/edit").to route_to("errors_logs#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/errors_logs").to route_to("errors_logs#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/errors_logs/1").to route_to("errors_logs#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/errors_logs/1").to route_to("errors_logs#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/errors_logs/1").to route_to("errors_logs#destroy", :id => "1")
    end
  end
end
