require "rails_helper"

RSpec.describe BlogsCategoriesController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/blogs_categories").to route_to("blogs_categories#index")
    end

    it "routes to #new" do
      expect(:get => "/blogs_categories/new").to route_to("blogs_categories#new")
    end

    it "routes to #show" do
      expect(:get => "/blogs_categories/1").to route_to("blogs_categories#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/blogs_categories/1/edit").to route_to("blogs_categories#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/blogs_categories").to route_to("blogs_categories#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/blogs_categories/1").to route_to("blogs_categories#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/blogs_categories/1").to route_to("blogs_categories#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/blogs_categories/1").to route_to("blogs_categories#destroy", :id => "1")
    end
  end
end
