require "rails_helper"

RSpec.describe PayersBlogsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/payers_blogs").to route_to("payers_blogs#index")
    end

    it "routes to #new" do
      expect(:get => "/payers_blogs/new").to route_to("payers_blogs#new")
    end

    it "routes to #show" do
      expect(:get => "/payers_blogs/1").to route_to("payers_blogs#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/payers_blogs/1/edit").to route_to("payers_blogs#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/payers_blogs").to route_to("payers_blogs#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/payers_blogs/1").to route_to("payers_blogs#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/payers_blogs/1").to route_to("payers_blogs#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/payers_blogs/1").to route_to("payers_blogs#destroy", :id => "1")
    end
  end
end
