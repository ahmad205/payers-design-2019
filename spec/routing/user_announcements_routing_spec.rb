require "rails_helper"

RSpec.describe UserAnnouncementsController, type: :routing do
  describe "routing" do
    it "routes to #index" do
      expect(:get => "/user_announcements").to route_to("user_announcements#index")
    end

    it "routes to #new" do
      expect(:get => "/user_announcements/new").to route_to("user_announcements#new")
    end

    it "routes to #show" do
      expect(:get => "/user_announcements/1").to route_to("user_announcements#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/user_announcements/1/edit").to route_to("user_announcements#edit", :id => "1")
    end


    it "routes to #create" do
      expect(:post => "/user_announcements").to route_to("user_announcements#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/user_announcements/1").to route_to("user_announcements#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/user_announcements/1").to route_to("user_announcements#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/user_announcements/1").to route_to("user_announcements#destroy", :id => "1")
    end
  end
end
