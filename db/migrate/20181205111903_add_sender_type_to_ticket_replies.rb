class AddSenderTypeToTicketReplies < ActiveRecord::Migration[5.2]
  def change
    add_column :ticket_replies, :sender_type, :integer
  end
end
