class CreatePayersBlogs < ActiveRecord::Migration[5.2]
  def change
    create_table :payers_blogs do |t|
      t.integer :author_id
      t.string :title
      t.string :slug
      t.string :content
      t.string :keywords
      t.integer :category_id

      t.timestamps
    end
  end
end
