class RenameCVerificationAdminId < ActiveRecord::Migration[5.2]
  def change
    rename_column :verifications, :admin_id, :user_id
  end
end
