class AddFeesToMoneyOps < ActiveRecord::Migration[5.2]
  def change
    add_column :money_ops, :fees, :float , :limit=>53
  end
end
