class AddTokenSentAtToLogin < ActiveRecord::Migration[5.2]
  def change
    add_column :logins, :email_token_sent_at, :datetime 
  end
end
