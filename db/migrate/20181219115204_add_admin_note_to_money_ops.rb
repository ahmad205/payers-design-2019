class AddAdminNoteToMoneyOps < ActiveRecord::Migration[5.2]
  def change
    add_column :money_ops, :admin_note, :string
  end
end
