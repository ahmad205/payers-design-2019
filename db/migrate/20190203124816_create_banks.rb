class CreateBanks < ActiveRecord::Migration[5.2]
  def change
    create_table :banks do |t|
      t.string :bank_name
      t.string :branch_name
      t.string :bank_code
      t.integer :country_id
      t.boolean :is_iban, :default => false
      t.boolean :verified, :default => false

      t.timestamps
    end
  end
end
