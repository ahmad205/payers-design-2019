class AddColumnToUsersBanks < ActiveRecord::Migration[5.2]
  def change
    add_column :users_banks, :branch_name, :string
    add_column :users_banks, :branch_number, :string
    add_column :users_banks, :account_type, :integer
    add_column :users_banks, :is_default, :boolean, :default => false
  end
end
