class AddCToAffilateProgram < ActiveRecord::Migration[5.2]
  def change
    add_column :affilate_programs, :invitor_commission, :float, :default => nil
    add_column :affilate_programs, :invitor_commission_status, :boolean, :default => nil
  end
end
