class AddAddressIdToUsersWalletsTransfers < ActiveRecord::Migration[5.2]
  def change
    add_column :users_wallets_transfers, :address_id, :integer
  end
end
