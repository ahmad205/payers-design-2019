class AddBranchNameToCards < ActiveRecord::Migration[5.2]
  def change
    add_column :cards, :branch_name, :string
  end
end
