class AddColumnToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :total_monthly_send, :float, :default => 0
    add_column :users, :total_monthly_recieve, :float, :default => 0
    add_column :users, :total_daily_send, :float, :default => 0
    add_column :users, :total_daily_recieve, :float, :default => 0

    add_column :users, :total_daily_withdraw, :float, :default => 0
    add_column :users, :total_monthly_withdraw, :float, :default => 0
    add_column :users, :total_daily_deposite, :float, :default => 0
    add_column :users, :total_monthly_deposite, :float, :default => 0

    add_column :users, :daily_send_number, :integer, :default => 0
    add_column :users, :daily_withdraw_number, :integer, :default => 0
    add_column :users, :daily_deposite_number, :integer, :default => 0
  end
end
