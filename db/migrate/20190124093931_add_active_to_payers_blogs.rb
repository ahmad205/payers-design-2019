class AddActiveToPayersBlogs < ActiveRecord::Migration[5.2]
  def change
    add_column :payers_blogs, :active, :integer
  end
end
