class CreateTickets < ActiveRecord::Migration[5.2]
  def change
    create_table :tickets do |t|
      t.string :number
      t.string :title
      t.text :content
      t.text :attachment

      t.timestamps
    end
  end
end
