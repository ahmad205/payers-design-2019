class AddPasswordChangedAtToUser < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :password_changed_at, :date 
  end
end
