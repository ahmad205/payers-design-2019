class AddSlugToQuickPayments < ActiveRecord::Migration[5.2]
  def change
    add_column :quick_payments, :slug, :string
  end
end
