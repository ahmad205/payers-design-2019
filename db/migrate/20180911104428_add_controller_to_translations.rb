class AddControllerToTranslations < ActiveRecord::Migration[5.2]
  def change
    add_column :translations, :controller, :string
  end
end
