class ChangePaymentGatewayToString < ActiveRecord::Migration[5.2]
  def change
      change_column :money_ops, :payment_gateway, :string
  end
end
