class CreateUsersBanks < ActiveRecord::Migration[5.2]
  def change
    create_table :users_banks do |t|
      t.integer :bank_id
      t.integer :user_id
      t.string :account_name
      t.string :account_number
      t.boolean :active, :default => true
      t.boolean :is_iban, :default => false

      t.timestamps
    end
  end
end
