class AddTicketRefToTicketReplies < ActiveRecord::Migration[5.2]
  def change
    add_reference :ticket_replies, :ticket, foreign_key: true
  end
end
