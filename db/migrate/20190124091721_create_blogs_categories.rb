class CreateBlogsCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :blogs_categories do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
