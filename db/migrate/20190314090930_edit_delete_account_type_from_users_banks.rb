class EditDeleteAccountTypeFromUsersBanks < ActiveRecord::Migration[5.2]
  def change
    remove_column :users_banks, :account_name
    remove_column :users_banks, :is_iban
    remove_column :users_banks, :account_type
    rename_column :users_banks, :branch_number, :swift_code
  end
end
