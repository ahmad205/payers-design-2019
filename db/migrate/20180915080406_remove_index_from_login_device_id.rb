class RemoveIndexFromLoginDeviceId < ActiveRecord::Migration[5.2]
  def change
    remove_index :logins, column: :device_id
  end
end
