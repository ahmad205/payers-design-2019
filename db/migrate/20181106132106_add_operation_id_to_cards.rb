class AddOperationIdToCards < ActiveRecord::Migration[5.2]
  def change
    add_column :cards, :operation_id, :string
  end
end
