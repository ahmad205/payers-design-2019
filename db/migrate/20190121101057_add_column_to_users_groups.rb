class AddColumnToUsersGroups < ActiveRecord::Migration[5.2]
  def change
    add_column :users_groups, :country_code, :string
    add_column :users_groups, :class, :string
    add_column :users_groups, :description, :string
    add_column :users_groups, :upgrade_limit, :float, :default => 0
    
    add_column :users_groups, :maximum_daily_send, :float, :default => 0
    add_column :users_groups, :maximum_monthly_send, :float, :default => 0
    add_column :users_groups, :maximum_daily_recieve, :float, :default => 0
    add_column :users_groups, :maximum_monthly_recieve, :float, :default => 0
    add_column :users_groups, :minimum_send_value, :float, :default => 0
    add_column :users_groups, :maximum_send_value, :float, :default => 0
    add_column :users_groups, :daily_send_number, :integer, :default => 0

    add_column :users_groups, :minimum_withdraw_per_time, :float
    add_column :users_groups, :minimum_deposite_per_time, :float
    add_column :users_groups, :daily_withdraw_number, :integer, :default => 0
    add_column :users_groups, :daily_deposite_number, :integer, :default => 0

    add_column :users_groups, :recieve_fees, :float, :default => 0
    add_column :users_groups, :recieve_ratio, :float, :default => 0
    add_column :users_groups, :withdraw_fees, :float, :default => 0
    add_column :users_groups, :withdraw_ratio, :float, :default => 0
    add_column :users_groups, :deposite_fees, :float, :default => 0
    add_column :users_groups, :deposite_ratio, :float, :default => 0


  end
end
