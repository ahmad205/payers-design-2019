class RenameColumnClassFromUsersGroups < ActiveRecord::Migration[5.2]
  def change
    rename_column :users_groups, :class, :classification
  end
end
