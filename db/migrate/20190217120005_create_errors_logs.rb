class CreateErrorsLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :errors_logs do |t|
      t.integer :user_id
      t.integer :user_type
      t.string :code
      t.string :message

      t.timestamps
    end
  end
end
