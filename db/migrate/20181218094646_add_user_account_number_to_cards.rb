class AddUserAccountNumberToCards < ActiveRecord::Migration[5.2]
  def change
    add_column :cards, :user_account_number, :string
    add_column :cards, :note, :string
  end
end
