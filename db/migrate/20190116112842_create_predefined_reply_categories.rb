class CreatePredefinedReplyCategories < ActiveRecord::Migration[5.2]
  def change
    create_table :predefined_reply_categories do |t|
      t.string :name

      t.timestamps
    end
  end
end
