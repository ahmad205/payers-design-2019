class CreateUserAnnouncements < ActiveRecord::Migration[5.2]
  def change
    create_table :user_announcements do |t|
      t.integer :user_id
      t.integer :active
      t.string :slug
      t.string :keyword
      t.string :title
      t.string :description

      t.timestamps
    end
  end
end
