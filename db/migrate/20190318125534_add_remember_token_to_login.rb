class AddRememberTokenToLogin < ActiveRecord::Migration[5.2]
  def self.up
    change_table :logins do |t|
      t.string :remember_token, limit: 128
    end

    add_index :logins, :remember_token

    logins = select_all("SELECT id FROM logins WHERE remember_token IS NULL")

    logins.each do |login|
      update <<-SQL
        UPDATE logins
        SET remember_token = '#{Clearance::Token.new}'
        WHERE id = '#{login['id']}'
      SQL
    end
  end

  def self.down
    change_table :logins do |t|
      t.remove  :remember_token
    end
  end
end
