class AddRespondedToTickets < ActiveRecord::Migration[5.2]
  def change
    add_column :tickets, :responded, :integer, :null => false, :default => 0
  end
end
