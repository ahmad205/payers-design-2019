class CreateTicketReplies < ActiveRecord::Migration[5.2]
  def change
    create_table :ticket_replies do |t|
      t.string :content

      t.timestamps
    end
  end
end
