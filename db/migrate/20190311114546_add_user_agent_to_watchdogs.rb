class AddUserAgentToWatchdogs < ActiveRecord::Migration[5.2]
  def change
    add_column :watchdogs, :user_agent, :string
  end
end
