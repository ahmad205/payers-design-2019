class DeleteColumnFromUsersGroups < ActiveRecord::Migration[5.2]
  def change
    remove_column :users_groups, :min_transfer
    remove_column :users_groups, :max_transfer

  end
end
