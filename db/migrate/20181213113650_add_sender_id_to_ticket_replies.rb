class AddSenderIdToTicketReplies < ActiveRecord::Migration[5.2]
  def change
    add_column :ticket_replies, :sender_id, :integer
  end
end
