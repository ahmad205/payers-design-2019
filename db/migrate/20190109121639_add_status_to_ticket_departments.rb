class AddStatusToTicketDepartments < ActiveRecord::Migration[5.2]
  def change
    add_column :ticket_departments, :status, :integer
  end
end
