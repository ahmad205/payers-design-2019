class AddReminderIdToTickets < ActiveRecord::Migration[5.2]
  def change
    add_column :tickets, :reminder_id, :integer
  end
end
