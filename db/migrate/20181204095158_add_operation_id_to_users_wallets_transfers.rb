class AddOperationIdToUsersWalletsTransfers < ActiveRecord::Migration[5.2]
  def change
    add_column :users_wallets_transfers, :operation_id, :string
  end
end
