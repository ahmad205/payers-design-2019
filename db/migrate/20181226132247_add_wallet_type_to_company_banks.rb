class AddWalletTypeToCompanyBanks < ActiveRecord::Migration[5.2]
  def change
    add_column :company_banks, :wallet_type, :integer
  end
end
