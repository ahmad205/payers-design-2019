class AddPasswordChangedAtToAdmin < ActiveRecord::Migration[5.2]
  def change
    add_column :admins, :password_changed_at, :date , default: Time.now
  end
end
