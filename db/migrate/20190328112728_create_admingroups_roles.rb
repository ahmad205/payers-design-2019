class CreateAdmingroupsRoles < ActiveRecord::Migration[5.2]
  def change
    create_table :admingroups_roles do |t|
      t.references :admins_group, index: true, foreign_key: true
      t.references :role, index: true, foreign_key: true

      t.timestamps
    end
  end
end
