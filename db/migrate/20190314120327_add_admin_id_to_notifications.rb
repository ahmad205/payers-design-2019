class AddAdminIdToNotifications < ActiveRecord::Migration[5.2]
  def change
    add_column :notifications, :admin_id, :integer
  end
end
