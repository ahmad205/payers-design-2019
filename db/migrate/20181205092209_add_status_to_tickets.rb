class AddStatusToTickets < ActiveRecord::Migration[5.2]
  def change
    add_column :tickets, :status, :integer
    add_column :tickets, :evaluation, :integer
  end
end
