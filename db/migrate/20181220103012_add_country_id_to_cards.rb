class AddCountryIdToCards < ActiveRecord::Migration[5.2]
  def change
    add_column :cards, :country_id, :integer
    add_column :cards, :card_type, :integer
  end
end
