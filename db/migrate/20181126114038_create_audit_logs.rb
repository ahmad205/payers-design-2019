class CreateAuditLogs < ActiveRecord::Migration[5.2]
  def change
    create_table :audit_logs do |t|
      t.integer :user_id
      t.string :user_name
      t.string :trace_id
      t.string :action_type
      t.string :action_meta
      t.string :ip

      t.timestamps
    end
  end
end
