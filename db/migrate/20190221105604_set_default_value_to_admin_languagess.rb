class SetDefaultValueToAdminLanguagess < ActiveRecord::Migration[5.2]
  def change

    admins = select_all("SELECT id FROM admins")

    admins.each do |admin|
      update <<-SQL
        UPDATE admins
        SET language = 'ar'
        WHERE id = '#{admin['id']}'
      SQL
    end
  end
end
