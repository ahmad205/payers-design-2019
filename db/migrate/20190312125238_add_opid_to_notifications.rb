class AddOpidToNotifications < ActiveRecord::Migration[5.2]
  def change
    add_column :notifications, :controller, :string
    add_column :notifications, :opid, :integer
  end
end
