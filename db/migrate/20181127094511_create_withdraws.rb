class CreateWithdraws < ActiveRecord::Migration[5.2]
  def change
    create_table :withdraws do |t|
      t.integer :user_id
      t.integer :bank_id
      t.integer :operation_id
      t.float :amount , :limit=>53
      t.float :fees , :limit=>53
      t.integer :status
      t.string :withdraw_type
      t.string :note

      t.timestamps
    end
  end
end
