class ChangeOperationIdInWithdraws < ActiveRecord::Migration[5.2]
  def change
    change_column :withdraws, :operation_id, :string
  end
end
