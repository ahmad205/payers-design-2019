class AddSeverityLevelToCompanyBanks < ActiveRecord::Migration[5.2]
  def change
    add_column :company_banks, :severity_level, :integer, :default => 1
    add_column :users, :allowed_bank_level  , :integer, :default => 1
  end
end
