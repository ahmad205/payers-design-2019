class AddUserGroupIdToUserAnnouncements < ActiveRecord::Migration[5.2]
  def change
    add_column :user_announcements, :user_group_id, :integer
    add_column :user_announcements, :availability, :integer
  end
end
