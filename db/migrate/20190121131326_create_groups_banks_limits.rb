class CreateGroupsBanksLimits < ActiveRecord::Migration[5.2]
  def change
    create_table :groups_banks_limits do |t|
      t.integer :bank_type
      t.integer :group_id
      t.float :maximum_daily_withdraw
      t.float :maximum_monthly_withdraw
      t.float :maximum_daily_deposite
      t.float :maximum_monthly_deposite

      t.timestamps
    end
  end
end
