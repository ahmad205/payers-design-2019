class CreateTranslations < ActiveRecord::Migration[5.2]
  def change
    create_table :translations do |t|
      t.string :text
      t.string :arabic_translation
      t.string :english_translation

      t.timestamps
    end
  end
end
