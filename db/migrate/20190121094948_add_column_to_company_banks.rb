class AddColumnToCompanyBanks < ActiveRecord::Migration[5.2]
  def change
    add_column :company_banks, :value_added_tax, :float, :default => 0
    add_column :company_banks, :verified_withdraw, :boolean, :default => true
    add_column :company_banks, :verified_deposite, :boolean, :default => true
    add_column :company_banks, :unverified_withdraw, :boolean, :default => true
    add_column :company_banks, :unverified_deposite, :boolean, :default => true
  end
end
