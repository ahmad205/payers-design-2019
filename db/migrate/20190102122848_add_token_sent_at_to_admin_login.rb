class AddTokenSentAtToAdminLogin < ActiveRecord::Migration[5.2]
  def change
    add_column :admin_logins, :email_token_sent_at, :datetime 
  end
end
