class AddServiceStatusToUsersWalletsTransfers < ActiveRecord::Migration[5.2]
  def change
    add_column :users_wallets_transfers, :service_status, :integer
  end
end
