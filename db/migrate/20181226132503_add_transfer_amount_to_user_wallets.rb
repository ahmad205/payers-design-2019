class AddTransferAmountToUserWallets < ActiveRecord::Migration[5.2]
  def change
    add_column :user_wallets, :transfer_amount, :float , :limit=>53
  end
end
