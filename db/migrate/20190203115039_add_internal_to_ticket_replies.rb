class AddInternalToTicketReplies < ActiveRecord::Migration[5.2]
  def change
    add_column :ticket_replies, :internal, :integer
  end
end
