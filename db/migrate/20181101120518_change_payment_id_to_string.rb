class ChangePaymentIdToString < ActiveRecord::Migration[5.2]
  def change
    change_column :money_ops, :payment_id, :string
  end
end
