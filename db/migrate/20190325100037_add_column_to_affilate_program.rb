class AddColumnToAffilateProgram < ActiveRecord::Migration[5.2]
  def change
    add_column :affilate_programs, :profit_source, :integer, :default => nil
  end
end
