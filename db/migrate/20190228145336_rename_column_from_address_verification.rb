class RenameColumnFromAddressVerification < ActiveRecord::Migration[5.2]
  def change
    rename_column :address_verifications, :country_id, :document_type
    rename_column :address_verifications, :city, :address
    rename_column :address_verifications, :state, :name
    rename_column :address_verifications, :street, :issue_date
    remove_column :address_verifications, :apartment_number
  end
end
