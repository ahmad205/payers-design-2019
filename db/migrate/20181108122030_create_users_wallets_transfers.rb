class CreateUsersWalletsTransfers < ActiveRecord::Migration[5.2]
  def change
    create_table :users_wallets_transfers do |t|
      t.integer :user_id
      t.integer :user_to_id
      t.integer :transfer_method
      t.integer :transfer_type
      t.integer :hold_period
      t.float :amount , :limit=>53
      t.float :ratio , :limit=>53
      t.integer :approve
      t.string :note

      t.timestamps
    end
  end
end
