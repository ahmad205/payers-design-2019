class CreateBankAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :bank_accounts do |t|
      t.integer :user_id
      t.string :bank_name
      t.string :branche_name
      t.string :swift_code
      t.string :country
      t.string :account_name
      t.string :account_number

      t.timestamps
    end
  end
end
