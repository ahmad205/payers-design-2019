class CreateQuickPayments < ActiveRecord::Migration[5.2]
  def change
    create_table :quick_payments do |t|
      t.integer :user_id
      t.integer :active
      t.integer :availability
      t.string :title
      t.string :description

      t.timestamps
    end
  end
end
