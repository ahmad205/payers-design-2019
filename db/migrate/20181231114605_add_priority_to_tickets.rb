class AddPriorityToTickets < ActiveRecord::Migration[5.2]
  def change
    add_column :tickets, :cc_recipient, :string
    add_column :tickets, :priority, :integer, :default => 2
  end
end
