# # This file should contain all the record creation needed to seed the database with its default values.
# # The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
# #
# # Examples:





# Country.create([{
#     short_code: "EG",
#     Full_Name: "Egypt",
#     Phone_code: "002",
#     Currency:  "EGP",
#     language: "Arabic",
#     active: "1",
#             },
#    {
#     short_code: "SA",
#     Full_Name: "Saudi Arabia",
#     Phone_code: "00966",
#     Currency:  "SAR",
#     language: "Arabic",
#     active: "1",
#             }
#     ]) 

# User.create([{
#     email: "ahmedhassan20593@gmail.com",
#     password: "Kh123456",
#     username: "AHassan",
#     firstname: "ahmad",
#     lastname: "hasan",
#     status: 1,
#     account_number: "PS100000001",
#     remember_token: Clearance::Token.new,
#     country_id: 1
# },{
#     email: "work@aymanfekri.com",
#     password: "Ayman12345",
#     username: "Ayman",
#     firstname: "ayman",
#     lastname: "fekri",
#     status: 1,
#     account_number: "PS100000002",
#     remember_token: Clearance::Token.new,
#     country_id: 1
# },{
#     email: "omararansa10@gmail.com",
#     password: "Omar123456",
#     username: "Omar",
#     firstname: "omar",
#     lastname: "aransa",
#     status: 1,
#     account_number: "PS100000003",
#     remember_token: Clearance::Token.new,
#     country_id: 1
# },{
#     email: "amira.elfayome@servicehigh.com",
#     password: "Amira123456",
#     username: "Amira",
#     firstname: "amira",
#     lastname: "elfayome",
#     status: 1,
#     account_number: "PS100000004",
#     remember_token: Clearance::Token.new,
#     country_id: 1
# }
# ])  
        
# # tansfer_balance_ratio = WalletsTransferRatio.create([{ key: 'max_transfer', value: 10, description: 'maximum wallets transfer balance' }, { key: 'min_transfer', value: 2, description: 'minimum wallets transfer balance' }])

# # defualt_notification = NotificationsSetting.create(user_id: 0,money_transactions: 1,pending_transactions: 2,transactions_updates: 2,help_tickets_updates: 3,tickets_replies: 3,account_login: 0,change_password: 3,verifications_setting: 1)

# # TicketDepartment.create(id: 1,name: 'department 1', code: 'AA', description: 'department 1')

# # Admin.create([{
# #             email: "ahmedhassan20593@gmail.com",
# #             password: "Kh123456",
# #             username: "AHassan",
# #             firstname: "ahmad",
# #             lastname: "hasan",
# #             status: 1,
# #             account_number: "PS100000001",
# #             remember_token: Clearance::Token.new,
# #             country_id: 1
# #         },{
# #             email: "work@aymanfekri.com",
# #             password: "Ayman12345",
# #             username: "Ayman",
# #             firstname: "ayman",
# #             lastname: "fekri",
# #             status: 1,
# #             account_number: "PS100000002",
# #             remember_token: Clearance::Token.new,
# #             country_id: 1
# #         },{
# #             email: "omararansa10@gmail.com",
# #             password: "Omar12345",
# #             username: "Omar",
# #             firstname: "omar",
# #             lastname: "aransa",
# #             status: 1,
# #             account_number: "PS100000003",
# #             remember_token: Clearance::Token.new,
# #             country_id: 1
# #         },{
# #             email: "amira.elfayome@servicehigh.com",
# #             password: "Amira123456",
# #             username: "Amira",
# #             firstname: "amira",
# #             lastname: "elfayome",
# #             status: 1,
# #             account_number: "PS100000004",
# #             remember_token: Clearance::Token.new,
# #             country_id: 1
# #         }
# # ])

# UsersGroup.create([{
#     id: 1,
#     country_code: "EG",
#     classification: "1",
#     name: "EG_class1",
#     description: "first class for Egyptian Groups - upgrade_limit is set to 0 because this class is the top for this group",
#     upgrade_limit: "0",
#     maximum_daily_send: "5000",
#     maximum_monthly_send: "150000",
#     maximum_daily_recieve: "5000",
#     maximum_monthly_recieve: "0",
#     minimum_send_value: "10",
#     maximum_send_value: "5000",
#     daily_send_number: "5",
#     minimum_withdraw_per_time: "100",
#     minimum_deposite_per_time: "100",
#     daily_withdraw_number: "5",
#     daily_deposite_number: "5",
#     recieve_fees: ".01",
#     recieve_ratio: ".001",
#     withdraw_fees: ".01",
#     withdraw_ratio: ".001",
#     deposite_fees: ".01", 
#     deposite_ratio: ".001",
#     },{
#     id: 2,
#     country_code: "EG",
#     classification: "2",
#     name: "EG_class2",
#     description: "second class for Egyptian Groups",
#     upgrade_limit: "90000",
#     maximum_daily_send: "3000",
#     maximum_monthly_send: "90000",
#     maximum_daily_recieve: "3000",
#     maximum_monthly_recieve: "90000",
#     minimum_send_value: "50",
#     maximum_send_value: "3000",
#     daily_send_number: "2",
#     minimum_withdraw_per_time: "50",
#     minimum_deposite_per_time: "50",
#     daily_withdraw_number: "2",
#     daily_deposite_number: "2",
#     recieve_fees: ".03",
#     recieve_ratio: ".003",
#     withdraw_fees: ".03",
#     withdraw_ratio: ".003",
#     deposite_fees: ".03", 
#     deposite_ratio: ".003",
#     },{
#     id: 3,
#     country_code: "SA",
#     classification: "1",
#     name: "SA_class1",
#     description: "first class for Saudian Groups - upgrade_limit is set to 0 because this class is the top for this group",
#     upgrade_limit: "0",
#     maximum_daily_send: "5000",
#     maximum_monthly_send: "150000",
#     maximum_daily_recieve: "5000",
#     maximum_monthly_recieve: "0",
#     minimum_send_value: "10",
#     maximum_send_value: "5000",
#     daily_send_number: "5",
#     minimum_withdraw_per_time: "100",
#     minimum_deposite_per_time: "100",
#     daily_withdraw_number: "5",
#     daily_deposite_number: "5",
#     recieve_fees: ".01",
#     recieve_ratio: ".001",
#     withdraw_fees: ".01",
#     withdraw_ratio: ".001",
#     deposite_fees: ".01", 
#     deposite_ratio: ".001",     
#     },{
#     id: 4,
#     country_code: "SA",
#     classification: "2",
#     name: "SA_class2",
#     description: "second class for Saudian Groups",
#     upgrade_limit: "90000",
#     maximum_daily_send: "3000",
#     maximum_monthly_send: "90000",
#     maximum_daily_recieve: "3000",
#     maximum_monthly_recieve: "90000",
#     minimum_send_value: "50",
#     maximum_send_value: "3000",
#     daily_send_number: "2",
#     minimum_withdraw_per_time: "50",
#     minimum_deposite_per_time: "50",
#     daily_withdraw_number: "2",
#     daily_deposite_number: "2",
#     recieve_fees: ".03",
#     recieve_ratio: ".003",
#     withdraw_fees: ".03",
#     withdraw_ratio: ".003",
#     deposite_fees: ".03", 
#     deposite_ratio: ".003",
#     },{
#     id: 5,
#     country_code: "Public",
#     classification: "0",
#     name: "Public_Unverified",
#     description: "Unverified Users - upgrade_limit is set to 0 because this group user's can not be upgraded until verifing their account",
#     upgrade_limit: "0",
#     maximum_daily_send: "100",
#     maximum_monthly_send: "3000",
#     maximum_daily_recieve: "100",
#     maximum_monthly_recieve: "3000",
#     minimum_send_value: "5",
#     maximum_send_value: "100",
#     daily_send_number: "1",
#     minimum_withdraw_per_time: "100",
#     minimum_deposite_per_time: "100",
#     daily_withdraw_number: "1",
#     daily_deposite_number: "2",
#     recieve_fees: "4",
#     recieve_ratio: ".04",
#     withdraw_fees: "4",
#     withdraw_ratio: ".04",
#     deposite_fees: "4", 
#     deposite_ratio: ".04",
#     },{
#     id: 6,
#     country_code: "Public",
#     classification: "1",
#     name: "Public_class1",
#     description: "first class for Public Groups - upgrade_limit is set to 0 because this class is the top for this group",
#     upgrade_limit: "0",
#     maximum_daily_send: "5000",
#     maximum_monthly_send: "150000",
#     maximum_daily_recieve: "5000",
#     maximum_monthly_recieve: "0",
#     minimum_send_value: "10",
#     maximum_send_value: "5000",
#     daily_send_number: "5",
#     minimum_withdraw_per_time: "100",
#     minimum_deposite_per_time: "100",
#     daily_withdraw_number: "5",
#     daily_deposite_number: "5",
#     recieve_fees: ".01",
#     recieve_ratio: ".001",
#     withdraw_fees: ".01",
#     withdraw_ratio: ".001",
#     deposite_fees: ".01", 
#     deposite_ratio: ".001",
#     },{
#     id: 7,
#     country_code: "Public",
#     classification: "2",
#     name: "Public_class2",
#     description: "second class for Public Groups",
#     upgrade_limit: "90000",
#     maximum_daily_send: "3000",
#     maximum_monthly_send: "90000",
#     maximum_daily_recieve: "3000",
#     maximum_monthly_recieve: "90000",
#     minimum_send_value: "50",
#     maximum_send_value: "3000",
#     daily_send_number: "2",
#     minimum_withdraw_per_time: "50",
#     minimum_deposite_per_time: "50",
#     daily_withdraw_number: "2",
#     daily_deposite_number: "2",
#     recieve_fees: ".03",
#     recieve_ratio: ".003",
#     withdraw_fees: ".03",
#     withdraw_ratio: ".003",
#     deposite_fees: ".03", 
#     deposite_ratio: ".003",
#     }
# ])

# GroupsBanksLimit.create([{
#     bank_type: "local_bank",
#     group_id: 1,
#     maximum_daily_withdraw: "10000" ,
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#    },{
#     bank_type: "electronic_bank",
#     group_id: 1,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#    },{
#     bank_type: "digital_bank",
#     group_id: 1,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "local_bank",
#     group_id: 2,
#     maximum_daily_withdraw: "10000" ,
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "electronic_bank",
#     group_id: 2,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "digital_bank",
#     group_id: 2,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "local_bank",
#     group_id: 3,
#     maximum_daily_withdraw: "10000" ,
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "electronic_bank",
#     group_id: 3,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "digital_bank",
#     group_id: 3,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "local_bank",
#     group_id: 4,
#     maximum_daily_withdraw: "10000" ,
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "electronic_bank",
#     group_id: 4,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "digital_bank",
#     group_id: 4,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "local_bank",
#     group_id: 5,
#     maximum_daily_withdraw: "10000" ,
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "electronic_bank",
#     group_id: 5,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "digital_bank",
#     group_id: 5,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#         bank_type: "local_bank",
#     group_id: 6,
#     maximum_daily_withdraw: "10000" ,
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "electronic_bank",
#     group_id: 6,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "digital_bank",
#     group_id: 6,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "local_bank",
#     group_id: 7,
#     maximum_daily_withdraw: "10000" ,
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "electronic_bank",
#     group_id: 7,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     },{
#     bank_type: "digital_bank",
#     group_id: 7,
#     maximum_daily_withdraw: "10000",
#     maximum_monthly_withdraw: "300000",
#     maximum_daily_deposite: "10000",
#     maximum_monthly_deposite: "300000"
#     }

# ])

# Setting.create([{
#             key: "website_name",
#             value: "Payers",
#             description: "this is the website name.",
#         },{
#             key: "payers_deposit_expenses",
#             value: "1",
#             description: "Determines when payers deposit expenses will be taken:, 1: added to the amount before deposit, 2: Deducted from the amount after deposit",
#         },{ 
#             key: "language",
#             value: "en",
#             description: "this is the website language.",
#         },{
#             key: "default_country",
#             value: "Egypt",
#             description: "website client default country.",
#         },{
#             key: "time_zone",
#             value: "Cairo",
#             description: "website time zone.",
#         },{
#             key: "date_format",
#             value: "DD-MM-YYYY",
#             description: "website date format.",
#         },{
#             key: "maintenance_mode",
#             value: "on",
#             description: "website main status.",
#         },{
#             key: "logging_level",
#             value: "level1",
#             description: "website logging level.",
#         },{
#             key: "withdraw_status",
#             value: "off",
#             description: "withdraw closing status.",
#         },{
#             key: "withdraw_msg",
#             value: "sorry this page is under work try again later",
#             description: "withdraw closing message.",
#         },{
#             key: "deposit_status",
#             value: "off",
#             description: "deposit closing status.",
#         },{
#             key: "deposit_msg",
#             value: "sorry this page is under work try again later",
#             description: "deposit closing message.",
#         },{
#             key: "transfer_status",
#             value: "off",
#             description: "transfer money closing status.",
#         },{
#             key: "transfer_msg",
#             value: "sorry this page is under work try again later",
#             description: "transfer closing message.",
#         },{
#             key: "cards_status",
#             value: "off",
#             description: "buy cards closing status.",
#         },{
#             key: "cards_msg",
#             value: "sorry this page is under work try again later",
#             description: "buy cards closing message.",
#         },{
#             key: "support_status",
#             value: "off",
#             description: "support tickets closing status.",
#         },{
#             key: "support_msg",
#             value: "sorry this page is under work try again later",
#             description: "support tickets closing message.",
#         }
# ])




Role.create([{
                name: "address_index",
                caption: "address",
                status: 1,
            },
            {
                name: "show_address",
                caption: "address",
                status: 1,
            },
            {
                name: "new_address",
                caption: "address",
                status: 1,
            },
            {
                name: "edit_address",
                caption: "address",
                status: 1,
            },
            {
                name: "delete_address",
                caption: "address",
                status: 1,
            },
            {
                name: "address_verification_index",
                caption: "address_verification",
                status: 1,
            },
            {
                name: "show_address_verification",
                caption: "address_verification",
                status: 1,
            },
            {
                name: "edit_address_verification",
                caption: "address_verification",
                status: 1,
            },
            {
                name: "admin_login_index",
                caption: "admin_login",
                status: 1,
            },
            {
                name: "show_admin_login",
                caption: "admin_login",
                status: 1,
            },
            {
                name: "admin_watchdogs_index",
                caption: "admin_watchdogs",
                status: 1,
            },
            {
                name: "show_admin_watchdogs",
                caption: "admin_watchdogs",
                status: 1,
            },
            {
                name: "admin_index",
                caption: "admin",
                status: 1,
            },
            {
                name: "show_admin",
                caption: "admin",
                status: 1,
            },
            {
                name: "new_admin",
                caption: "admin",
                status: 1,
            },
            {
                name: "affilate_program_index",
                caption: "affilate_program",
                status: 1,
            },
            {
                name: "audit_log_index",
                caption: "audit_log",
                status: 1,
            },
            {
                name: "show_audit_log",
                caption: "audit_log",
                status: 1,
            },
            {
                name: "delete_audit_log",
                caption: "audit_log",
                status: 1,
            },
            {
                name: "bank_account_index",
                caption: "bank_account",
                status: 1,
            },
            {
                name: "show_bank_account",
                caption: "bank_account",
                status: 1,
            },
            {
                name: "new_bank_account",
                caption: "bank_account",
                status: 1,
            },
            {
                name: "edit_bank_account",
                caption: "bank_account",
                status: 1,
            },
            {
                name: "delete_bank_account",
                caption: "bank_account",
                status: 1,
            },
            {
                name: "banks_index",
                caption: "banks",
                status: 1,
            },
            {
                name: "show_banks",
                caption: "banks",
                status: 1,
            },
            {
                name: "new_banks",
                caption: "banks",
                status: 1,
            },
            {
                name: "edit_banks",
                caption: "banks",
                status: 1,
            },
            {
                name: "delete_banks",
                caption: "banks",
                status: 1,
            },
            {
                name: "blogs_category_index",
                caption: "blogs_category",
                status: 1,
            },
            {
                name: "show_blogs_category",
                caption: "blogs_category",
                status: 1,
            },
            {
                name: "new_blogs_category",
                caption: "blogs_category",
                status: 1,
            },
            {
                name: "edit_blogs_category",
                caption: "blogs_category",
                status: 1,
            },
            {
                name: "delete_blogs_category",
                caption: "blogs_category",
                status: 1,
            },
            {
                name: "card_index",
                caption: "card",
                status: 1,
            },
            {
                name: "show_card",
                caption: "card",
                status: 1,
            },
            {
                name: "new_card",
                caption: "card",
                status: 1,
            },
            {
                name: "edit_card",
                caption: "card",
                status: 1,
            },
            {
                name: "delete_card",
                caption: "card",
                status: 1,
            },
            {
                name: "cards_category_index",
                caption: "cards_category",
                status: 1,
            },
            {
                name: "show_cards_category",
                caption: "cards_category",
                status: 1,
            },
            {
                name: "new_cards_category",
                caption: "cards_category",
                status: 1,
            },
            {
                name: "edit_cards_category",
                caption: "cards_category",
                status: 1,
            },
            {
                name: "delete_cards_category",
                caption: "cards_category",
                status: 1,
            },
            {
                name: "cards_logs_index",
                caption: "cards_logs",
                status: 1,
            },
            {
                name: "show_cards_logs",
                caption: "cards_logs",
                status: 1,
            },
            {
                name: "company_banks_index",
                caption: "company_banks",
                status: 1,
            },
            {
                name: "show_company_banks",
                caption: "company_banks",
                status: 1,
            },
            {
                name: "new_company_banks",
                caption: "company_banks",
                status: 1,
            },
            {
                name: "edit_company_banks",
                caption: "company_banks",
                status: 1,
            },
            {
                name: "delete_company_banks",
                caption: "company_banks",
                status: 1,
            },
            {
                name: "countries_index",
                caption: "countries",
                status: 1,
            },
            {
                name: "show_countries",
                caption: "countries",
                status: 1,
            },
            {
                name: "new_countries",
                caption: "countries",
                status: 1,
            },
            {
                name: "edit_countries",
                caption: "countries",
                status: 1,
            },
            {
                name: "delete_countries",
                caption: "countries",
                status: 1,
            },
            {
                name: "errors_logs_index",
                caption: "errors_logs",
                status: 1,
            },
            {
                name: "show_errors_logs",
                caption: "errors_logs",
                status: 1,
            },
            {
                name: "groups_banks_limits_index",
                caption: "groups_banks_limits",
                status: 1,
            },
            {
                name: "show_groups_banks_limits",
                caption: "groups_banks_limits",
                status: 1,
            },
            {
                name: "new_groups_banks_limits",
                caption: "groups_banks_limits",
                status: 1,
            },
            {
                name: "edit_groups_banks_limits",
                caption: "groups_banks_limits",
                status: 1,
            },
            {
                name: "delete_groups_banks_limits",
                caption: "groups_banks_limits",
                status: 1,
            },
            {
                name: "money_ops_index",
                caption: "money_ops",
                status: 1,
            },
            {
                name: "show_money_ops",
                caption: "money_ops",
                status: 1,
            },
            {
                name: "new_money_ops",
                caption: "money_ops",
                status: 1,
            },
            {
                name: "nationalid_verifications_index",
                caption: "nationalid_verifications",
                status: 1,
            },
            {
                name: "show_nationalid_verifications",
                caption: "nationalid_verifications",
                status: 1,
            },
            {
                name: "edit_nationalid_verifications",
                caption: "nationalid_verifications",
                status: 1,
            },
            {
                name: "notifications_index",
                caption: "notifications",
                status: 1,
            },
            {
                name: "show_notifications",
                caption: "notifications",
                status: 1,
            },
            {
                name: "delete_notifications",
                caption: "notifications",
                status: 1,
            },
            {
                name: "notifications_settings_index",
                caption: "notifications_settings",
                status: 1,
            },
            {
                name: "new_notifications_settings",
                caption: "notifications_settings",
                status: 1,
            },
            {
                name: "edit_notifications_settings",
                caption: "notifications_settings",
                status: 1,
            },
            {
                name: "payers_blogs_index",
                caption: "payers_blogs",
                status: 1,
            },
            {
                name: "show_payers_blogs",
                caption: "payers_blogs",
                status: 1,
            },
            {
                name: "new_payers_blogs",
                caption: "payers_blogs",
                status: 1,
            },
            {
                name: "edit_payers_blogs",
                caption: "payers_blogs",
                status: 1,
            },
            {
                name: "predefined_replies_index",
                caption: "predefined_replies",
                status: 1,
            },
            {
                name: "show_predefined_replies",
                caption: "predefined_replies",
                status: 1,
            },
            {
                name: "new_predefined_replies",
                caption: "predefined_replies",
                status: 1,
            },
            {
                name: "edit_predefined_replies",
                caption: "predefined_replies",
                status: 1,
            },
            {
                name: "delete_predefined_replies",
                caption: "predefined_replies",
                status: 1,
            },
            {
                name: "quick_payments_index",
                caption: "quick_payments",
                status: 1,
            },
            {
                name: "show_quick_payments",
                caption: "quick_payments",
                status: 1,
            },
            {
                name: "new_quick_payments",
                caption: "quick_payments",
                status: 1,
            },
            {
                name: "edit_quick_payments",
                caption: "quick_payments",
                status: 1,
            },
            {
                name: "selfie_verifications_index",
                caption: "selfie_verifications",
                status: 1,
            },
            {
                name: "show_selfie_verifications",
                caption: "selfie_verifications",
                status: 1,
            },
            {
                name: "edit_selfie_verifications",
                caption: "selfie_verifications",
                status: 1,
            },
            {
                name: "settings",
                caption: "settings",
                status: 1,
            },
            {
                name: "payers_statistics",
                caption: "payers_statistics",
                status: 1,
            },
            {
                name: "sms_logs",
                caption: "sms_logs",
                status: 1,
            },
            {
                name: "ticket_department_index",
                caption: "ticket_department",
                status: 1,
            },
            {
                name: "show_ticket_department",
                caption: "ticket_department",
                status: 1,
            },
            {
                name: "new_ticket_department",
                caption: "ticket_department",
                status: 1,
            },
            {
                name: "edit_ticket_department",
                caption: "ticket_department",
                status: 1,
            },
            {
                name: "delete_ticket_department",
                caption: "ticket_department",
                status: 1,
            },
            {
                name: "tickets_index",
                caption: "tickets",
                status: 1,
            },
            {
                name: "show_tickets",
                caption: "tickets",
                status: 1,
            },
            {
                name: "new_tickets",
                caption: "tickets",
                status: 1,
            },
            {
                name: "reply_tickets",
                caption: "tickets",
                status: 1,
            },
            {
                name: "close_tickets",
                caption: "tickets",
                status: 1,
            },
            {
                name: "tickets_priority",
                caption: "tickets",
                status: 1,
            },
            {
                name: "tickets_reminder",
                caption: "tickets",
                status: 1,
            },
            {
                name: "tickets_statistics",
                caption: "tickets",
                status: 1,
            },
            {
                name: "translations_index",
                caption: "translations",
                status: 1,
            },
            {
                name: "show_translations",
                caption: "translations",
                status: 1,
            },
            {
                name: "new_translations",
                caption: "translations",
                status: 1,
            },
            {
                name: "edit_translations",
                caption: "translations",
                status: 1,
            },
            {
                name: "uploads_index",
                caption: "uploads",
                status: 1,
            },
            {
                name: "show_uploads",
                caption: "uploads",
                status: 1,
            },
            {
                name: "new_uploads",
                caption: "uploads",
                status: 1,
            },
            {
                name: "edit_uploads",
                caption: "uploads",
                status: 1,
            },
            {
                name: "delete_uploads",
                caption: "uploads",
                status: 1,
            },
            {
                name: "user_announcements_index",
                caption: "user_announcements",
                status: 1,
            },
            {
                name: "show_user_announcements",
                caption: "user_announcements",
                status: 1,
            },
            {
                name: "new_user_announcements",
                caption: "user_announcements",
                status: 1,
            },
            {
                name: "edit_user_announcements",
                caption: "user_announcements",
                status: 1,
            },
            {
                name: "user_infos",
                caption: "user_infos",
                status: 1,
            },
            {
                name: "user_wallets_index",
                caption: "user_wallets",
                status: 1,
            },
            {
                name: "show_user_wallets",
                caption: "user_wallets",
                status: 1,
            },
            {
                name: "new_user_wallets",
                caption: "user_wallets",
                status: 1,
            },
            {
                name: "edit_user_wallets",
                caption: "user_wallets",
                status: 1,
            },
            {
                name: "admin_add_balance_to_user",
                caption: "user_wallets",
                status: 1,
            },
            {
                name: "user_index",
                caption: "user",
                status: 1,
            },
            {
                name: "show_user",
                caption: "user",
                status: 1,
            },
            {
                name: "new_user",
                caption: "user",
                status: 1,
            },
            {
                name: "edit_user",
                caption: "user",
                status: 1,
            },
            {
                name: "users_banks_index",
                caption: "users_banks",
                status: 1,
            },
            {
                name: "show_users_banks",
                caption: "users_banks",
                status: 1,
            },
            {
                name: "new_users_banks",
                caption: "users_banks",
                status: 1,
            },
            {
                name: "edit_users_banks",
                caption: "users_banks",
                status: 1,
            },
            {
                name: "delete_users_banks",
                caption: "users_banks",
                status: 1,
            },
            {
                name: "users_groups_index",
                caption: "users_groups",
                status: 1,
            },
            {
                name: "show_users_groups",
                caption: "users_groups",
                status: 1,
            },
            {
                name: "new_users_groups",
                caption: "users_groups",
                status: 1,
            },
            {
                name: "edit_users_groups",
                caption: "users_groups",
                status: 1,
            },
            {
                name: "delete_users_groups",
                caption: "users_groups",
                status: 1,
            },
            {
                name: "users_wallets_transfers_index",
                caption: "users_wallets_transfers",
                status: 1,
            },
            {
                name: "show_users_wallets_transfers",
                caption: "users_wallets_transfers",
                status: 1,
            },
            {
                name: "new_users_wallets_transfers",
                caption: "users_wallets_transfers",
                status: 1,
            },
            {
                name: "verifications_index",
                caption: "verifications",
                status: 1,
            },
            {
                name: "show_verifications",
                caption: "verifications",
                status: 1,
            },
            {
                name: "new_verifications",
                caption: "verifications",
                status: 1,
            },
            {
                name: "edit_verifications",
                caption: "verifications",
                status: 1,
            },
            {
                name: "delete_verifications",
                caption: "verifications",
                status: 1,
            },
            {
                name: "wallets_transfer_ratios_index",
                caption: "wallets_transfer_ratios",
                status: 1,
            },
            {
                name: "new_wallets_transfer_ratios",
                caption: "wallets_transfer_ratios",
                status: 1,
            },
            {
                name: "edit_wallets_transfer_ratios",
                caption: "wallets_transfer_ratios",
                status: 1,
            },
            {
                name: "watchdogs_index",
                caption: "watchdogs",
                status: 1,
            },
            {
                name: "show_watchdogs",
                caption: "watchdogs",
                status: 1,
            },
            {
                name: "delete_watchdogs",
                caption: "watchdogs",
                status: 1,
            },
            {
                name: "withdraws_index",
                caption: "withdraws",
                status: 1,
            },
            {
                name: "show_withdraws",
                caption: "withdraws",
                status: 1,
            },
            {
                name: "new_withdraws",
                caption: "withdraws",
                status: 1,
            },
            {
                name: "edit_withdraws",
                caption: "withdraws",
                status: 1,
            },
            {
                name: "delete_withdraws",
                caption: "withdraws",
                status: 1,
            }
])

AdminsGroup.create([{
    id: "1",
    name: "Super Admin",
}
])

Role.all.find_each do |role|
    AdmingroupsRole.create!(admins_group_id: "1",:role_id => role.id)
end