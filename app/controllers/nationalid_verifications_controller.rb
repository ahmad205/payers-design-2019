class NationalidVerificationsController < ApplicationController
  before_action :set_nationalid_verification, only: [:show, :edit, :update, :destroy]
  before_action :require_login

  # create new National Id Verification
  # @param [String] legal_name
  # @param [Integer] national_id
  # @param [Integer] document_type
  # @param [Date] issue_date
  # @param [Date] expire_date
  # @param [Blob] attachment1
  # @param [Blob] attachment2
  # @return [Integer] id
  # @return [String] username
  # @return [String] legal_name
  # @return [Integer] national_id
  # @return [Integer] document_type
  # @return [Date] issue_date
  # @return [Date] expire_date
  # @return [Blob] attachment1
  # @return [Blob] attachment2
  # @return [Integer] status
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def create
    begin
      @current_user_verification = NationalidVerification.where("user_id =?", current_user.id.to_i).first
      if @current_user_verification != nil
        redirect_to root_path , notice: "not allowed"
      else
        @nationalid_verification = NationalidVerification.new(nationalid_verification_params)
        @nationalid_verification.user_id = current_user.id
        if params[:nationalid_verification][:legal_name] != "" and params[:nationalid_verification][:national_id] != "" and params[:nationalid_verification][:document_type] != "" and params[:nationalid_verification][:issue_date] != "" and params[:nationalid_verification][:expire_date] != "" and params[:nationalid_verification][:images] != nil
          @nationalid_verification.status = "Pending"
        else
          @nationalid_verification.status = "UnVerified"
        end
        if params[:nationalid_verification][:legal_name]
          @legal_name = params[:nationalid_verification][:legal_name]
          current_user.update(firstname: @legal_name.partition(" ").first, lastname: @legal_name.partition(" ").last)
        end
        respond_to do |format|
          if @nationalid_verification.save
            if @nationalid_verification.status == "Pending"
              format.html { redirect_to activate_account_path }
            else
              format.html { redirect_to dashboard_path }
            end
            format.json { render :show, status: :created, location: @nationalid_verification }
            AuditlogJob.perform_async(user_id: current_user.id, user_name: current_user.uuid, action_type: "Upload Nationalid verifications", action_meta: "Nationalid verification was successfully created.", ip: request.env['REMOTE_ADDR'])
          else
            format.html { redirect_to activate_account_path, notice: @nationalid_verification.errors }
            format.json { render json: @nationalid_verification.errors, status: :unprocessable_entity }
          end
        end
      end
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end

  # edit National Id Verification,
  # only admins can edit status and note.
  # @param [String] legal_name
  # @param [Integer] national_id
  # @param [Integer] document_type
  # @param [Date] issue_date
  # @param [Date] expire_date
  # @param [Blob] attachment1
  # @param [Blob] attachment2
  # @param [Integer] status
  # @param [String] note
  # @return [Integer] id
  # @return [String] username
  # @return [String] legal_name
  # @return [Integer] national_id
  # @return [Integer] document_type
  # @return [Date] issue_date
  # @return [Date] expire_date
  # @return [Blob] attachment1
  # @return [Blob] attachment2
  # @return [Integer] status
  # @return [String] note
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def update
    begin
      if (@nationalid_verification.status == "Verified") or (current_user.id != @nationalid_verification.user_id.to_i)
        redirect_to root_path , notice: "not allowed" 
      else
        if params[:nationalid_verification][:legal_name] != "" and params[:nationalid_verification][:national_id] != "" and params[:nationalid_verification][:document_type] != "" and params[:nationalid_verification][:issue_date] != "" and params[:nationalid_verification][:expire_date] != "" and ( (params[:nationalid_verification][:images] != nil) or (@nationalid_verification.images.attached? == true))
          @nationalid_verification.status = "Pending"
        else
          @nationalid_verification.status = "UnVerified"
        end
        if params[:nationalid_verification][:legal_name]
          @legal_name = params[:nationalid_verification][:legal_name]
          if @legal_name.split(" ")[0] == "عبد" or @legal_name.split(" ")[0] == "abd"
            current_user.update(firstname: @legal_name.split(" ")[0] + " " + @legal_name.split(" ")[1] , lastname: @legal_name.split(' ')[2..-1].join(' '))
          else
            current_user.update(firstname: @legal_name.partition(" ").first, lastname: @legal_name.partition(" ").last)
          end
        end
        respond_to do |format|
          if @nationalid_verification.update(nationalid_verification_params)
            if @nationalid_verification.status == "Pending"
              format.html { redirect_to activate_account_path }
            else
              format.html { redirect_to dashboard_path }
            end
            format.json { render :show, status: :ok, location: @nationalid_verification }
            AuditlogJob.perform_async(user_id: current_user.id, user_name: current_user.uuid, action_type: "Edit Nationalid verifications", action_meta: "Nationalid verification was successfully updated.", ip: request.env['REMOTE_ADDR'])
          else
            format.html { redirect_to activate_account_path, notice: @nationalid_verification.errors }
            format.json { render json: @nationalid_verification.errors, status: :unprocessable_entity }
          end
        end
      end
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_nationalid_verification
      @nationalid_verification = NationalidVerification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def nationalid_verification_params
      params.require(:nationalid_verification).permit(:user_id, :legal_name, :national_id, :document_type, :issue_date, :expire_date, :status, :note, :attachment1, :attachment2, images: [])
    end
end
