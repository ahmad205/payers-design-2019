class AffilateProgramsController < ApplicationController
  before_action :set_affilate_program, only: [:show, :edit, :update, :destroy]
  before_action :require_login , except: [:set_invitation_code]
  skip_before_action :check2fa ,only: [:set_invitation_code]
  skip_before_action :check_active_session ,only: [:set_invitation_code]
  skip_before_action :check_password_expiration ,only: [:set_invitation_code]

  def affiliate_program
    begin
      @user = current_user
      @affiliate_commission = AffilateProgram.where(refered_by: current_user.id).all    
      @valid_commission = @affiliate_commission.where(invitor_commission_status: true).sum(:invitor_commission)
      @latest_commission =  @affiliate_commission.where.not(invitor_commission: nil , invitor_commission: 0 ).last
      @total_commission = @affiliate_commission.where.not(invitor_commission: nil).sum(:invitor_commission)
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end

  def affilate_records
    begin
      @total_commission = AffilateProgram.where(refered_by: current_user.id).where.not(invitor_commission: nil,invitor_commission_status: nil).joins(:user).all    
      @commission_setting = Setting.where(:key => "invitor_commision").first
      render :layout => false
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end


  # when invited users click on invitation link which send to their mails,
  # they are redirected here and invitation code put in the session to be saved during registeration
  # @param [Integer] refered_by
  def set_invitation_code
    session[:refered_by] = params[:refered_by]
    session[:profit_source] = params[:through]
    redirect_to sign_up_path
  end


  # registered users can send invitation mail to other users
  # @param [String] email
  def send_invitation_email
    begin
      @email_to = params[:invitiation][:email]
      EmailJob.perform_async(user_mail:current_user,text:@email_to,subject:'send invitation')
      AuditlogJob.perform_async(user_id: current_user.id, user_name: current_user.uuid, action_type: "send invitation", action_meta: "send invitation.", ip: request.env['REMOTE_ADDR'])
      redirect_to affiliate_program_path 
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end

  def transfer_commission
    begin
      @balance = params[:balance].to_f
      @user_wallet = UserWallet.where(user_id: current_user.id).first
      if (@user_wallet and @balance > 0 and @balance != nil )
        @user_wallet.update(amount: @user_wallet.amount.to_f + @balance)
        AffilateProgram.where(refered_by: current_user.id, invitor_commission_status: true).update_all(invitor_commission_status: false)
        MoneyOp.where(user_id: current_user.id, optype: 5, status: 0).update_all(status: 1)
        AuditlogJob.perform_async(user_id: current_user.id, user_name: current_user.uuid, action_type: "withdraw commission", action_meta: "transfer commission to wallet balance", ip: request.env['REMOTE_ADDR'])
        access_notification("Marketing Profits", "You have been Added $ #{@balance.to_f} From Marketing Profits To Your Wallet Balance Successfully", "affilate_programs", @balance)
        render :json => { 'result': "done" }
      end
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_affilate_program
      @affilate_program = AffilateProgram.find(params[:id])
    end

    def access_notification(title, smstext, control, amount)

      @user = current_user
      @title = title
      @smstext = smstext
      @amount = amount

      @user_notification_setting = NotificationsSetting.where(user_id: @user.id.to_i).first      

      Notification.create(user_id: @user.id ,title: @title, description: @smstext , notification_type: @user_notification_setting.money_transactions, controller: control)
        @userphone = @user.country.Phone_code.to_s + @user.telephone
      if @user_notification_setting.money_transactions == 3
        SMSNotification.sms_notification_setting(@userphone, @smstext)
        SmsLog.create(:user_id => @user.id, :pinid => @smstext,:status => 1,:sms_type => 3)
        EmailNotification.email_notification_setting(user_mail:@user.email, subject:@title, text: smstext)
      elsif @user_notification_setting.money_transactions == 2
        SMSNotification.sms_notification_setting(@userphone, @smstext)
        SmsLog.create(:user_id => @user.id, :pinid => @smstext,:status => 1, :sms_type => 3)
      elsif @user_notification_setting.money_transactions == 1
        EmailNotification.email_notification_setting(user_mail:@user.email, subject:@title, text: smstext)
      end

    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def affilate_program_params
      params.require(:affilate_program).permit(:user_id, :refered_by, :invitor_commission, :invitor_commission_status, :profit_source)
    end
end
