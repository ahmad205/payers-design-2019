class LoginsController < ApplicationController
  before_action :require_login
  before_action :set_login, only: [:show, :edit, :update, :destroy]

  
  # show user's active sessions
  # works only if current user is an admin
  # @param [Integer] user_id
  def active_sessions
    if current_user.id == params[:id].to_i
      @logins = Login.where("user_id =? ",params[:id])
    else
      redirect_to root_path , notice: "not allowed" 
    end
  end


  # create new login
  # @param [Integer] user_id
  # @param [String] ip_address
  # @param [String] user_agent	
  # @param [String] device_id
  # @param [String] operation_type
  # @return [Integer] id
  # @return [Integer] user_id
  # @return [String] ip_address
  # @return [String] user_agent	
  # @return [String] device_id
  # @return [String] operation_type	
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def create
    begin
      @login = Login.new(login_params)
      respond_to do |format|
        if @login.save
          format.html { redirect_to @login, notice: 'Login was successfully created.' }
          format.json { render :show, status: :created, location: @login }
        else
          format.html { render :new }
          format.json { render json: @login.errors, status: :unprocessable_entity }
        end
      end
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end

  # destroy login
  # @param [Integer] id
  
  def destroy
    begin
      @login.destroy
      respond_to do |format|
        format.html { redirect_to logins_url, notice: 'Login was successfully destroyed.' }
        format.json { head :no_content }
      end  
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end   
  end

  def error_page
    render :layout => false
  end

  def lock_page
    render :layout => false
  end

  def check_to_unlock
    begin
      @user = User.authenticate(current_user.email,params[:unlock][:password])
      @currentuser = current_user
      if !(@user)
        @currentuser.failedattempts += 1
        if @currentuser.failedattempts >= 3 
          @currentuser.disabled = 1
          @token =  Random.new.rand(111111..999999)
          @currentuser.unlock_token = @token
          EmailJob.perform_async(user_mail:@currentuser,text:@token,subject:'unlock account')
          @currentuser.save
          before_sign_out
          sign_out
          redirect_to sign_in_path  
        else
          @currentuser.save
          redirect_to lock_page_path , notice: "wrong password" 
        end 
      else
        current_user.update(failedattempts: 0)
        session[:last_visit] = Time.now
        redirect_to dashboard_path  
      end
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_login
      @login = Login.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def login_params
      params.require(:login).permit(:user_id, :ip_address, :user_agent, :device_id)
    end
end
