class AddressesController < ApplicationController
  before_action :set_address, only: [:show, :update, :destroy]

  # GET a list of spacific user addresses
  # @param [Integer] user_id the user unique id.
  # @return [id] address unique ID.
  # @return [address] the detailed address.
  # @return [country] the user address country.
  # @return [governorate] the user address governorate.
  # @return [city] the user address city.
  # @return [street] the user address street.
  # @return [default_address] defualt address for a user(true , false).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def user_addresses
    @addresses = Address.where(:user_id => current_user.id.to_i).all
  end


  # POST a new address and save it
  # @return [id] address unique ID (Created automatically).
  # @return [user_id] the user unique id.
  # @return [address] the detailed address.
  # @return [country] the user address country.
  # @return [governorate] the user address governorate.
  # @return [city] the user address city.
  # @return [street] the user address street.
  # @return [default_address] defualt address for a user(true , false).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def create
    @address = Address.new(address_params)
    @address.user_id = current_user.id.to_i
      if @address.save
        redirect_back fallback_location: root_path
        AuditlogJob.perform_async(user_id: current_user.id, user_name: current_user.uuid, action_type: "Add new address", action_meta: "Address was successfully created.", ip: request.env['REMOTE_ADDR'])
      else
        redirect_back fallback_location: root_path, notice: "Address wasn't successfully created."
      end
  
  end

  # Change an existing address params(address,country,governorate,governorate,street,default_address)
  # @return [id] address unique ID.
  # @return [user_id] the user unique id.
  # @return [address] the detailed address.
  # @return [country] the user address country.
  # @return [governorate] the user address governorate.
  # @return [city] the user address city.
  # @return [street] the user address street.
  # @return [default_address] defualt address for a user(true , false).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.

  def update_address
    @address = Address.where(:id => params[:id]).first
    if @address.user_id == current_user.id
      @address.update(country: params[:country], address: params[:address], governorate: params[:governorate], city: params[:city], street: params[:street], default_address: params[:default_address])
      redirect_back fallback_location: user_addresses_url, notice: 'Address successfully updated'
    else
      redirect_back fallback_location: root_path, notice: 'You do not have permission to do this'
    end
  end

  # DELETE an existing address
  # @param [id] address unique ID.
  def destroy
    respond_to do |format|
        @addresses = Address.where(:user_id => @address.user_id).all
        if  @addresses.count == 1
          format.html { redirect_to user_addresses_url, notice: 'Cannot Delete this last Address' }
        else

          if @address.default_address == true
            format.html { redirect_to user_addresses_url, notice: 'Cannot Delete a Default Address' }
          else
            @address.destroy
            format.html { redirect_to user_addresses_url, notice: 'Address was successfully destroyed.' }
            format.json { head :no_content }
            AuditlogJob.perform_async(user_id: current_user.id, user_name: current_user.uuid, action_type: "Delete address", action_meta: "Address was successfully destroyed.", ip: request.env['REMOTE_ADDR'])
          end

        end
    end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_address
      @address = Address.where(user_id: current_user.id.to_i, id: params[:id]).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def address_params
      params.require(:address).permit(:user_id, :address, :country, :governorate, :city, :street, :default_address)
    end
end
