class UsersController < ApplicationController
  before_action :require_login , only: [:show, :edit, :update, :destroy,:index, :add_admin, :add_admin_post]
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  skip_before_action :check2fa ,only: [:new, :destroy, :create, :resend_two_factor,:homepage, :after_sign_up, :resend_confirmation_mail]
  skip_before_action :check_active_session ,only: [:new, :destroy, :create, :resend_two_factor,:homepage, :after_sign_up, :resend_confirmation_mail]
  skip_before_action :check_password_expiration ,only: [:new, :create, :resend_two_factor,:homepage, :after_sign_up, :resend_confirmation_mail]


  
  
  include Clearance::Authentication 
  include ActionView::Helpers::NumberHelper
  #before_action :get_google_auth ,only: [:show, :edit, :update,:index]
  require 'rqrcode'

  def dashboard
    @user_wallet = UserWallet.where(:user_id => current_user.id.to_i).first
    @total_balance = @user_wallet.amount + @user_wallet.transfer_amount
    @user_country = Country.where(id:current_user.country_id).pluck(:short_code).first
    if @user_country == "EG"
      @currency_name = t('users.EGP')
    elsif @user_country == "SA"
      @currency_name = t('users.SAR')
    else
      @currency_name = t('users.dollar')
    end
    @currency_ratio = Setting.check_currency_ratio(@user_country)
    @total_sar = @total_balance.to_f * @currency_ratio.to_f
    @total_balance = @total_balance.round(2)
    if @total_balance == 0
      @total_balance = "0.00"
    end
    @total_balance = number_with_delimiter(@total_balance, :delimiter => ',').to_s.split(".")

    @amount = @user_wallet.amount.round(2)
    @amount_sar = @amount.to_f * @currency_ratio.to_f  
    if @amount == 0
      @amount = "0.00"
    end
    @amount = number_with_delimiter(@amount, :delimiter => ',').to_s.split(".")

    @fozen_balance = UsersWalletsTransfer.where(user_to_id: current_user.id.to_i , approve:0).pluck(:amount).sum.round(2)
    @fozen_sar = @fozen_balance.to_f * @currency_ratio.to_f
    if @fozen_balance == 0
      @fozen_balance = "0.00"
    end
    @fozen_balance = number_with_delimiter(@fozen_balance, :delimiter => ',').to_s.split(".")

    @country = current_user.country.short_code
    @max_receive = UsersGroup.where(country_code: @country ).all
    if @max_receive == []
      @max_receive = UsersGroup.where(country_code: "Public" ).all
    end
    @max_class = @max_receive.maximum("classification")
    @max_receive = @max_receive.where(classification: @max_class ).pluck(:maximum_monthly_recieve).first
    # @max_receive = "90000"

    @notifications = Notification.where(user_id: current_user.id.to_i).all.order(created_at: :DESC)
    @verification = UserInfo.where(user_id: current_user.id.to_i).first

    
    
  end

  def pass_to_security_settings
    session[:authenticated] = "false"    
    if request.method == 'POST'
      @user = User.authenticate(current_user.email,params[:auhthenticate_user][:password])
      if (@user)  
        session[:authenticated] = "true"
        redirect_to secure_my_account_path 
      else
        session[:authenticated] = "false"
        render template: "users/pass_to_security_settings" 
      end
    end
  end

  #don't forget to empty this session variable after finishing this function!!!!!!!!!!!!!!!!!!
  def secure_my_account
    if  session[:authenticated] != "true"
      redirect_to pass_to_security_settings_path 
    end
    @user = current_user
  end

  # show data of user
  # @param [Integer] id 
  # @return [String] username
  # @return [String] email
  # @return [String] account_number
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def show
    if current_user.id == params[:id].to_i
      @user = User.where("id =? " ,params[:id]).first
      @qr = RQRCode::QRCode.new(@user.provisioning_uri("payers"), :size => 7, :level => :h )
      @user_announcement = UserAnnouncement.where(active: 1).all
      @user_group_id = 1
      @user_announcements = @user_announcement.where("user_group_id = ? OR user_id = ? OR availability = ?", @user_group_id,current_user.id,1).all.order('created_at DESC')
      @user_quick_payment_page = QuickPayment.where(user_id: current_user.id).first
      @user_infos = UserInfo.where("user_id =?", current_user.id).first
      @current_user_nationalid = NationalidVerification.where("user_id =?", current_user.id.to_i).first
      @current_user_address = AddressVerification.where("user_id =?", current_user.id.to_i).first
      @current_user_selfie = SelfieVerification.where("user_id =?", current_user.id.to_i).first
    else
      redirect_to error_page_path 
    end
  end
 
  def homepage   
   render layout: false
  end

  # after number of failed login attempts, user's account will be disabled and link will be send to user's mail. 
  # when the user click the link, he is redirected here to unlock account and allow him to signin again. 
  # @param [Integer] user_id 
  # @param [String] token 
  def unlockaccount
    @id = params[:id]
    @token = params[:token]
    @user = User.where("id =?",@id ).first
    if @token  == @user.unlock_token      
       @user.failedattempts = 0
       @user.disabled = 0
       @user.unlock_token = ""
       if @user.save
        AuditlogJob.perform_async(user_id: @user.id, user_name: @user.uuid, action_type: "unlock account", action_meta: "user account has been Unlocked,please sign in .", ip: request.env['REMOTE_ADDR'])
        flash.now.notice = 'Your account is Unlocked,please sign in .'
       end
       render template: "sessions/new"  
    else
      flash.now.notice = 'InValid Token .'
      render template: "sessions/new"  
    end
  end

  def change_locale
    current_user.update(language: params[:locale])
    redirect_to(request.env['HTTP_REFERER']) 
  end

  # confirm user mail after registeration
  # @param [Integer] user_id 
  # @param [String] token
  # when user signup, a confirmation link is send to his email .
  # when the user click the link, he is redirected here to confirm account and signin
  def confirmmail
    @id = params[:id].to_i
    @token = params[:token]
    @user = User.where("id =?",@id ).first
    @verify_code = Verification.where("user_id =?",@id ).first
    if @verify_code.email_confirmation_token == @token
      @user.update(:status => 1)
      @verify_code.update(:email_confirmed_at => Time.now)
      session[:user_id] = nil
      session[:confirmation_code] = nil
      AuditlogJob.perform_async(user_id: @user.id, user_name: @user.uuid, action_type: "confirm mail", action_meta: "user email has been confirmed .", ip: request.env['REMOTE_ADDR'])
    end
    redirect_to sign_in_path
  end

  # form to create new user
  def new
    if session[:refered_by] != nil
      @refered_by = session[:refered_by]
    end
    @user = User.new
    render :layout => false
  end

  # edit user account
  def edit
    if current_user.id != params[:id].to_i 
      redirect_to root_path , notice: "not allowed" 
    end
  end

  # create new account
  # @param [String] username 
  # @param [String] email
  # @param [String] password 
  # @param [Integer] country_id 
  # @param [String] refered_by 
  # @return [String] username 
  # @return [String] email
  # @return [String] password 
  # @return [Integer] country_id 
  # @return [String] uuid
  # @return [String] remember_token
  # @return [String] account_number
  # @return [String] invitation_code
  # @return [Integer] active_otp
  # @return [boolean] account_currency
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def create
    @user = User.new(user_params)
    @token = SecureRandom.hex(4)
    @user.language = Setting.where(key: "language").pluck(:value).first
    @ps = Country.where(id: @user.country_id).pluck(:short_code).first
    @contry_rond = 10.times.map{rand(10)}.join
    @user.account_number = @ps[0,2] + @contry_rond
    respond_to do |format|
      if @user.save
        User.user_details(@user)      
        session[:user_id] = @user.id
        session[:confirmation_code] = @token 
        Verification.create(:user_id => @user.id,:email_confirmation_token => @token)
        Watchdog.create(:user_id => @user.id,:ipaddress => request.env['REMOTE_ADDR'],:logintime => Time.now,:lastvisit => Time.now ,:operation_type => "sign_up",user_agent: request.user_agent)
        if (session[:refered_by] != nil )
          @invited_by = User.where("invitation_code =? ",session[:refered_by]).first
          if (@invited_by != nil)
            AffilateProgram.create(:user_id => @user.id, :refered_by => @invited_by.id, :profit_source => session[:profit_source] )
          end
          session[:refered_by] = nil
          session[:profit_source] = nil 
        end
        EmailJob.perform_async(user_mail:@user,text:@token,subject:'confirm mail')
        format.html { redirect_to after_sign_up_path }
        format.json { render json: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def resend_confirmation_mail
    if session[:confirmation_code] != nil and session[:user_id] != nil
      @user = User.where(id: session[:user_id].to_i).first
      EmailJob.perform_async(user_mail:@user,text:session[:confirmation_code],subject:'confirm mail')
    end
  end

  def after_sign_up
    render :layout => false
  end

  # after user sign in, he redirected to this page to confirm signin.
  # user should enter the code which has been sent to his email or his google code.
  def get_two_factor
    session[:verify_code] = false
    render :layout => false
  end

  # to check the correctness of the entered code.
  # @param [String] code
  # @param [String] token_code
  def post_two_factor
    @user = current_user 
    session[:verify_code] = false
    @user_code = params[:confirm][:code].to_s
    @sent_code = session[:token_code].to_s
    @login = Login.where(user_id: @user.id).last
    if (@user.active_otp == 3)
      @applications_list = SMSService.new(code:@user_code,user_id: @user.id).call
    end
    if ((@user.active_otp == 1 or @user.active_otp == 3) and @login.email_token_sent_at < 5.minute.ago)
      redirect_to two_factor_path, notice: "Code has been expired, please sign out and try again later.."
    elsif (@user.active_otp == 1  and  @user_code == @sent_code and @login.email_token_sent_at >= 5.minute.ago) or (@user.active_otp == 2 and (@user.authenticate_otp(@user_code))) or (@user.active_otp == 3  and  @applications_list["verified"] ==  true and @login.email_token_sent_at >= 5.minute.ago)
         session[:verify_code] = true
         session[:token_code]= ""
         redirect_to dashboard_path
    else 
        redirect_to two_factor_path, notice: "wrong code!" 
    end
    
  end

  def resend_two_factor
    @token = Random.new.rand(111111..999999)
    session[:token_code] = @token
    @login = Login.where("user_id = ? and device_id = ? ", current_user.id,cookies[:device_id]).first
    @login.update(email_token_sent_at: DateTime.now)
    EmailJob.perform_async(user_mail:current_user.email,text:@token,subject:'confirm sign in')
    redirect_to two_factor_path
  end

  # update data of a user .
  # edit users data, only admins and account owner can edit this data.
  # @param [Integer] username 
  # @param [Integer] email
  # @param [password] password 
  # @param [Integer] country_id 
  # @param [Integer] firstname
  # @param [Integer] lastname
  # @param [Integer] active_otp
  # @param [boolean] account_currency
  # @param [password] confirm_password 
  # @return [Integer] username 
  # @return [Integer] email
  # @return [Integer] password 
  # @return [Integer] country_id 
  # @return [Integer] firstname
  # @return [Integer] lastname 
  # @return [String] uuid
  # @return [String] remember_token
  # @return [String] account_number
  # @return [String] invitation_code
  # @return [Integer] active_otp
  # @return [boolean] account_currency
  def update
    #if !(params[:user][:active_otp])
      #@user = User.authenticate(current_user.email,params[:user][:confirm_password])
      #if !(@user)
        #redirect_to edit_user_path(params[:id]) , notice: "wrong password confirmation" and return
      #end
    #end
    if current_user.id == params[:id].to_i
      @nationalid_verification = NationalidVerification.where(user_id: current_user.id).first
      @verification = UserInfo.where(user_id: current_user.id.to_i).first
     respond_to do |format|
      if (params[:user][:password])
        @user.password_changed_at = Time.now
      end
      params[:user][:country_id] = current_user.country_id
      if params[:user][:telephone] and @verification and @verification.mobile == 1
        params[:user][:telephone] = current_user.telephone
      end
      if (params[:user][:firstname] or params[:user][:lastname]) and @nationalid_verification and @nationalid_verification.status != "UnVerified"  
        params[:user][:firstname] = current_user.firstname
        params[:user][:lastname] = current_user.lastname
      elsif (params[:user][:firstname] or params[:user][:lastname]) and @nationalid_verification and @nationalid_verification.status == "UnVerified" 
        @nationalid_verification.update(:legal_name => params[:user][:firstname] + " " + params[:user][:lastname] )
      end
      if @user.update(user_params)
        if (params[:user][:active_otp] or params[:user][:password] or params[:user][:secret_code] )
          format.html { redirect_to secure_my_account_path }
        else
         format.html { redirect_to @user }
        end
        format.json { render :show, status: :ok, location: @user }
        AuditlogJob.perform_async(user_id: current_user.id, user_name: current_user.uuid, action_type: "Edit profile", action_meta: "User profile was successfully updated", ip: request.env['REMOTE_ADDR'])
      else
        #if (params[:user][:password])
          #  format.html { render :edit_password }
        if (params[:user][:firstname])
          format.html { render :show }
        elsif (params[:user][:active_otp] or params[:user][:password] or params[:user][:secret_code])
          format.html { render :secure_my_account }
        end
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
      end
    else
      redirect_to root_path , notice: "not allowed" 
    end    
  end

  # edit user password
  def edit_password
    @user = User.where("id =?", params[:id]).first
  end
  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    if current_user.roleid == 1
      @user.destroy
      respond_to do |format|
        format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
        format.json { head :no_content }
      end
    else
      redirect_to root_path , notice: "not allowed" 
    end
  end

  def send_confirmation_mail
    if params[:id].to_i == 1
      @token = SecureRandom.hex(4)
      session[:current_token_code] = @token
      EmailJob.perform_async(user_mail:current_user.email,text:@token,subject:'confirm sign in')
      render :json => { 'result': @token }
    end
  end


  # on change confirmation way invoke this function to make changes
  # if mail confirmation is selected, send token to user mail, to confirm changes
  # if google authentacation has been selected, generate new QR,user read it and enter qrcode to comfirm changes
  # @param [String] active_otp
  def generate_qr_code
    @user = current_user
    if params[:id] == "google"
      @user.update_attribute(:otp_secret_key, ROTP::Base32.random_base32)
      @qr = RQRCode::QRCode.new(@user.provisioning_uri("payers"), :size => 7, :level => :h )
      render :json => { 'result': @qr  , 'otp_secret_key': @user.otp_secret_key}
    end
  end

  def change_two_factor
    @user = current_user
    @selected_two_factor = params[:id]
    if (@user.update(active_otp: @selected_two_factor ))
      session[:current_token_code] = ""
      render :json => { 'result': "true" }
    else
      render :json => { 'result': "error" }
    end
  end


  # check correctnes of the code in case of google code
  # @param  [String] code
  # @return [boolean] result
  def confirm_google_code
    @user = current_user
    @user_code = params[:code]
    #if (@user.authenticate_otp(@user_code, drift: 60))
    if (@user.authenticate_otp(@user_code))
       @result = true
    else
      @result = false
    end 
    render :json => { 'result': @result }
  end

  # Reset Expired Password
  # If Password has not changed for 3 months, User can not enter his account until change the password 
  # @param [String] current_passord 
  # @param [String] password
  # @param [String] confirm_password 
  # @return [String] username 
  # @return [String] email
  # @return [String] password 
  # @return [Integer] country_id 
  # @return [String] uuid
  # @return [String] remember_token
  # @return [String] account_number
  # @return [String] invitation_code
  # @return [Integer] active_otp
  # @return [boolean] account_currency
  # @return [datetime] created_at
  # @return [datetime] updated_at

  def reset_expired_password
    @user = current_user
    if request.method == 'POST'
      @user = User.authenticate(current_user.email,params[:user][:current_password])
      if !(@user)
        redirect_to reset_expired_password_path, notice: "wrong password confirmation"
      else
        @test_new_password = User.authenticate(current_user.email,params[:user][:password])
        if (@test_new_password)
          redirect_to reset_expired_password_path, notice: "New Password Must be different from current password"
        else
          respond_to do |format|
            @user.password_changed_at = Time.now
            if @user.update(user_params)
              format.html { redirect_to @user }
            else
              format.html { render :reset_expired_password }
            end
          end
        end
      end 
    else
      render :layout => false
    end    
  end

  def check_mail
    @useremail = User.where(:email => params[:email]).first
    if (@useremail)
      @message='True'
    else
      @message='False'
    end
    render :json => { 'bool': @message }
  end

  def check_phone
    @userphone = User.where("telephone =? and id != ?", params[:phone], current_user.id ).first
    if !(@userphone) 
      @message = 'True'
    else
      @message = t('user_infos.repeated_phone')
    end
    render :json => { 'message': @message}
  end

  #deprecated from users/show
  def check_country_code
    @phone_code = Country.where(:id => params[:country_id]).pluck(:Phone_code).first
    render :json => { 'phone_code': @phone_code }
  end
  



  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    def url_encode(s)
      s.to_s.dup.force_encoding("ASCII-8BIT").gsub(/[^a-zA-Z0-9_\-.]/) {
        sprintf("%%%02X", $&.unpack("C")[0])
      }
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:uuid, :users_group_id,  :firstname, :lastname, :language, :disabled, :loginattempts, :username, :email, :password, :secret_code, :otp_secret_key, :active_otp, :account_number, :telephone, :account_currency, :country_id, :unlock_token, :refered_by, :avatar, :password_changed_at, :total_monthly_send, :total_monthly_recieve, :total_daily_send, :total_daily_recieve, :total_daily_withdraw, :total_monthly_withdraw, :total_daily_deposite, :total_monthly_deposite, :daily_send_number, :daily_withdraw_number, :daily_deposite_number)
      
    end
end
