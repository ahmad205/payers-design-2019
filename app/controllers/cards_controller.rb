class CardsController < ApplicationController

  # Get list of all the user cards
  # @return [id] card unique ID (Created automatically).
  # @return [number] card unique number (Created automatically and must be 16 digit).
  # @return [value] card value.
  # @return [status] card status (1 for Active, 2 for Charged , 3 for Pending , 4 for Disabled).
  # @return [country_id] card spacific country.
  # @return [card_type] card type (1 for spacific user, 2 for spacific country).
  # @return [expired_at] card expired Date ( Must be equal or greater than 2 days from now ).
  # @return [user_id] card user ID (defualt = 1).
  # @return [invoice_id] card invoice ID (defualt = 0).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def user_cards
    @user_cards = Card.where(user_id: current_user.id.to_i).all.order('created_at DESC')
  end

  def charging_card
    render layout: false
  end

  # Charging active card post
  # @param [String] number card unique number (Created automatically and must be 16 digit).
  # @return [id] card unique ID (Created automatically).
  # @return [number] card unique number (Created automatically and must be 16 digit).
  # @return [value] card value.
  # @return [status] card status (1 for Active, 2 for Charged , 3 for Pending , 4 for Disabled).
  # @return [expired_at] card expired Date ( Must be equal or greater than 2 days from now ).
  # @return [user_id] card user ID (defualt = 1).
  # @return [invoice_id] card invoice ID (defualt = 0).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def charging_card_post

    @card_number = params[:charging_card][:card_number].delete('-')
    @carddata = Card.where(number: @card_number).first
    if @carddata
      @user_id = @carddata.user_id.to_i
      @card_type = @carddata.card_type
      if Date.today > @carddata.expired_at
        redirect_back fallback_location: root_path, notice: 'Sorry , This Card is Expired'
      else
        ActiveRecord::Base.transaction do
          @user_wallet = UserWallet.where(user_id: current_user.id.to_i).first

          if (@carddata.number == @card_number && @user_id == current_user.id.to_i && @carddata.status.to_i == 1) ||
            (@carddata.number == @card_number && @card_type == 2 && @carddata.status.to_i == 1 && current_user.country_id.to_i == @carddata.country_id.to_i)
            if @card_type == 2
              @money_operation = MoneyOp.create(:optype => 4,:amount => @carddata.value ,:payment_gateway => "Payers" ,:status => 1,:payment_date => DateTime.now,:user_id => current_user.id.to_i)
              @carddata.update(operation_id: @money_operation.opid , user_id: current_user.id)
            else
              @money_operation = MoneyOp.where(opid: @carddata.operation_id).first
              @money_operation.update(status: 1)
            end

            @charging_card_control_status = Setting.where(key: "charging_card_status").first
            if @charging_card_control_status && @charging_card_control_status.value == "open"
              @new_balance = @user_wallet.amount.to_f + @carddata.value.to_f
              @user_wallet.update(:amount => @new_balance)
            else
              @new_balance = @user_wallet.transfer_amount.to_f + @carddata.value.to_f
              @user_wallet.update(:transfer_amount => @new_balance)
            end

            @carddata.update(:status => 2)
            @smstext = "you_have_processed_anew_deposit_of/#{@carddata.value.to_f}/USD_to_your_account/by_prepaid_Payers_card/your_transaction_has_been_successfully_created/the_amount_has_been_successfully_added_to_your_account"
            # @smstext = " لقد قمت بمعاملة ايداع بمبلغ #{@carddata.value.to_f} دولار أمريكي إلى حسابك عن طريق بطاقة بايرز مسبقة الدفع، معاملتك تمت بنجاح، تم إيداع المبلغ في حسابك"
            @notification = access_notification(@smstext,@money_operation.id)
            redirect_back fallback_location: root_path, notice: 'Card was successfully Charged to your wallet'

          else
            redirect_back fallback_location: root_path, notice: 'Sorry , Invalid card for your account'
          end
        end
      end
    else
      redirect_back fallback_location: root_path, notice: 'Sorry , Invalid card number'
    end

  end

  def remittance
    @deposit_status = Setting.where(key: ["deposit_status","deposit_msg"]).all
    render layout: false
  end

  def payment_method
    @local_banks = CompanyBank.where(:bank_category => "local_bank", :status => 1).all
    @electronic_banks = CompanyBank.where(:bank_category => "electronic_bank", :status => 1).all
    @digital_banks = CompanyBank.where(:bank_category => "digital_bank", :status => 1).all
    @domain = request.base_url
    @perfectcompany= CompanyBank.where("bank_key = 'perfectmoney'").first
    @user_country = Country.where(id:current_user.country_id).pluck(:short_code).first
    if @user_country == "EG"
      @currency_name = t('users.EGP')
    elsif @user_country == "SA"
      @currency_name = t('users.SAR')
    else
      @currency_name = t('users.dollar')
    end
    @currency_ratio = Setting.check_currency_ratio(@user_country)
    render layout: false
  end
  
  def payment_data

    @selectedbank = params[:selectedbank]
    @value = params[:value].to_f
    @bank_type = params[:bank_type]
    @bank = CompanyBank.where(:bank_key => @selectedbank ).first
    @net_card_value =  @value
    @expenses = Card.payers_deposit_expenses(current_user.users_group_id,@value)
    @message = Card.checkdeposit(current_user,@value,@bank_type,@selectedbank)
    if @message == ""
      @message = "true"
    end

    if (Setting.where(:key => "payers_deposit_expenses").first.value.to_i == 1)      
      @result = @expenses + (@value * (@bank.ratio/100) + @bank.fees + @bank.value_added_tax)
    else
      @result = (@value * (@bank.ratio/100) + @bank.fees + @bank.value_added_tax)
      @net_card_value = @value - @expenses
    end
    render :json => { 'value': @result ,'bankdata': @bank, 'net_card_value': @net_card_value ,'result' => @message} 
  end

  def pay_operation
    @bank_type = params[:bank_type]
    @bank_key = params[:bank_key]
    @amount = params[:balance_val]
    @card_value = params[:card_vall]
    @net_card_value = params[:net_card_value]
    @fees = params[:fees_val]
    @domain = request.base_url
    @message = Card.checkdeposit(current_user,@card_value,@bank_type,@bank_key)
    if @message == ""

      if @bank_key == 'paypal'
        redirect_to  paypalurl_path(:val=>@amount,:cardvalue => @card_value)
      else
        @new_operation = MoneyOp.create(:optype => 4,:amount => @amount , :fees => @fees ,:payment_gateway => @bank_key ,:status => 0,:payment_date => DateTime.now, :payment_id => 1,:user_id => current_user.id.to_i)
        @new_card = Card.create(:operation_id => @new_operation.opid , :value => @card_value,:expired_at => Date.today + 5.days,:status => 3,:user_id => current_user.id.to_i,:card_type => 1, :net_value => @net_card_value )
        @log = CardsLog.create(:user_id => current_user.id.to_i,:action_type => 1,:ip => request.remote_ip,:card_id => @new_card.id)
        @smstext = "you_have_processed_anew_deposit_of/#{@card_value}/USD_to_your_account/via_direct_bank_deposit/your_transaction_is_inprogress/we_will_notify_you"
        # @smstext = "لقد قمت بمعاملة ايداع بمبلغ #{@card_value} دولار أمريكي إلى حسابك عن طريق الايداع البنكي المباشر، معاملتك قيد التنفيذ، سوف نعلمك عند تمام تنفيذها وإيداع المبلغ في حسابك"
        @notification = access_notification(@smstext,@new_operation.id)
        current_user.update(:total_daily_deposite => current_user.total_daily_deposite.to_f + @card_value.to_f, :daily_deposite_number => current_user.daily_deposite_number.to_i + 1)
        redirect_to("#{@domain}/money_ops/#{@new_operation.id}")
      end 
    else
      redirect_back fallback_location: root_path	,:notice => @message
    end
  end

  def paypalurl

    @paypalbank= CompanyBank.where("bank_key = 'paypal'").first
    values = {
        business:  @paypalbank.account_email,
        cmd: "_xclick",
        currency_code:"USD",
        return: "#{Rails.application.secrets.app_host}#{paypalret_path}",
        amount: params[:val],
        item_name: params[:cardvalue],
        custom:  current_user.id,
        uid: current_user.id,

        notify_url: "#{Rails.application.secrets.app_host}#{paypal_ipn_path}"

    }
   redirect_to  "#{Rails.application.secrets.paypal_host}/cgi-bin/webscr?" + values.to_query

  end

  def upload_payment_data
    @operation_id = params[:upload_payment_data][:operation_id]
    @notification_id = params[:upload_payment_data][:notification_id]
    @user_account = params[:upload_payment_data][:user_account_number]
    @branch_name = params[:upload_payment_data][:branch_name]
    @payment_receipt = params[:upload_payment_data][:payment_receipt]
    @card_data = Card.where(operation_id: @operation_id).first
    @card_data.update(user_account_number: @user_account , branch_name: @branch_name , payment_receipt: @payment_receipt)
    @smstext = "required_data_for_deposit/#{@card_data.value}/USD_to_your_account/has_been_successfully_uploaded/and_is_being_reviewed_by_the_admin/we_will_notify_you"
    # @smstext = "تم رفع بيانات الإيداع بنجاح وجارى مراجعتها من قبل الأدمن ، سوف نعلمك عند تمام تنفيذها وإيداع المبلغ في حسابك"
    access_notification(@smstext,@notification_id)
    redirect_back fallback_location: root_path
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_card
      @card = Card.find(params[:id])
    end

    def access_notification(smstext,opid)
      @opid = opid
      @smstext = smstext
      @splitxt = @smstext.split('/')
      @smstext_translation = t("mails.#{@splitxt[0]}") + "#{@splitxt[1]}" + t("mails.#{@splitxt[2]}") + t("mails.#{@splitxt[3]}") + t("mails.#{@splitxt[4]}") + t("mails.#{@splitxt[5]}")
      @user_notification_setting = NotificationsSetting.where(user_id: current_user.id.to_i).first
      #@mail_text = "#{@opid}" + "/" + @smstext
      
      Notification.create(user_id: current_user.id ,title: "Recharge Wallet Balance", description: @smstext , notification_type: @user_notification_setting.money_transactions ,controller: "money_ops", opid: @opid)
   
      if @user_notification_setting.money_transactions == 3
        SMSNotification.sms_notification_setting(current_user.telephone,@smstext_translation)
        SmsLog.create(:user_id => current_user.id, :pinid => @smstext_translation,:status => 1,:sms_type => 3)
        EmailNotification.email_notification_setting(user_mail:current_user.email,subject:'Recharge Wallet Balance',text:@opid)
      elsif @user_notification_setting.money_transactions == 2
        SMSNotification.sms_notification_setting(@user.telephone,@smstext_translation)
        SmsLog.create(:user_id => current_user.id, :pinid => @smstext_translation,:status => 1,:sms_type => 3)
      elsif @user_notification_setting.money_transactions == 1
        EmailNotification.email_notification_setting(user_mail:current_user.email,subject:'Recharge Wallet Balance',text:@opid)
      end

    end
    
    # Never trust parameters from the scary internet, only allow the white list through.
    def card_params
      params.require(:card).permit(:operation_id, :number, :value, :status, :expired_at, :user_id, :invoice_id, :country_id, :card_type, :user_account_number, :note, :branch_name)
    end
end
