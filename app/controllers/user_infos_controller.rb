class UserInfosController < ApplicationController
  before_action :set_user_info, only: [:show, :edit, :update, :destroy]
  before_action :require_login

  
  
  def create
    begin
      @user_info = UserInfo.new(user_info_params)
      respond_to do |format|
        if @user_info.save
          format.html { redirect_to @user_info, notice: 'User info was successfully created.' }
          format.json { render :show, status: :created, location: @user_info }
        else
          format.html { render :new }
          format.json { render json: @user_info.errors, status: :unprocessable_entity }
        end
      end
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end

  def activate_account
    begin
      @user_infos = UserInfo.where("user_id =?", current_user.id).first
      @current_user_nationalid = NationalidVerification.where("user_id =?", current_user.id.to_i).first
      @current_user_address = AddressVerification.where("user_id =?", current_user.id.to_i).first
      @current_user_selfie = SelfieVerification.where("user_id =?", current_user.id.to_i).first


      @nationalid_verification = @current_user_nationalid 
      if @nationalid_verification == nil
        @nationalid_verification = NationalidVerification.new
      end

      @selfie_verification = @current_user_selfie 
      if @selfie_verification == nil
        @selfie_verification = SelfieVerification.new
      end

      @address_verification =  @current_user_address
      if @address_verification  == nil
        @address_verification  = AddressVerification.new
      end

      render layout: false
      
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end

  # POST a new pin code SMS and send it to user
  # @return [pinId] SMS return pinId that was coming from the SMS provider.
  def send2fasms
    begin
      @user_country_code = current_user.country.Phone_code
      @tel = @user_country_code.to_s + params[:phone]
      SMSJob.perform_async(tel:@tel ,user_id: current_user.id)
      render :json => { 'message': 'true', 'tel': params[:phone] }
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end

  def send2favoice
    begin
      @user_country_code = current_user.country.Phone_code
      @tel = @user_country_code.to_s + params[:phone]
      SMSJob.perform_async(tel:@tel,user_id: current_user.id,voice: 1)
      render :json => { 'message': 'true', 'tel': params[:phone] }
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end

  def resendsms
    begin
      @applications_list = SMSService.new(user_id: current_user.id).call
      render :json => { 'message': @applications_list, 'tel': params[:phone] }
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end

  # POST verify SMS code
  # @return [verified] Code Verification status(ture , false).
  def verifysms_code
    begin
      @code = params[:code].to_i
      @phone = params[:tel].to_s
      @applications_list = SMSService.new(code:@code,user_id: current_user.id).call
      @userinfo = UserInfo.where("user_id =?", current_user.id ).first
      if @applications_list["verified"] ==  true
        if (current_user.update(:telephone => @phone))
          @userinfo = UserInfo.where("user_id =?", current_user.id ).first
          if @userinfo != nil 
            @userinfo.update(:mobile => 1 )
            if @userinfo.address_verification_id != 0 and @userinfo.nationalid_verification_id != 0 and @userinfo.selfie_verification_id != 0
              @userinfo.update(:status => "Verified")
              User.set_usergroup_for_verified_users(current_user)
            end
          else
            UserInfo.create(:user_id => current_user.id ,:mobile => 1, :address_verification_id => 0, :nationalid_verification_id => 0, :selfie_verification_id => 0, :status => "UnVerified")
          end
          @message = "true"
        else
          @message = "true"
        end
      else 
        @message = "false"
      end    
      render :json => { 'message': @message, 'tel': @phone }
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end


    private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_info
      @user_info = UserInfo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_info_params
      params.require(:user_info).permit(:user_id, :mobile, :address_verification_id, :nationalid_verification_id, :selfie_verification_id, :status)
    end
end
