class ApplicationController < ActionController::Base
  include Clearance::Controller
  include Response
  include ExceptionHandler
  before_action :check2fa ,except: [:get_two_factor, :post_two_factor, :confirmmail, :unlockaccount, :send_confirmation_email,:fast_pay , :fast_pay_post, :trust_user, :users_blogs, :catblogs, :display_blog,:sitemap,:payers_blogs_categories,:search_blog_post,:search_blog,:testmail]
  before_action :check_active_session ,except: [:get_two_factor, :post_two_factor, :confirmmail, :unlockaccount, :send_confirmation_email, :lock_page, :check_to_unlock,:fast_pay , :fast_pay_post, :trust_user , :users_blogs , :catblogs ,:display_blog, :sitemap,:payers_blogs_categories,:search_blog_post,:search_blog,:testmail], :unless => :api_request?
  before_action :check_password_expiration ,except: [:reset_expired_password, :post_reset_expired_password, :get_two_factor, :post_two_factor, :confirmmail, :unlockaccount, :send_confirmation_email, :lock_page, :check_to_unlock,:fast_pay , :fast_pay_post, :trust_user, :users_blogs, :catblogs,:display_blog,:sitemap,:payers_blogs_categories,:search_blog_post,:search_blog,:testmail], :unless => :api_request?
  before_action :set_locale
  before_action :clear_privacy_seesion,except: [:secure_my_account]

  rescue_from ActiveRecord::RecordNotFound, :with => :record_not_found
  layout :setlayout

       #attr_reader :current_user

  def current_user
    if (cookies[:remember_token])
      @cookie = cookies[:remember_token]
      @rem_token = Login.where(remember_token: @cookie).first
      if @rem_token
          @current_user = User.where(id: @rem_token.user_id).first
      else
        cookies.delete(:remember_token)
        cookies.delete(:device_id)
        session[:last_visit] = nil          
        @current_user
      end
    end 
    @current_user
  end
  
  def require_login
    if !(current_user)
      cookies.delete(:remember_token)
      cookies.delete(:device_id)
      session[:last_visit] = nil
      render template: "sessions/new"
    end
  end

  def clear_privacy_seesion
    session[:authenticated] = "false"
  end   

  def check2fa
    if  session[:verify_code] == false
      if current_user
        redirect_to two_factor_path
      else 
        render template: "sessions/new"
      end
    end
  end

  def check_active_session
    if current_user
      @session_log = Login.where("user_id = ? and device_id = ? ", current_user.id,cookies[:device_id]).first
      if @session_log and (@session_log.ip_address == request.remote_ip)
        if (session[:last_visit] == nil or session[:last_visit] <= 1.hours.ago)
            before_sign_out
            sign_out
            redirect_to sign_in_path
        elsif (session[:last_visit] >= 15.minutes.ago)
            if (session[:last_visit] <= 5.minutes.ago)
              session[:last_visit] = Time.now
            end
        else
            redirect_to lock_page_path 
        end
      else
        before_sign_out
        sign_out
        redirect_to sign_in_path
      end
    else
      redirect_to sign_in_path
    end
  end

  def check_password_expiration
    if current_user
      if (current_user.password_changed_at < 1.month.ago) # prevent user from logging if his password expired
        flash.now.notice = 'Your Password has been expired, please reset your password.'
        redirect_to reset_expired_password_path
      end
    else
      redirect_to sign_in_path
    end

  end

  def set_locale

  if current_user
    locale = params[:locale].to_s.strip.to_sym
    I18n.locale = I18n.available_locales.include?(locale) ?
        locale :
        I18n.locale = current_user.language
      end
  end

  def before_sign_out
    @user_sessions = Login.where(:remember_token => cookies[:remember_token], :user_id => current_user.id).first
    if (@user_sessions  != nil)
      @user_sessions.destroy
    end
    cookies.delete(:device_id)
    session[:last_visit] = nil
  end

  def record_not_found
    redirect_to error_page_path , notice: "not allowed" 
  end

  
  protected
  def authenticate_request!
    @current_user = (AuthorizeApiRequest.new(request.headers).call)[:user]
   # unless user_id_in_token?
      # render json: { errors: ['Not Authenticated'] }, status: :unauthorized
    #  return
    #end
  #  @current_user = User.find(auth_token[:user_id])
  #rescue JWT::VerificationError, JWT::DecodeError
   # render json: { errors: ['Not Authenticated'] }, status: :unauthorized
  end

  private
  def http_token
      @http_token ||= if request.headers['Authorization'].present?
        request.headers['Authorization'].split(' ').last
      end
  end

  def auth_token
    @auth_token ||= JsonWebToken.decode(http_token)
  end

  def user_id_in_token?
    http_token && auth_token && auth_token[:user_id].to_i
  end

  def setlayout
    if current_user 
        "admin_layout"
    end
  end

  def api_request?
    request.format.json?
  end


  # def url_after_denied_access_when_signed_out
  #   '/'
  #end

  #def sign_in(user)
   # user.reset_remember_token! if user
   # super
  #end

  #users#url_after_create
  # def url_after_create
  #   '/'
  #end

  #sessions#url_for_signed_in_users
  # def url_for_signed_in_users
  #   '/'
  #end

  #sessions#url_after_create
  # def url_after_create
  #   '/'
  #end

  #passwords#url_after_update
  # def url_after_update
  #   '/'
  #end

end
