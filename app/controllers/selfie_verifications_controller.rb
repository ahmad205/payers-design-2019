class SelfieVerificationsController < ApplicationController
  before_action :set_selfie_verification, only: [:show, :edit, :update, :destroy]
  before_action :require_login



  # create new Selfie Verification
  # @param [Blob] avatar
  # @return [Integer] id
  # @return [String] username
  # @return [Blob] avatar
  # @return [Integer] status
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def create
    begin
      @selfie_verification = SelfieVerification.new(selfie_verification_params)
      @selfie_verification.user_id = current_user.id
      if params[:selfie_verification][:avatar] != nil
        @selfie_verification.status = "Pending"
      else
        @selfie_verification.status = "UnVerified"
      end
      respond_to do |format|
        if @selfie_verification.save
          if @selfie_verification.status == "Pending"
            format.html { redirect_to activate_account_path}
          else
            format.html { redirect_to dashboard_path }
          end
          format.json { render :show, status: :created, location: @selfie_verification }
          AuditlogJob.perform_async(user_id: current_user.id, user_name: current_user.uuid, action_type: "Upload Selfie verifications", action_meta: "Selfie verification was successfully created.", ip: request.env['REMOTE_ADDR'])
        else
          format.html { redirect_to activate_account_path, notice: @selfie_verification.errors }
          format.json { render json: @selfie_verification.errors, status: :unprocessable_entity }
        end
      end
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end

  # edit Selfie Verification,
  # only admins can edit status and note.
  # @param [Blob] avatar
  # @param [Integer] status
  # @param [String] note
  # @return [Integer] id
  # @return [String] username
  # @return [Blob] avatar
  # @return [Integer] status
  # @return [String] note
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def update
    begin
      if (params[:selfie_verification][:avatar] != nil) or (@selfie_verification.avatar.attached? == true)
        @selfie_verification.status = "Pending"
      else
        @selfie_verification.status = "UnVerified"
      end
      respond_to do |format|
        if @selfie_verification.update(selfie_verification_params)
          if @selfie_verification.status == "Pending"
            format.html { redirect_to activate_account_path }
          else
            format.html { redirect_to dashboard_path }
          end
          format.json { render :show, status: :ok, location: @selfie_verification }
          AuditlogJob.perform_async(user_id: current_user.id, user_name: current_user.uuid, action_type: "Edit Selfie verifications", action_meta: "Selfie verification was successfully updated.", ip: request.env['REMOTE_ADDR'])
        else
          format.html { redirect_to activate_account_path, notice: @selfie_verification.errors }
          format.json { render json: @selfie_verification.errors, status: :unprocessable_entity }
        end
      end
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end

  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_selfie_verification
      @selfie_verification = SelfieVerification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def selfie_verification_params
      params.require(:selfie_verification).permit(:user_id, :note, :avatar, :status)
    end
end
