class NotificationsController < ApplicationController
  before_action :set_notification, only: [:show, :destroy, :notification_read]


  # GET a Specific existing notification
  # @param [Integer] id notification unique ID.
  # @return [id] notification unique ID.
  # @return [user_id] notification user unique ID.
  # @return [title] notification title.
  # @return [description] notification content.
  # @return [notification_type] notification service type(1 for mail ,2 for message ,3 for mail & message).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def show
  end

  # GET list of notifications log of a specific user
  # @param [Integer] user_id notification user unique ID.
  # @return [id] notification unique ID.
  # @return [user_id] notification user unique ID.
  # @return [title] notification title.
  # @return [description] notification content.
  # @return [notification_type] notification service type(1 for mail ,2 for message ,3 for mail & message).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def user_notifications
    @user_notifications = Notification.where(user_id: current_user.id.to_i).all.order(created_at: :DESC).paginate(:page => params[:page], :per_page => 5)
    render layout: false
  end
  
  # GET a spacific notification data and search for any existing notification
  # @param [String] search search word that you want to find (title , description).
  # @param [Date] searchdatefrom search created at date from.
  # @param [Date] searchdateto search created at date to.
  # @param [Integer] notification_type notification service type(1 for mail ,2 for message ,3 for mail & message).
  # @param [Integer] search_user search user ID than you want.
  # @return [id] notification unique ID.
  # @return [user_id] notification user unique ID.
  # @return [title] notification title.
  # @return [description] notification content.
  # @return [notification_type] notification service type(1 for mail ,2 for message ,3 for mail & message).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def search

    @search = params[:search]
    @searchdatefrom = params[:searchdatefrom]
    @searchdateto = params[:searchdateto]
    @notification_type = params[:notification_type]

    @notifications = Notification.where(user_id: current_user.id.to_i).all
    @notifications = @notifications.where("notifications.created_at >= ? ", @searchdatefrom)   if  @searchdatefrom.present?
    @notifications = @notifications.where("notifications.created_at <= ? ", @searchdateto)   if  @searchdateto.present?
    @notifications = @notifications.where("notifications.created_at <= ? AND notifications.created_at >= ? ", @searchdateto,@searchdatefrom)   if  (@searchdateto.present? and  @searchdatefrom.present?)
    @notifications = @notifications.where(["title LIKE  ? OR description LIKE  ?","%#{@search}%","%#{@search}%"]).distinct.all if  @search.present?
    @notifications = @notifications.where("notifications.notification_type = ? ", @notification_type)   if  @notification_type.present?

  end
 
  def make_all_read
    @notifications = Notification.where(user_id: current_user.id.to_i).all
    @notifications.update_all(read:1)
    render json: {'success' => "done"}
  end

  def notification_read
    @notification.update(read:1)
    render json: {'success' => "done"}
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_notification
      @notification = Notification.where(user_id: current_user.id.to_i, id: params[:id]).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def notification_params
      params.require(:notification).permit(:user_id, :title, :description, :notification_type, :read, :opid, :controller, :admin_id)
    end
end
