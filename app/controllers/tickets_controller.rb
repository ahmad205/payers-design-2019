class TicketsController < ApplicationController
  before_action :set_ticket, only: [:show, :edit, :update, :destroy]

  # GET list of all tickets and display it
  # @return [id] ticket unique ID (Created automatically).
  # @return [number] the ticket number.
  # @return [title] the ticket title.
  # @return [content] the ticket content.
  # @return [attachment] the ticket attachment.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def index
    @tickets = Ticket.where(:user_id => current_user.id.to_i).all
    @department = TicketDepartment.all
    @support_status = Setting.where(key: ["support_status","support_msg"]).all
    @random_number = SecureRandom.rand(100000...999999)
  end


  # GET a spacific ticket and display it
  # @param  [Integer] id ticket unique ID.
  # @return [number] the ticket number.
  # @return [title] the ticket title.
  # @return [content] the ticket content.
  # @return [attachment] the ticket attachment.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def show
    if @ticket.user_id == current_user.id
      @ticket_reply = TicketReply.where(:ticket_id => params[:id]).all
    else
      redirect_back fallback_location: root_path, notice: 'You do not have permission to do this'  
    end
  end

  # POST a new ticket and save it
  # @return [id] ticket unique ID (Created automatically).
  # @return [number] the ticket number.
  # @return [title] the ticket title.
  # @return [content] the ticket content.
  # @return [attachment] the ticket attachment.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def create
    @ticket = Ticket.new(ticket_params)
    @department = TicketDepartment.all
    @ticket.user_id = current_user.id.to_i
    @ticket.status = 0
    respond_to do |format|
      if @ticket.save
        format.html { redirect_to tickets_url, notice: 'Ticket was successfully created.' }
        format.json { render :show, status: :created, location: tickets_url }
      else
        format.html { redirect_to tickets_url, notice: "Ticket wasn't successfully created." }
        format.json { render json: @ticket.errors, status: :unprocessable_entity }
      end
    end
  end

  # Close a specific ticket
  # @param [Integer] id ticket unique ID.
  def close_ticket
    @ticket = Ticket.where(:id => params[:id]).first
    if @ticket.user_id == current_user.id
      @ticket.update(status: 3)
      @smstext = "You Payers Ticket with number #{@ticket.number} was successfully Closed."
      @title = "Tickets Status Updates"
      @notification = access_notification(@smstext,@title,"tickets",@ticket.id)
      redirect_back fallback_location: root_path, notice: 'Ticket successfully Closed'
    else
      redirect_back fallback_location: root_path, notice: 'You do not have permission to do this'
    end
  end

  def report_ticket
    @ticket = Ticket.where(:id => params[:id]).first
    if @ticket.user_id == current_user.id
      @ticket.update(priority: 5)
      redirect_back fallback_location: root_path, notice: 'Ticket successfully reported'
    else
      redirect_back fallback_location: root_path, notice: 'You do not have permission to do this'
    end
  end

  def evaluate_ticket
    @ticket = Ticket.where(id: params[:ticket_id].to_i).first
    @ticket.update(evaluation: params[:evaluation])
    redirect_back fallback_location: root_path, notice: 'Thanks For Your Evaluation.'
  end 

  # POST a new reply to a ticket and save it
  # @return [ticket_id] ticket unique ID.
  # @return [content] the ticket replies content.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def create_reply
    ticket_reply = TicketReply.new(ticket_reply_params)
    ticket_reply.sender_type = 1
    ticket_reply.sender_id = current_user.id
    if ticket_reply.save
      @ticket = Ticket.where(id: ticket_reply.ticket_id).first
      @ticket.update(responded: 1,status: 1)
      @smstext = "Your reply to the ticket with number #{@ticket.number} was successfully Added."
      Notification.create(user_id: current_user.id ,title: 'Tickets Replies', description: @smstext , notification_type: 0,controller: "tickets", opid:@ticket.id)
    redirect_back fallback_location: root_path, notice: 'Ticket reply was successfully created.'
    else 
      redirect_back fallback_location: root_path, notice: ticket_reply.errors

    end
  end




  # # GET an existing ticket and edit params
  # # @param [String] number the ticket number.
  # # @param [String] title the ticket title.
  # # @param [String] content the ticket content.
  # # @param [String] attachment the ticket attachment.
  # def edit
  #   @department = TicketDepartment.all
  # end

  # # Change an existing ticket params(number,title,content,attachment)
  # # @return [id] ticket unique ID.
  # # @return [number] the ticket number.
  # # @return [title] the ticket title.
  # # @return [content] the ticket content.
  # # @return [attachment] the ticket attachment.
  # # @return [created_at] Date created.
  # # @return [updated_at] Date Updated.
  # def update
  #   respond_to do |format|
  #     if @ticket.update(ticket_params)
  #       format.html { redirect_to @ticket, notice: 'Ticket was successfully updated.' }
  #       format.json { render :show, status: :ok, location: @ticket }
  #     else
  #       format.html { render :edit }
  #       format.json { render json: @ticket.errors, status: :unprocessable_entity }
  #     end
  #   end
  # end






  # DELETE /tickets/1
  # DELETE /tickets/1.json
  def destroy
    @ticket.destroy
    respond_to do |format|
      format.html { redirect_to tickets_url, notice: 'Ticket was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ticket
      @ticket = Ticket.find(params[:id])
    end

    def access_notification(smstext,title,control,opid)

      @smstext = smstext
      @title = title
      @user_notification_setting = NotificationsSetting.where(user_id: current_user.id.to_i).first
        
      Notification.create(user_id: current_user.id ,title: @title, description: @smstext , notification_type: @user_notification_setting.help_tickets_updates ,controller: control , opid: opid)
        
      if @user_notification_setting.help_tickets_updates == 3
        SMSNotification.sms_notification_setting(current_user.telephone,@smstext)
        SmsLog.create(:user_id => current_user.id, :pinid => @smstext,:status => 1,:sms_type => 3)
        EmailNotification.email_notification_setting(user_mail:current_user.email,subject:@title,text:@smstext)
      elsif @user_notification_setting.money_transactions == 2
        SMSNotification.help_tickets_updates(current_user.telephone,@smstext)
        SmsLog.create(:user_id => current_user.id, :pinid => @smstext,:status => 1,:sms_type => 3)
      elsif @user_notification_setting.help_tickets_updates == 1
        EmailNotification.email_notification_setting(user_mail:current_user.email,subject:@title,text:@smstext)
      end

    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def ticket_params
      params.require(:ticket).permit(:number, :title, :content, :attachment, :status ,:evaluation, :user_id, :priority, :cc_recipient)
    end

    def ticket_reply_params
      params.require(:ticket_reply).permit(:content, :ticket_id)
    end
end
