class VerificationsController < ApplicationController
  before_action :require_login , only: [:show, :edit, :update, :destroy,:index]
  before_action :set_verification, only: [:show, :edit, :update, :destroy]



  # POST /verifications
  # POST /verifications.json
  def create
    begin
      @verification = Verification.new(verification_params)
      respond_to do |format|
        if @verification.save
          format.html { redirect_to @verification, notice: 'Verification was successfully created.' }
          format.json { render :show, status: :created, location: @verification }
        else
          format.html { render :new }
          format.json { render json: @verification.errors, status: :unprocessable_entity }
        end
      end
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end

  # PATCH/PUT /verifications/1
  # PATCH/PUT /verifications/1.json
  def update
    begin
      respond_to do |format|
        if @verification.update(verification_params)
          format.html { redirect_to @verification, notice: 'Verification was successfully updated.' }
          format.json { render :show, status: :ok, location: @verification }
        else
          format.html { render :edit }
          format.json { render json: @verification.errors, status: :unprocessable_entity }
        end
      end
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end

  # DELETE /verifications/1
  # DELETE /verifications/1.json
  def destroy
    begin
      @verification.destroy
      respond_to do |format|
        format.html { redirect_to verifications_url, notice: 'Verification was successfully destroyed.' }
        format.json { head :no_content }
      end
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_verification
      @verification = Verification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def verification_params
      params.require(:verification).permit(:user_id, :email_confirmation_token, :email_confirmed_at)
    end
end
