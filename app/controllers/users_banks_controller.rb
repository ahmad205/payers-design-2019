class UsersBanksController < ApplicationController
  before_action :set_users_bank, only: [:show, :edit, :update, :destroy]
  before_action :require_login

  # GET list of all bank accounts and display it
  # @return [id] bank account unique ID (Created automatically).
  # @return [bank_id] the bank id from banks model.
  # @return [user_id] id of bank account's owner.
  # @return [account_name] the bank account name.
  # @return [account_number] the bank account number.
  # @return [active] bank account status.
  # @return [is_iban] bank type swift or iban.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def index
    begin
      @users_banks = UsersBank.where(user_id: current_user.id, active: true).includes(:bank).all
      @users_bank = UsersBank.new
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end

  

  # GET a new bank account
  # @param [Integer] id bank unique ID (Created automatically).
  # @param [String] bank_name the name of the general bank if create new general bank.
  # @param [String] branch_name the branch name of the bank if create new general bank.
  # @param [String] bank_code the bank code if create new general bank.
  # @param [String] country_id the bank country id if create new general bank.
  # @param [boolean] verified the bank status, default false until admin approve it, if create new general bank.
  # @param [boolean] is_iban_bank the generalbank status, if create new general bank.
  # @param [Integer] bank_id the general bank id.
  # @param [Integer] user_id id of bank account's owner.
  # @param [String] account_name the bank account name.
  # @param [String] account_number the bank account number.
  # @param [boolean] active the bank status.
  # @param [String] is_iban the bank type 1 for iban 0 for swift.
  def new
    @users_bank = UsersBank.new
  end

  

  # POST a new user bank account and save it
  # @return [id] user bank unique ID (Created automatically).
  # @return [bank_id] the general bank id.
  # @return [bank_name]  the name of the general bank. 
  # @return [branch_name]  the branch name of the bank. 
  # @return [bank_code]  the bank code .
  # @return [country_id]  the bank country id. 
  # @return [verified]  the bank status, default false until admin approve it.
  # @return [is_iban]  the generalbank status.
  # @return [user_id] id of bank account's owner.
  # @return [account_name] the bank account name.
  # @return [account_number] the bank account number.
  # @return [active] the bank status.
  # @return [is_iban] the bank type 1 for iban 0 for swift.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def create
    begin
      @users_bank = UsersBank.new(users_bank_params)
      @users_bank.user_id = current_user.id
      if (params[:users_bank][:bank_id] == "create a new bank")
        @bank = Bank.create(:bank_name => params[:users_bank][:bank_name], :country_id => current_user.country_id, :verified => 0)
        @users_bank.bank_id = @bank.id
      end
      if @users_bank.save
        if @users_bank.is_default == true
          UsersBank.where("id != ? and user_id =? and is_default =?",  @users_bank.id , current_user.id, true).update_all(is_default: false)
        end
        redirect_back fallback_location: root_path
      else
        redirect_back fallback_location: root_path, notice: "Bank account wasn't successfully created."
      end
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end

  # Change an existing user bank params(account_name,account_number,active,is_iban)
  # @return [id] user's bank unique ID.
  # @return [account_name] the bank account name.
  # @return [account_number] the bank account number.
  # @return [active] the bank status.
  # @return [is_iban] the bank type 1 for iban 0 for swift.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def update
    begin
      respond_to do |format|
        @users_bank.bank_id = @users_bank.bank_id
        @is_used = Withdraw.where(user_id: current_user.id, bank_id: @users_bank.id).first
        if @is_used == nil
          if @users_bank.update(users_bank_params)
            if @users_bank.is_default == true
              UsersBank.where("id != ? and user_id =? and is_default =?",  @users_bank.id , current_user.id, true).update_all(is_default: false)
            end
            format.html { redirect_to users_banks_path }
            format.json { render :index, status: :ok, location: @users_bank }
          else
            format.html { render :index }
            format.json { render json: @users_bank.errors, status: :unprocessable_entity }
          end
        else
          @new_bank = UsersBank.create(account_number: params[:users_bank][:account_number], branch_name: params[:users_bank][:branch_name], swift_code: params[:users_bank][:swift_code], is_default: params[:users_bank][:is_default], bank_id:  @users_bank.bank_id, user_id: current_user.id)
          if @new_bank   
            if @new_bank.is_default == true
              UsersBank.where("id != ? and user_id =? and is_default =?",  @new_bank.id , current_user.id, true).update_all(is_default: false)
            end
            @users_bank.update(:active => false)
            format.html { redirect_to users_banks_path }
            format.json { render :index, status: :ok, location: @users_bank }
          else
            format.html { render :index }
            format.json { render json: @users_bank.errors, status: :unprocessable_entity }
          end
        end
      end
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end

  def bank_details
    begin
      @bank = Bank.where(:id => params[:id]).first     
      render :json => { bank: @bank }
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end

  # DELETE /users_banks/1
  # DELETE /users_banks/1.json
  def destroy
    begin
      @users_bank.update(:active => false)
      respond_to do |format|
        format.html { redirect_to users_banks_url}
        format.json { head :no_content }
      end
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_users_bank
      @users_bank = UsersBank.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def users_bank_params
      params.require(:users_bank).permit(:bank_id, :user_id, :account_number, :active, :branch_name, :swift_code, :is_default)
    end
end
