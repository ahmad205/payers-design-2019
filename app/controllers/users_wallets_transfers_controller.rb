class UsersWalletsTransfersController < ApplicationController
  before_action :set_users_wallets_transfer, only: [:show, :edit, :update, :destroy]

  # GET /users_wallets_transfers
  # GET /users_wallets_transfers.json
  def index
    @users_wallets_transfers = UsersWalletsTransfer.where("user_id = ? OR user_to_id = ?", current_user.id,current_user.id).all
  end

  # GET /users_wallets_transfers/1
  # GET /users_wallets_transfers/1.json
  def show
    if @users_wallets_transfer.user_to_id != current_user.id
      @user_data = User.where(id: @users_wallets_transfer.user_to_id).first
      @trans_type = "ارسال"
    else
      @user_data = User.where(id: @users_wallets_transfer.user_id).first
      @trans_type = "استقبال"
    end

    if @users_wallets_transfer.address_id
      @sender_address = Address.where(id: @users_wallets_transfer.address_id).pluck(:address).first
    end

    if @users_wallets_transfer.approve == 0 
      @time = @users_wallets_transfer.created_at + @users_wallets_transfer.hold_period.days + 2.hours
    else
      @time = @users_wallets_transfer.updated_at + 2.hours
    end

    if @time.strftime("%p") == "PM"
      @mor = "مساءا"
    else 
      @mor = "صباحا"
    end
    @month = UsersWalletsTransfer.checkmonth(@time.strftime("%m"))
    @operation_notification = Notification.where(user_id: current_user.id.to_i, opid: @users_wallets_transfer.id , controller:"users_wallets_transfers").all.order('created_at DESC').limit(3)

  end

  # GET /users_wallets_transfers/new
  def new
    @users_wallets_transfer = UsersWalletsTransfer.new
  end

  # POST /users_wallets_transfers
  # POST /users_wallets_transfers.json
  def create
    @users_wallets_transfer = UsersWalletsTransfer.new(users_wallets_transfer_params)

    respond_to do |format|
      if @users_wallets_transfer.save
        format.html { redirect_to @users_wallets_transfer, notice: 'Users wallets transfer was successfully created.' }
        format.json { render :show, status: :created, location: @users_wallets_transfer }
      else
        format.html { render :new }
        format.json { render json: @users_wallets_transfer.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @users_wallets_transfer.update(users_wallets_transfer_params)
        @user_to = User.where(id: @users_wallets_transfer.user_id.to_i).first
        @title = "wallet transfer balance"
        @smstext = "You have updated your order on payers with number #{@users_wallets_transfer.operation_id} to status #{params[:users_wallets_transfer][:service_status]} to user #{@user_to.firstname} #{@user_to.lastname}"
        @smstext_to = "Your Operation on payers with number #{@users_wallets_transfer.operation_id} was successfully updated to status #{params[:users_wallets_transfer][:service_status]} by user #{current_user.firstname} #{current_user.lastname}"
        Notification.create(user_id: current_user.id ,title: "Balance transfer", description: @smstext , notification_type: 1)
        Notification.create(user_id: @user_to.id ,title: "Balance transfer", description: @smstext_to , notification_type: 1)
        EmailNotification.email_notification_setting(user_mail:@user_to.email,subject:@title,text:@smstext_to)
        EmailNotification.email_notification_setting(user_mail:current_user.email,subject:@title,text:@smstext)
        format.html { redirect_to @users_wallets_transfer, notice: 'Order Status was successfully updated.' }
        format.json { render :show, status: :ok, location: @users_wallets_transfer }
      else
        format.html { redirect_to @users_wallets_transfer, notice: 'Sorry something went wrong.' }
        format.json { render json: @users_wallets_transfer.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_users_wallets_transfer
      @users_wallets_transfer = UsersWalletsTransfer.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def users_wallets_transfer_params
      params.require(:users_wallets_transfer).permit(:user_id, :user_to_id, :transfer_method, :transfer_type, :hold_period, :amount, :ratio, :approve, :note, :address_id ,:service_status)
    end
end
