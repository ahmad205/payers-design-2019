class CardsCategoriesController < ApplicationController

  # GET list of a Displayed Cards Categories ordered Desc
  # @return [id] card category unique ID.
  # @return [card_value] card category value (Unique).
  # @return [active] card category display status (1 for show).
  # @return [order] card category unique order (high order for high card category value).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def display_cards_categories
    @categories = CardsCategory.where(:active => 1).all.order(card_value: :desc)
    @cards_status = Setting.where(key: ["cards_status","cards_msg"]).all
  end



  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cards_category
      @cards_category = CardsCategory.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cards_category_params
      params.require(:cards_category).permit(:card_value, :active, :order)
    end
end
