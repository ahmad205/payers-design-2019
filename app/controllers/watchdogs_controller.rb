class WatchdogsController < ApplicationController
  before_action :require_login 
  before_action :set_watchdog, only: [:show, :edit, :update, :destroy]

  

  # show log of specific user
  # works only if current user is an admin
  # @return [String] username
  # @return [String] ipaddress	
  # @return [String] details
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def user_log
    begin
      if current_user.id == params[:id].to_i
        @user_notifications = Notification.where(user_id: current_user.id.to_i,read: nil).all
        @log = Watchdog.where("user_id =? ",params[:id]).where(:operation_type => "sign_in").joins("INNER JOIN users ON users.id = watchdogs.user_id").distinct.all.order('created_at DESC').limit(10)
        render layout: false
      else
        redirect_to root_path , notice: "not allowed" 
      end
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end

  # POST /watchdogs
  # POST /watchdogs.json
  def create
    begin
      @watchdog = Watchdog.new(watchdog_params)
      respond_to do |format|
        if @watchdog.save
          format.html { redirect_to @watchdog, notice: 'Watchdog was successfully created.' }
          format.json { render :show, status: :created, location: @watchdog }
        else
          format.html { render :new }
          format.json { render json: @watchdog.errors, status: :unprocessable_entity }
        end
      end
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end

  # PATCH/PUT /watchdogs/1
  # PATCH/PUT /watchdogs/1.json
  def update
    respond_to do |format|
      if @watchdog.update(watchdog_params)
        format.html { redirect_to @watchdog, notice: 'Watchdog was successfully updated.' }
        format.json { render :show, status: :ok, location: @watchdog }
      else
        format.html { render :edit }
        format.json { render json: @watchdog.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /watchdogs/1
  # DELETE /watchdogs/1.json
  def destroy
    begin
      @watchdog.destroy
      respond_to do |format|
        format.html { redirect_to watchdogs_url, notice: 'Watchdog was successfully destroyed.' }
        format.json { head :no_content }
      end
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_watchdog
      @watchdog = Watchdog.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def watchdog_params
      params.require(:watchdog).permit(:user_id, :logintime, :ipaddress, :lastvisit)
    end
end
