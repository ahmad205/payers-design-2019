class NotificationsSettingsController < ApplicationController
  before_action :set_notifications_setting, only: [:update]

  # Get a spacific user notification setting
  # @return [id] notification setting unique ID.
  # @return [user_id] notification setting user ID.
  # @return [money_transactions] Withdrawal,transfer and remittance transactions(0 for non ,1 for mail ,2 for message ,3 for mail & message).
  # @return [pending_transactions] pending transactions(0 for non ,1 for mail ,2 for message ,3 for mail & message).
  # @return [transactions_updates] current transactions updates(0 for non ,1 for mail ,2 for message ,3 for mail & message).
  # @return [help_tickets_updates] help tickets updates(0 for non ,1 for mail ,2 for message ,3 for mail & message).
  # @return [tickets_replies] new ticket replies(0 for non ,1 for mail ,2 for message ,3 for mail & message).
  # @return [account_login] login to user account(0 for non ,1 for mail ,2 for message ,3 for mail & message).
  # @return [change_password] change password(0 for non ,1 for mail ,2 for message ,3 for mail & message).
  # @return [verifications_setting] when enable or disable two factor auth(0 for non ,1 for mail ,2 for message ,3 for mail & message).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def user_notifications_setting 
    @notifications_setting = NotificationsSetting.where(user_id: current_user.id.to_i).first
  end


  # Change an existing notification settings params
  # @param [Integer] money_transactions_mail enable mail notifications for money transactions(1 for mail).
  # @param [Integer] money_transactions_message enable message notifications for money transactions(2 for message).
  # @param [Integer] pending_transactions_mail enable mail notifications for pending transactions(1 for mail).
  # @param [Integer] pending_transactions_message enable message notifications for pending transactions(2 for message).
  # @param [Integer] transactions_updates_mail enable mail notifications for current transactions updates(1 for mail).
  # @param [Integer] transactions_updates_message enable message notifications for current transactions updates(2 for message).
  # @param [Integer] help_tickets_updates enable mail notifications for help tickets updates(1 for mail).
  # @param [Integer] help_tickets_updates enable message notifications for help tickets updates(2 for message).
  # @param [Integer] tickets_replies enable mail notifications for new ticket replies(1 for mail).
  # @param [Integer] tickets_replies enable message notifications for new ticket replies(2 for message).
  # @param [Integer] account_login enable mail notifications for login to user account(1 for mail).
  # @param [Integer] account_login enable message notifications for login to user account(2 for message).
  # @param [Integer] change_password enable mail notifications for change password(1 for mail).
  # @param [Integer] change_password enable message notifications for change password(2 for message).
  # @param [Integer] verifications_setting enable mail notifications for two factor auth status(1 for mail).
  # @param [Integer] verifications_setting enable message notifications for two factor auth status(2 for message).
  # @return [id] notification setting unique ID.
  # @return [user_id] notification setting user ID.
  # @return [money_transactions] Withdrawal,transfer and remittance transactions(0 for non ,1 for mail ,2 for message ,3 for mail & message).
  # @return [pending_transactions] pending transactions(0 for non ,1 for mail ,2 for message ,3 for mail & message).
  # @return [transactions_updates] current transactions updates(0 for non ,1 for mail ,2 for message ,3 for mail & message).
  # @return [help_tickets_updates] help tickets updates(0 for non ,1 for mail ,2 for message ,3 for mail & message).
  # @return [tickets_replies] new ticket replies(0 for non ,1 for mail ,2 for message ,3 for mail & message).
  # @return [account_login] login to user account(0 for non ,1 for mail ,2 for message ,3 for mail & message).
  # @return [change_password] change password(0 for non ,1 for mail ,2 for message ,3 for mail & message).
  # @return [verifications_setting] when enable or disable two factor auth(0 for non ,1 for mail ,2 for message ,3 for mail & message).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def update
    @result = notification_params
    @notifications_setting.money_transactions = @result[0]
    @notifications_setting.pending_transactions = @result[1]
    @notifications_setting.transactions_updates = @result[2]
    @notifications_setting.help_tickets_updates = @result[3]
    @notifications_setting.tickets_replies = @result[4]
    @notifications_setting.account_login = @result[5]
    @notifications_setting.change_password = @result[6]
    @notifications_setting.verifications_setting = @result[7]
      if @notifications_setting.save
        AuditlogJob.perform_async(user_id: current_user.id, user_name: current_user.uuid, action_type: "Edit Notifications setting", action_meta: "Notifications setting was successfully updated", ip: request.env['REMOTE_ADDR'])
        redirect_to(user_notifications_setting_path,:notice => "Notifications setting was successfully updated")
      else
        redirect_to(user_notifications_setting_path,:notice => "Something went wrong")
      end

  end

  def add_defualt_notifications
    @notification = NotificationsSetting.find(params[:id])
    @defualt = NotificationsSetting.where(:user_id => 0).first
    @notification.money_transactions = @defualt.money_transactions
    @notification.pending_transactions = @defualt.pending_transactions
    @notification.transactions_updates = @defualt.transactions_updates
    @notification.help_tickets_updates = @defualt.help_tickets_updates
    @notification.tickets_replies = @defualt.tickets_replies
    @notification.account_login = @defualt.account_login
    @notification.change_password = @defualt.change_password
    @notification.verifications_setting = @defualt.verifications_setting
    @notification.save
    AuditlogJob.perform_async(user_id: current_user.id, user_name: current_user.uuid, action_type: "Edit Notifications setting", action_meta: "Notifications setting was successfully updated", ip: request.env['REMOTE_ADDR'])
    redirect_to(user_notifications_setting_path,:notice => "Notifications setting was successfully updated")
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_notifications_setting
      @notifications_setting = NotificationsSetting.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def notifications_setting_params
      params.require(:notifications_setting).permit(:user_id, :money_transactions, :pending_transactions, :transactions_updates, :help_tickets_updates, :tickets_replies, :account_login, :change_password, :verifications_setting)
    end

    
    def notification_params
      @money_transactions_mail = params[:notifications_setting][:money_transactions_mail].to_i
      @money_transactions_sms = params[:notifications_setting][:money_transactions_sms].to_i
      @pending_transactions_mail = params[:notifications_setting][:pending_transactions_mail].to_i
      @pending_transactions_sms = params[:notifications_setting][:pending_transactions_sms].to_i
      @transactions_updates_mail = params[:notifications_setting][:transactions_updates_mail].to_i
      @transactions_updates_sms = params[:notifications_setting][:transactions_updates_sms].to_i
      @help_tickets_updates_mail = params[:notifications_setting][:help_tickets_updates_mail].to_i
      @help_tickets_updates_sms = params[:notifications_setting][:help_tickets_updates_sms].to_i
      @tickets_replies_mail = params[:notifications_setting][:tickets_replies_mail].to_i
      @tickets_replies_sms = params[:notifications_setting][:tickets_replies_sms].to_i
      @account_login_mail = params[:notifications_setting][:account_login_mail].to_i
      @account_login_sms = params[:notifications_setting][:account_login_sms].to_i
      @change_password_mail = params[:notifications_setting][:change_password_mail].to_i
      @change_password_sms = params[:notifications_setting][:change_password_sms].to_i
      @verifications_setting_mail = params[:notifications_setting][:verifications_setting_mail].to_i
      @verifications_setting_sms = params[:notifications_setting][:verifications_setting_sms].to_i

      @result = NotificationsSetting.usernotification(@money_transactions_mail,@money_transactions_sms,@pending_transactions_mail,
      @pending_transactions_sms,@transactions_updates_mail,@transactions_updates_sms,@help_tickets_updates_mail,
      @help_tickets_updates_sms,@tickets_replies_mail,@tickets_replies_sms,@account_login_mail,@account_login_sms,@change_password_mail,
      @change_password_sms,@verifications_setting_mail,@verifications_setting_sms)
    end
    
end
