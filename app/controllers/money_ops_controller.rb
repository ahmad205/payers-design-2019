class MoneyOpsController < ApplicationController
  before_action :set_money_op, only: [:show,:paypal_ipn,:perfectmoneypost,:perfectmoneyerror]
  skip_before_action :verify_authenticity_token
  # GET list of Money Operations and display it
  # @return [id] Money Operation unique ID (Created automatically).
  # @return [opid] Operation unique number  (Created automatically and must be 12 digits and letters ).
  # @return [optype] Operation type (1 for Sent & 2 for Receive , 3 for withdraw , 4 for remittance).
  # @return [amount] Operation amount.
  # @return [payment_gateway] Operation payment gateway (paypal,bitcoin,...).
  # @return [status] Operation status (0 for pending & 1 for compeleted & 2 for cancelled).
  # @return [payment_date] Payment operation creation date.
  # @return [payment_id] Payment ID for the operation itself.
  # @return [user_id] Operation user id.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def index
    @users_wallets_transfers = UsersWalletsTransfer.where("user_id = ? OR user_to_id = ?", current_user.id,current_user.id).pluck(:operation_id)
    @money_ops = MoneyOp.where("user_id = ? OR opid IN (?)", current_user.id.to_i,@users_wallets_transfers).all.order('created_at DESC')
    @pending_ops = @money_ops.where(status: 0).all.paginate(:page => params[:page], :per_page => 5)
    @cancelled_ops = @money_ops.where(status: 2).all.paginate(:page => params[:page], :per_page => 5)
    @compeleted_ops = @money_ops.where(status: 1).all.paginate(:page => params[:page], :per_page => 5)
  end

  # GET a spacific Money Operation and display it
  # @param [Integer] id Money operation unique ID.
  # @return [opid] Operation unique number (Created automatically and must be 12 digits and letters ).
  # @return [optype] Operation type (1 for Sent & 2 for Receive).
  # @return [amount] Operation amount.
  # @return [payment_gateway] Operation payment gateway (paypal,bitcoin,...).
  # @return [status] Operation status (0 for pending & 1 for compeleted & 2 for cancelled).
  # @return [payment_date] Payment operation creation date.
  # @return [payment_id] Payment ID for the operation itself.
  # @return [user_id] Operation user id.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def show
    if set_money_op.optype == 1 || set_money_op.optype == 2
    @money_data = UsersWalletsTransfer.where("operation_id = ?",set_money_op.opid).first
    @user1 = User.where("id = ?",@money_data.user_id).first
    @user2 = User.where("id = ?",@money_data.user_to_id).first
    @bank_type = ""
    elsif set_money_op.optype == 4 && set_money_op.status == 0
    @remittance_data = Card.where("operation_id = ?",set_money_op.opid).first
    @bank = CompanyBank.where("bank_key = ?",set_money_op.payment_gateway).first
    @bank_type = @bank.bank_category.to_s
    end
    @time = @money_op.updated_at + 2.hours
    if @time.strftime("%p") == "PM"
      @mor = "مساءا"
    else 
      @mor = "صباحا"
    end
    @month = UsersWalletsTransfer.checkmonth(@time.strftime("%m"))
    @operation_notification = Notification.where(opid: @money_op.id , controller:"money_ops").all.order('created_at DESC').limit(3)

  end

  # GET a list of a spacific user bank accounts
  # @param [Integer] user_id user unique ID.
  # @return [id] bank unique ID (Created automatically).
  # @return [bank_name] the bank name.
  # @return [branche_name] the branche name of the bank.
  # @return [swift_code] the bank swift code.
  # @return [country] the bank country.
  # @return [account_name] the bank account name.
  # @return [account_number] the bank account number.
  # @return [user_id] bank account user id.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def withdraw_op
    @user_banks = UsersBank.where(:user_id => current_user.id.to_i, :active => 1).all
    @withdraw_status = Setting.where(key: ["withdraw_status","withdraw_msg"]).all
    @user_country = Country.where(id:current_user.country_id).pluck(:short_code).first
    if @user_country == "EG"
      @currency_name = t('users.EGP')
    elsif @user_country == "SA"
      @currency_name = t('users.SAR')
    else
      @currency_name = t('users.dollar')
    end
    @currency_ratio = Setting.check_currency_ratio(@user_country)
    render layout:false
  end

  # GET a spacific user bank account data
  # @param [Integer] id bank account unique ID.
  # @return [bank_name] the bank name.
  # @return [branche_name] the branche name of the bank.
  # @return [swift_code] the bank swift code.
  # @return [country] the bank country.
  # @return [account_name] the bank account name.
  # @return [account_number] the bank account number.
  # @return [user_id] bank account user id.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def bank_data
    @selectedbank = params[:selectedbank].to_i
    @user_bank = UsersBank.where(:id => @selectedbank ).first
    @bank_data = Bank.where(:id => @user_bank.bank_id).first
    @users_group = UsersGroup.where(:id => current_user.users_group_id ).first
    @result = params[:amount].to_f * (@users_group.withdraw_ratio/100) + @users_group.withdraw_fees
    @message = Withdraw.checkwithdraw(current_user.id.to_i,params[:amount],params[:withdraw_method],params[:selectedbank])
    if @message == ""
      @message = "true"
    end
    render :json => {'userbank': @user_bank, 'bankdata': @bank_data, 'fees': @result ,'message': @message}
  end

  # Create a new withdraw operation
  # @param [Integer] id withdraw unique ID (Created automatically).
  # @param [Integer] user_id the user unique id.
  # @param [Integer] bank_id the user bank account id.
  # @param [String] operation_id the money operation unique id.
  # @param [Float] amount withdraw amount.
  # @param [Float] fees company fees.
  # @param [Integer] status withdraw status (1 for Pending, 2 for Accepted , 3 for Cancelled).
  # @param [String] withdraw_type withdraw bank type (local_bank, electronic_bank , digital_bank).
  # @param [String] note withdraw user note.
  # @param [Date] created_at Date created.
  # @param [Date] updated_at Date Updated.
  def withdrawpost
    @amount = params[:withdrawamount]
    @withdraw_type = params[:withdraw_method]
    @bank_id = params[:bank_id]
    @user_id = current_user.id.to_i
    @payment_gateway = params[:payment_gateway]
    @currentuser = current_user
    @op_type = "withdraw"
    @message = Withdraw.checkwithdraw(@user_id,@amount,@withdraw_type,@bank_id)
    if @message == ""
      # ActiveRecord::Base.transaction do
        @user_wallet = UserWallet.where(:user_id => @user_id).first
        @user_balance = @user_wallet.amount
        @user_new_balance = @user_balance.to_f - @amount.to_f
        @domain = request.base_url
        @user_wallet.update(:amount => @user_new_balance)
        current_user.update(:total_daily_withdraw =>  current_user.total_daily_withdraw.to_f + @amount.to_f, :total_monthly_withdraw => current_user.total_monthly_withdraw.to_f + @amount.to_f, :daily_withdraw_number => current_user.daily_withdraw_number.to_f + 1)
        @operation = MoneyOp.create(optype:3 ,amount:@amount ,payment_gateway:@payment_gateway ,status:0 ,user_id: @user_id, fees:params[:fees].to_f ,payment_date: Time.now)
        @with_op = Withdraw.create(user_id: @user_id ,bank_id:@bank_id , operation_id: @operation.opid , amount:@amount , fees:params[:fees].to_f , status:1 , withdraw_type:@withdraw_type, note:params[:note].to_s)
        AuditlogJob.perform_async(user_id: @user_id, user_name: current_user.uuid, action_type: "create withdraw request", action_meta: "Withdraw request was successfully created and waiting admin confirm", ip: request.env['REMOTE_ADDR'])
        @notification = access_notification(@currentuser,@amount,@payment_gateway,@op_type,"withdraws",@with_op.id)
        redirect_to("#{@domain}/withdraws/#{@with_op.id}")
      # end
    else
      redirect_to(withdraw_op_path,:notice => @message )
    end
  end

  def new_transaction
    render layout: false
  end

  def paypalconfirm

     @timenow = Time.now.to_i
     status = params[:st].to_s
     @uidc = params[:tx].to_s
     @amount = params[:amt]
     @user_wallet = UserWallet.where(:user_id => current_user.id.to_i).first
     @deposit_expenses_setting = Setting.where(:key => "payers_deposit_expenses").first.value.to_i
     @domain = request.base_url
     @currentuser = current_user
     @op_type = "remittance"
     if status == "Completed"
       #calculate paypal ratio of transfer
       @bank= CompanyBank.where("bank_key = 'paypal'").first
       @user_group = UsersGroup.where(:id => current_user.users_group_id.to_i).first         

       if (@deposit_expenses_setting == 1)
        @original_amount = ((((@amount.to_f - @bank.fees - @user_group.deposite_fees) * 100 )/(@bank.ratio + @user_group.deposite_ratio + 100)) - @bank.value_added_tax).to_f 
        @net_card_value =   @original_amount
        @fees = @amount.to_f - @original_amount
      else
        @original_amount = ((((@amount.to_f - @bank.fees) * 100 )/(@bank.ratio + 100))- @bank.value_added_tax).to_f
        @payers_deposit = (@original_amount * (@user_group.deposite_ratio/100) + @user_group.deposite_fees).to_f
        @net_card_value =  @original_amount - @payers_deposit
        @fees = @amount.to_f - @original_amount + @payers_deposit
       end
       

       if @bank.wallet_type == 2         
          @user_new_balance = @user_wallet.transfer_amount.to_f + @net_card_value     
          @user_wallet.update(:transfer_amount => @user_new_balance)
       else  
          @user_new_balance = @user_wallet.amount.to_f + @net_card_value
          @user_wallet.update(:amount => @user_new_balance)
       end
       @new_operation = MoneyOp.create(:optype => 4,:amount => @amount , :fees => @fees ,:payment_gateway => 'paypal' ,:status => 1,:payment_date => DateTime.now, :payment_id => @uidc,:user_id => current_user.id.to_i)
       @new_card =  Card.create(:operation_id => @new_operation.opid ,:value => @original_amount,:expired_at => Date.today + 5.days,:status => 2,:user_id => current_user.id.to_i,:card_type => 1, :net_value => @net_card_value)
       @log = CardsLog.create(:user_id => current_user.id.to_i,:action_type => 2,:ip => request.remote_ip,:card_id => @new_card.id)
       @notification = access_notification(@currentuser,@net_card_value,@bank.bank_key,@op_type,"money_ops",@new_operation.id)
       current_user.update(:total_daily_deposite => current_user.total_daily_deposite + @original_amount, :daily_deposite_number => current_user.daily_deposite_number.to_i + 1)
       AuditlogJob.perform_async(user_id: current_user.id, user_name: current_user.uuid, action_type: "pay through paypal", action_meta: "Balance was successfully added to your account", ip: request.env['REMOTE_ADDR'])
       redirect_to("#{@domain}/money_ops/#{@new_operation.id}")
     end

  end

  def paypal_ipn

    @opid = params[:txn_id].to_s
    @userid = params[:custom].to_i
    status = params[:payment_status].to_s
    @user_wallet = UserWallet.where(:user_id => @userid).first
    @user_balance = @user_wallet.amount
    @paypal_operation = MoneyOp.where(:payment_id => @opid , :payment_gateway => 'paypal').first
    @bank= CompanyBank.where("bank_key = 'paypal'").first

    # if status == "Completed"
    if @paypal_operation == nil && status == "Completed"
      @paypal_operation.update(:status => 1 , :payment_id => params[:txn_id])
      if @bank.wallet_type == 2
        @user_new_balance = @user_wallet.transfer_amount.to_f + @paypal_operation.amount.to_f
        @user_wallet.update(:transfer_amount => @user_new_balance)
      else
        @user_new_balance = @user_wallet.amount.to_f + @paypal_operation.amount.to_f
        @user_wallet.update(:amount => @user_new_balance)
      end

      redirect_to(money_ops_path)
      
    end
    
  end

  def perfectmoney
    @perfectcompany= CompanyBank.where("bank_key = 'perfectmoney'").first   
  end

  def perfectmoneypost

    #recieve the real value of cart  first then the current userid  and split them with ;
    @realcard_userid = params[:PAYMENT_ID].to_s.split("p")
    @op_type = "remittance"
    @deposit_expenses_setting = Setting.where(:key => "payers_deposit_expenses").first.value.to_i
    @currentuser = current_user
 
    if params[:PAYEE_ACCOUNT] != nil && params[:PAYMENT_BATCH_NUM] != nil
        ##calculate paypal ratio of transfer
        @bank= CompanyBank.where("bank_key = 'perfectmoney'").first
        @user_group = UsersGroup.where(:id => current_user.users_group_id.to_i).first         
        @userid = @realcard_userid[1].to_i

        if (@deposit_expenses_setting == 1)
          @original = ((((params[:PAYMENT_AMOUNT].to_f - @bank.fees - @user_group.deposite_fees) * 100 )/(@bank.ratio + @user_group.deposite_ratio + 100)) - @bank.value_added_tax).to_f 
          #@net_card_value =   @original
          @real_cart_value = @realcard_userid[0].to_i
          @fees = params[:PAYMENT_AMOUNT].to_f - @original
        else
          @original = ((((params[:PAYMENT_AMOUNT].to_f - @bank.fees) * 100 )/(@bank.ratio + 100))- @bank.value_added_tax).to_f
          @payers_deposit = (@original * (@user_group.deposite_ratio/100) + @user_group.deposite_fees).to_f
          @real_cart_value =  @realcard_userid[0].to_i - @payers_deposit
          @fees = params[:PAYMENT_AMOUNT].to_f - @original + @payers_deposit
         end

        #@original = (((params[:PAYMENT_AMOUNT].to_f - @bank.fees) * 100 )/(@bank.ratio + 100)).to_f
        #@fees =   params[:PAYMENT_AMOUNT].to_f - @original
        #@userid = @realcard_userid[1].to_i
        #@real_cart_value = @realcard_userid[0].to_i
       
        ActiveRecord::Base.transaction do
          @user_wallet = UserWallet.where(:user_id => current_user.id.to_i).first
          if @bank.wallet_type == 2
            @user_new_balance = @user_wallet.transfer_amount.to_f + @real_cart_value.to_f
            @user_wallet.update(:transfer_amount => @user_new_balance)
          else
            @user_new_balance = @user_wallet.amount.to_f + @real_cart_value.to_f
            @user_wallet.update(:amount => @user_new_balance)
          end
          @domain = request.base_url
          @new_operation = MoneyOp.create(:optype => 4,:amount => params[:PAYMENT_AMOUNT], :fees => @fees ,:payment_gateway => 'perfectmoney' ,:status => 1,:payment_date => DateTime.now, :payment_id => params[:PAYMENT_ID],:user_id => current_user.id.to_i)
          @new_card =  Card.create(:operation_id => @new_operation.opid ,:value => params[:PAYMENT_AMOUNT],:expired_at => Date.today + 5.days,:status => 2,:user_id => current_user.id.to_i,:card_type => 1, :net_value => @real_cart_value)
          @log = CardsLog.create(:user_id => current_user.id.to_i,:action_type => 2,:ip => request.remote_ip,:card_id => @new_card.id)
          @notification = access_notification(@currentuser,params[:PAYMENT_AMOUNT],@bank.bank_key,@op_type,"money_ops",@new_operation.id)
          current_user.update(:total_daily_deposite => current_user.total_daily_deposite + @original, :daily_deposite_number => current_user.daily_deposite_number + 1)
          AuditlogJob.perform_async(user_id: current_user.id, user_name: current_user.uuid, action_type: "pay through perfect", action_meta: "Balance was successfully added to your account", ip: request.env['REMOTE_ADDR'])
          redirect_to("#{@domain}/money_ops/#{@new_operation.id}")
        end
      end
  end

  def perfectmoneyerror
    redirect_to(remittance_path,:notice => 'Payment operation has been cancelled')
  end

  def coinmoney  
    # @api_secret = "sec59eb7e2f46bf97b1e6c1c38a90d1b6ad"
    # order_id = 20593
    # token = "btc"
    # callback_url = "http://localhost:3000/callback/#{order_id}"
    # url = "https://api.paybear.io/v2/#{token}/payment?token=#{@api_secret}"
    # @response = ActiveSupport::JSON.decode(open(url).read)
    # @test = Coinpayments.rates(accepted: 1).delete_if { |_k, v| v["accepted"] == 0 }.keys
    # @test2 = Coinpayments.rates
  end

  def coinmoney_post

    @net_val = params[:coinmoney][:value]
    @currency = params[:coinmoney][:currency]  
    @final_value = params[:coinmoney][:total_money].to_f
    @fees = @final_value - @net_val.to_f
    @currentuser = current_user
    @op_type = "remittance"
    @transaction = Coinpayments.create_transaction(@final_value, 'USD', @currency)
    @new_operation = MoneyOp.create(:optype => 4,:amount => @final_value, :fees => @fees.to_f ,:payment_gateway => @currency ,:status => 0,:payment_date => DateTime.now, :payment_id => @transaction.txn_id,:user_id => current_user.id.to_i)
    @new_card = Card.create(:operation_id => @new_operation.opid ,:value => @net_val,:expired_at => Date.today + 5.days,:status => 3,:user_id => current_user.id.to_i,:card_type => 1)
    @log = CardsLog.create(:user_id => current_user.id.to_i,:action_type => 1,:ip => request.remote_ip,:card_id => @new_card.id)
    @notification = access_notification(@currentuser,@net_val,@currency,@op_type,"money_ops",@new_operation.id)
    AuditlogJob.perform_async(user_id: current_user.id, user_name: current_user.uuid, action_type: "pay through coinpayment", action_meta: "Balance was successfully added to your account", ip: request.env['REMOTE_ADDR'])
    # redirect_to @transaction.status_url

  end

  def call_coinmoney

    ActiveRecord::Base.transaction do 
 
      @tx = params[:txn_id].to_s
      @status_text = params[:status_text].to_s  #reply from api
      @tx_found= MoneyOp.where.where(payment_id: @tx).first  #this date will be compared with our data
      @net_card_value = @tx_found.amount.to_f - @tx_found.fees.to_f
 
      if @status_text == 'Complete'  && @tx_found.status.to_i != 1
        @tx_found.status = 1
        @tx_found.save
        @user_wallet = UserWallet.where(:user_id => @tx_found.user_id.to_i).first
        @user_data = User.where(id: @tx_found.user_id.to_i).first
        @bank= CompanyBank.where(bank_key: @tx_found.payment_gateway).first
          
        if @bank.wallet_type == 2       
          @user_new_balance = @user_wallet.transfer_amount.to_f + @net_card_value     
          @user_wallet.update(:transfer_amount => @user_new_balance)
        else  
          @user_new_balance = @user_wallet.amount.to_f + @net_card_value
          @user_wallet.update(:amount => @user_new_balance)
        end

        @update_card =  Card.where(:operation_id => @tx_found.opid).first
        @update_card.update(status:2)
        @notification = access_notification(@user_data,@net_card_value.round(2),"coinpayment","remittance","money_ops",@tx_found.id)
        @user_data.update(:total_daily_deposite => @user_data.total_daily_deposite + @net_card_value, :daily_deposite_number => @user_data.daily_deposite_number.to_i + 1)
        AuditlogJob.perform_async(user_id: @user_data.id, user_name: @user_data.uuid, action_type: "pay through #{@tx_found.payment_gateway}", action_meta: "Balance was successfully added to your account", ip: request.env['REMOTE_ADDR'])
      else
        @tx_found.status = 0
        @tx_found.save
      end
      
    end 

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_money_op
      @money_op = MoneyOp.where(id: params[:id]).first
    end

    def access_notification(currentuser,original_amount,bank_key,op_type,control,opid)

      @original_amount = original_amount
      @bank_key = bank_key.to_s
      @op_type = op_type.to_s
      @currentuser = currentuser
      @user_notification_setting = NotificationsSetting.where(user_id: @currentuser.id.to_i).first
      if @op_type == "remittance"
        @title = "Recharge Wallet Balance"
        if @bank_key == "paypal" || @bank_key == "perfectmoney" || @bank_key == "coinpayment"
          @smstext = "you_have_processed_anew_deposit_of/#{@original_amount.round(2)}/USD_to_your_account/via_direct_online_bank/your_transaction_has_been_successfully_created/the_amount_has_been_successfully_added_to_your_account"
          # @smstext = " لقد قمت بمعاملة ايداع بمبلغ #{@original_amount} دولار أمريكي إلى حسابك عن طريق الدفع عبر البنوك الالكترونية، معاملتك تمت بنجاح، تم إيداع المبلغ في حسابك"
        else
          @smstext = "you_have_processed_anew_deposit_of/#{@original_amount.round(2)}/USD_to_your_account/via_direct_online_bank/your_transaction_is_inprogress/we_will_notify_you"
          # @smstext = "لقد قمت بمعاملة ايداع بمبلغ #{@original_amount} دولار أمريكي إلى حسابك عن طريق الدفع عبر البنوك الالكترونية، معاملتك قيد التنفيذ، سوف نعلمك عند تمام تنفيذها وإيداع المبلغ في حسابك"
        end
      elsif @op_type == "withdraw"
        @title = "Balance Withdraw"
        @smstext = "you_have_processed_anew_withdraw_of/#{@original_amount}/USD_from_your_account/via_direct_bank_withdraw/your_transaction_is_inprogress/we_will_notify_you_withdraw"
        # @smstext = "لقد قمت بمعاملة سحب مبلغ #{@original_amount} دولار أمريكي من حسابك عن طريق السحب البنكي المباشر، معاملتك قيد التنفيذ، سوف نعلمك عند تمام تأكيدها وامكانية سحب المبلغ المطلوب "
      end

      @splitxt = @smstext.split('/')
      @smstext_translation = t("mails.#{@splitxt[0]}") + "#{@splitxt[1]}" + t("mails.#{@splitxt[2]}") + t("mails.#{@splitxt[3]}") + t("mails.#{@splitxt[4]}") + t("mails.#{@splitxt[5]}")
      Notification.create(user_id: @currentuser.id ,title: @title, description: @smstext , notification_type: @user_notification_setting.money_transactions,controller: control ,opid: opid)
        
      if @user_notification_setting.money_transactions == 3
        SMSNotification.sms_notification_setting(@currentuser.telephone,@smstext_translation)
        SmsLog.create(:user_id => @currentuser.id, :pinid => @smstext_translation,:status => 1,:sms_type => 3)
        EmailNotification.email_notification_setting(user_mail:@currentuser.email,subject:@title,text: opid)
      elsif @user_notification_setting.money_transactions == 2
        SMSNotification.sms_notification_setting(@currentuser.telephone,@smstext_translation)
        SmsLog.create(:user_id => @currentuser.id, :pinid => @smstext_translation,:status => 1,:sms_type => 3)
      elsif @user_notification_setting.money_transactions == 1
        EmailNotification.email_notification_setting(user_mail:@currentuser.email,subject:@title,text: opid)
      end

    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def money_op_params
      params.require(:money_op).permit(:opid, :optype, :amount, :payment_gateway, :status, :payment_date, :payment_id, :user_id ,:fees , :admin_note)
    end
end
