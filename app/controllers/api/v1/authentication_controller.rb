
class Api::V1::AuthenticationController < ApplicationController
    include Clearance::Authentication     

    skip_before_action :verify_authenticity_token

        # POST /authentication
        # POST /authentication.json
        #=begin
        # @api {post} /authentication/ authenticate User
        # @apiVersion 0.3.0
        # @apiName AuthenticateUser
        # @apiGroup Authentication
        # @apiDescription User Authenticate.
        # @apiParam {String}     email             User's Email
        # @apiParam {String}     password          User's Password
        #
        # @apiExample Example usage: 
        # curl -X POST -d email="amira.elfayome@servicehigh.com" -d password="Amira123456" http://localhost:3000/api/v1/auth_user      
        # @apiSuccess {Integer}     id                   User's-ID.
        # @apiSuccess {String}      email                User's email.
        # @apiSuccess {String}     auth_token           User's authentication token.
        #
        # @apiSuccessExample Response (example):
        #     HTTP/ 200 OK
        #     {
        #       "success": "200 ok"
        #    }
        #
        # @apiError NoAccessRight Invalid Email/Password.
        #
        # @apiErrorExample Error-Response:
        #     HTTP/1.1 400 Bad Request
        #     {
        #       "error": "Invalid Email/Password"
        #    }
        #=end


        def authenticate_user
            @check_account_status = User.where("email =?",params[:email]).first
            if @check_account_status != nil 
                if @check_account_status.disabled == 1
                    render json: "Your account is Locked, please visit your email to unlock the account"
                elsif @check_account_status.status == 1
                    user = User.authenticate(params[:email],params[:password])
                    if (user)  
                        user.loginattempts += 1
                        user.failedattempts = 0
                        user.save
                        @device_id = SecureRandom.uuid
                        Login.create(user_id: user.id, ip_address: request.remote_ip,user_agent: request.user_agent,device_id: @device_id, :operation_type => "sign_in")
                        AuditlogJob.perform_async(user_id: user.id, user_name: user.uuid, action_type: "log in", action_meta: "user was successfully logeed in.", ip: request.env['REMOTE_ADDR'])
                        UserMailer.signin_details(user,request.remote_ip,request.user_agent,@device_id).deliver_later
                        render json: payload(user)                   
                    else                                
                        @check_account_status.failedattempts += 1
                        if @check_account_status.failedattempts >= 3
                            @token = SecureRandom.hex(4)
                            @check_account_status.disabled = 1
                            @check_account_status.unlock_token = @token
                            UserMailer.unlockaccount(@check_account_status,@token).deliver_later
                        end
                        @check_account_status.save                             
                        render json: {errors: ['Invalid Email/Password']}, status: :unauthorized
                    end
                else
                    render json: "please confirm your mail first"
                end
            else
                render json: {message: 'Invalid Email'}, status: :unauthorized
            end
        end


        #=begin
        # @api {post} /getEditToken
        # @apiVersion 0.3.0
        # @apiName AuthenticateUser
        # @apiGroup Authentication
        # @apiDescription User Temporary Token.
        # @apiParam {String}     email             User's Email
        # @apiParam {String}     password          User's Password
        #
        # @apiExample Example usage: 
        # curl -X POST -d email="amira.elfayome@servicehigh.com" -d password="Amira123456" http://localhost:3000/api/v1/getEditToken      
        # @apiSuccess {Integer}     id                   User's-ID.
        # @apiSuccess {String}      email                User's email.
        # @apiSuccess {String}     Temporary_token           User's Temporary token.
        #
        # @apiSuccessExample Response (example):
        #     HTTP/ 200 OK
        #     {
        #       "success": "200 ok"
        #    }
        #
        # @apiError NoAccessRight Invalid Email/Password.
        #
        # @apiErrorExample Error-Response:
        #     HTTP/1.1 400 Bad Request
        #     {
        #       "error": "Invalid Email/Password"
        #    }
        #=end

        def getEditToken
            user = User.authenticate(params[:email],params[:password])
            if (user) 
                render json: payload2(user) 
            else                                                        
                render json: {errors: ['Invalid Email/Password']}, status: :unauthorized
            end

        end
    
        # POST /authentication
        # POST /authentication.json
        # =begin
        # @api {post} /unlockaccount/ unlock user account 
        # @apiVersion 0.3.0
        # @apiName unlockaccount
        # @apiGroup Authentication
        # @apiDescription UnLock account after Number of failed attempts.
        # @apiParam {String}     email             User's Email
        # @apiParam {String}     token             User's token to unlock account
        #
        # @apiExample Example usage: 
        # curl -X POST -d email="engamira333@gmail.com" -d token="65f152a6" http://localhost:3000/api/v1/unlockaccount 
        #
        # @apiSuccessExample Response (example):
        #     HTTP/ 200 OK
        #     {
        #       "success": "200 ok"
        #    }
        #
        # @apiError required_parameter_missing wrong email.
        # @apiError required_parameter_missing2 wrong code.
        #
        # @apiErrorExample Error-Response:
        #     HTTP/1.1 400 Bad Request
        #     {
        #       "error": "Invalid Email"
        #    }
        # @apiErrorExample Error-Response:
        #     HTTP/1.1 400 Bad Request
        #     {
        #       "error": "Invalid Token"
        #    }
        # =end
        def unlockaccount
            @email = params[:email]
            @user_code = params[:token]
            @user = User.where("email =?",@email ).first
            if  (@user != nil)
                if @user_code.to_s  == @user.unlock_token
                    @user.unlock_token = ""
                    @user.failedattempts = 0
                    @user.disabled = 0
                    @user.save
                    render json:  {'result' => true}
                else
                    render json: {'result' => false, 'error' => 'Invalid Token'} 
                end
            else
                    render json: {'result' => false, 'error' => 'Invalid Email'} 
            end
        end

    private

        def payload(user)
            return nil unless user and user.id
            {
                auth_token: JsonWebToken.encode({user_id: user.id},""),
                user: {id: user.id, email: user.email}
                }
        end

        def payload2(user)
            return nil unless user and user.id
            {
                Temporary_token: JsonWebToken.encode({user_id: user.id},15.minutes.from_now),
                user: {id: user.id, email: user.email}
                }
        end
end


