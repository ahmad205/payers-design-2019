class Api::V1::UsersController < ApplicationController
    before_action :authenticate_request!, except: [:create, :confirmmail, :resend_confirmmail]
    before_action :set_user, only: [:show, :edit, :update, :destroy]
    skip_before_action :verify_authenticity_token
    attr_reader :current_user
    
    # GET /users/1
    # GET /users/1.json
    # =begin
    # @api {get} /api/users/{:id} Get User Data
    # @apiVersion 0.3.0
    # @apiName GetUser
    # @apiGroup User
    # @apiDescription get details of specific user.
    # @apiExample Example usage:
    # curl -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ.lKv0AacisCZz_YNhA5YrOtTftPTtEqUZuRe1td-wmoM" -i http://localhost:3000/api/v1/users/1
    #
    # @apiSuccess {Integer}    id                   Users-ID.
    # @apiSuccess {String}     firstname            Users firstname.
    # @apiSuccess {String}     lastname             Users lastname.
    # @apiSuccess {String}     email                Users email.
    # @apiSuccess {Integer}    country_id           Users country.
    # @apiSuccess {Datetime}   created_at           date of creating the User.
    # @apiSuccess {Datetime}   updated_at           date of updating the User.
    #
    # @apiError NoAccessRight Only authenticated users can access the data.
    # @apiError UserNotFound   The <code>id</code> of the User was not found.
    #
    # @apiErrorExample Response (example):
    #     HTTP/1.1 401 Unauthorized
    #     {
    #       "error": "NoAccessRight"
    #    }
    # @apiErrorExample Error-Response:
    #     HTTP/1.1 404 Not Found
    #     {
    #       "error": "UserNotFound"
    #    }
    # =end

  def show    
    @user = User.where("id =? " ,params[:id]).first
      respond_to do |format|
        if current_user.id == params[:id].to_i        
          format.json { render json: @user }
        else
          format.json { render json: "Not Allowed" }
        end
      end
  end
 
  def homepage   
    # curl -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4" -H "Content-Type: application/json" -X GET http://localhost:3000/api/v1/homepage
    render json: {'logged_in' => true} 
  end

    # confirm user mail after registeration
    # POST /confirm user mail after registeration
    # =begin
    # @api {post} /api/v1/confirmmail confirm user mail
    # @apiVersion 0.3.0
    # @apiName POSTconfirmmail
    # @apiGroup User
    # @apiDescription confirm user mail after registeration.
    # @apiExample Example usage:
    # curl -X POST  http://localhost:3000/api/v1/confirmmail -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{"email": "engamira333@gmail.com","token": "3aa4e716"}' 
    #
    # @apiSuccessExample Success-Response:
    # HTTP/1.1 200 OK
    # {
    #   "result : true"
    # }
    #
    # @apiError required_parameter_missing wrong code .
    # @apiError required_parameter_missing2 wrong email .
    #
    # @apiErrorExample Response (example):
    #     HTTP/1.1 400 Badrequest
    #     {
    #       "result : false"
    #       "error": "MissingData"
    #       "status": 400- Bad Request
    #    }
    # =end 

  def confirmmail
      @email = params[:email]
      @token = params[:token]
      @user = User.where("email =?",@email ).first
        if @user
           @verify_code = Verification.where("user_id =?",@user.id ).first
           if @verify_code.email_confirmation_token == @token
              @user.update(:status => 1)
              @verify_code.update(:email_confirmed_at => Time.now) 
              render json: {'result' => true}
           else
              render json: {'result' => false, 'error' => 'wrong code'}
           end      
        else
              render json: {'result' => false, 'error' => 'Email Not Found'}
        end
  end

    # resend confirmation mail 
    # POST /resend confirmation mail to user after registeration
    # =begin
    # @api {post} /api/v1/resend_confirmmail resend confirmation mail
    # @apiVersion 0.3.0
    # @apiName POSTresend_confirmmail
    # @apiGroup User
    # @apiDescription resend confirmation mail to user after registeration.
    # @apiExample Example usage:
    # curl -X POST  http://localhost:3000/api/v1/resend_confirmmail -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{"email": "engamira333@gmail.com"}' 
    #
    # @apiSuccessExample Success-Response:
    # HTTP/1.1 200 OK
    # {
    #    "result : true"
    # }
    #
    # @apiError required_parameter_missing wrong email .
    #
    #  @apiErrorExample Response (example):
    #     HTTP/1.1 400 Badrequest
    #     {
    #       "result : false"
    #       "error": "MissingData"
    #       "status": 400- Bad Request
    #    }
    # =end 

  def resend_confirmmail
    @email = params[:email]
    @user = User.where("email =?",@email ).first     
      if @user
         @verify_code = Verification.where("user_id =?",@user.id ).first      
         UserMailer.confirmmail(@user,@verify_code.email_confirmation_token).deliver_later
         render json: {'result' => true}
      else
         render json: {'result' => false, 'error' => 'Email Not Found'}
      end
  end

    # POST /users
    # POST /users.json
    # =begin
    # @api {post} /users/ Create new User
    # @apiVersion 0.3.0
    # @apiName postUser
    # @apiGroup User
    # @apiDescription create new user.
    # @apiParam {String}     firstname         User's First Name 
    # @apiParam {String}     lastname          User's Last Name 
    # @apiParam {Integer}    country_id        User's Country 
    # @apiParam {String}     email             User's Email
    # @apiParam {String}     password          User's Password
    # @apiParam {String}     refered_by        Invitation Code Of the Invitor
    #
    # @apiExample Example usage:       
    # curl -X POST  http://localhost:3000/api/v1/users -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{"user": {"firstname": "amira2","lastname": "amira2","password": "Amira123456", "country_id": 1,"email": "tessst@gmail.com", "refered_by": "71000e83"}}' 
    # @apiSuccess {Integer}     id                   Users-ID.
    # @apiSuccess {String}      firstname            Users firstname.
    # @apiSuccess {String}      lastname             Users lastname.
    # @apiSuccess {String}      email                Users email.
    # @apiSuccess {Integer}     country_id           Users country.
    # @apiSuccess {String}      secret_code          Users Secret Code
    # @apiSuccess {String}      telephone            Users telephone
    # @apiSuccess {Integer}     account_currency     Users currency
    # @apiSuccess {Integer}     active_otp           Users active two factor authentication
    #
    # @apiSuccessExample Response (example):
    #     HTTP/ 200 OK
    #     {
    #       "success": "200 ok"
    #    }
    #
    # @apiError NoAccessRight Only authenticated users can create User.
    #
    # @apiError MissingToken invalid-token.
    # @apiErrorExample Error-Response:
    #     HTTP/1.1 400 Bad Request
    #     {
    #       "error": "Missing Token"
    #    }
    # =end

  def create
    @user = User.new(user_params)
    @user.uuid = SecureRandom.hex(4)
    @user.invitation_code = SecureRandom.hex(4)
    @user.auth_token = SecureRandom.hex(6)
    @token = SecureRandom.hex(4)
    @user.status = 1
    @user.language = Setting.where(key: "language").pluck(:value).first
    @user.password = params[:password]
    @ps = Country.where(id: @user.country_id).pluck(:short_code).first
    @contry_rond = 10.times.map{rand(10)}.join
    @user.account_number = @ps[0,2] + @contry_rond
    respond_to do |format|
        if @user.save
          User.user_details(@user)
          # Verification.create(:user_id => @user.id,:email_confirmation_token => @token)
          Watchdog.create(:user_id => @user.id,:ipaddress => request.env['REMOTE_ADDR'],:logintime => Time.now,:lastvisit => Time.now ,:operation_type => "sign_up",user_agent: request.user_agent)
          if (params[:user][:refered_by] != nil )
              @invited_by = User.where("invitation_code =? ",params[:user][:refered_by]).first
              if (@invited_by != nil)
                 AffilateProgram.create(:user_id => @user.id, :refered_by => @invited_by.id)
              end
          end
          # UserMailer.confirmmail(@user,@token).deliver_later
          format.json { render json: @user, status: :created } 
          # format.json { render :show, status: :created, location: @user }
        else
          flash[:error] = "Email or Password are not valid"
          format.json { render json: flash }
        end
    end
  end

    # PATCH/PUT /users/1
    # PATCH/PUT /users/1.json
    # =begin
    # @api {put} /users/:id/edit Update User 
    # @apiVersion 0.3.0
    # @apiName PutUser
    # @apiGroup User
    #
    # @apiDescription update details of a user.
    #
    # @apiParam {String}     firstname            Users firstname.
    # @apiParam {String}     lastname             Users lastname.
    # @apiParam {String}     email                Users email.
    # @apiParam {Integer}    country_id           Users country.
    # @apiParam {String}     secret_code          Users Secret Code
    # @apiParam {String}     telephone            Users telephone
    # @apiParam {Integer}    account_currency     Users currency
    # @apiParam {Integer}    active_otp           Users active two factor authentication 
    #
    # @apiExample Example usage:
    # curl -X PUT http://localhost:3000/api/v1/users/10 -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4" -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{ "email": "amira@ggggg.com"}'
    #
    # @apiSuccess {Integer}      id                     Users-ID.
    # @apiSuccess {String}       firstname              Users firstname.
    # @apiSuccess {String}       lastname               Users lastname.
    # @apiSuccess {String}       email                  Users email.
    # @apiSuccess {Integer}      country_id             Users country.
    # @apiSuccess {String}       secret_code            Users Secret Code
    # @apiSuccess {String}       telephone              Users telephone
    # @apiSuccess {Integer}      account_currency       Users currency
    # @apiSuccess {Integer}      active_otp             Users active two factor authentication 
    # @apiSuccess {Datetime}     created_at             Date of creating the User.
    # @apiSuccess {Datetime}     updated_at             Date of updating the User.
    #
    # @apiError NoAccessRight Only authenticated users can update the data.
    # @apiError UserNotFound   The <code>id</code> of the User was not found.
    #
    # @apiErrorExample Error-Response:
    #     HTTP/1.1 404 Not Found
    #     {
    #       "error": "UserNotFound"
    #    }
    # @apiErrorExample Response (example):
    #     HTTP/1.1 401 Not Authenticated
    #     {
    #       "error": "NoAccessRight"
    #    }
    # =end
       
  def update
     
    respond_to do |format|
      if current_user.roleid == 1 or current_user.id == params[:id].to_i 
        if @user.update(user_params)
           format.json { render json: @user, status: :ok, location: @user }
           AuditlogJob.perform_async(user_id: current_user.id, user_name: current_user.uuid, action_type: "Edit profile", action_meta: "User profile was successfully updated", ip: request.env['REMOTE_ADDR'])
        else
           format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      else
        format.json { render json: "Not Allowed" }
      end
    end   
  end

  private
      # Use callbacks to share common setup or constraints between actions.
  def set_user
      @user = User.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def user_params
    params.require(:user).permit(:uuid, :roleid,  :firstname, :lastname, :status, :language, :disabled, :loginattempts, :username, :email, :password, :secret_code, :otp_secret_key, :active_otp, :account_number, :telephone, :account_currency, :country_id, :invitation_code, :unlock_token, :refered_by) 
  end

end
