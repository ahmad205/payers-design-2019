class Api::V1::SettingsController < ApplicationController

  def mail_setting

    @via = params[:via]
    @provider = params[:provider]
    @default_subject = params[:default_subject]
    @default_text = params[:default_text]
    @default_user_mail = params[:default_user_mail]
    @data = YAML.load_file "config/mail_setting.yml"
    @data["mail_setting"]["provider"] = @provider
    @data["mail_setting"]["via"] = @via
    @data["mail_setting"]["default_subject"] = @default_subject
    @data["mail_setting"]["default_text"] = @default_text
    @data["mail_setting"]["default_user_mail"] = @default_user_mail
    File.open("config/mail_setting.yml", 'w') { |f| YAML.dump(@data, f) }

  end 

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_setting
      @setting = Setting.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def setting_params
      params.require(:setting).permit(:key, :value, :description)
    end
end
