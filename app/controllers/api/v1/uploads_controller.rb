module Api
  module V1
    class UploadsController < ApplicationController
      before_action :set_upload, only: [:show, :edit, :update, :destroy]
      skip_before_action :verify_authenticity_token


      # GET /uploads
      # GET /uploads.json
      def index
        @uploads = Upload.all
      end

      # GET /uploads/1
      # GET /uploads/1.json
      def show
      end

      # GET /uploads/new
      def new
        @upload = Upload.new
      end

      # GET /uploads/1/edit
      def edit
      end

      # POST /uploads
      # POST /uploads.json
      def image_status

        # @attachment = Upload.where(:id => params[:id].to_i).first
        #    if  current_user = session[:user_id]
        #      @data_url = @attachment.avatar.service_url
            
        #      require 'open-uri'
        #      open('image.png', 'wb') do |file|
        #        @the_file = file << open(@data_url).read
        #        send_file @the_file
        #      end
            
        #      else

        #      redirect_to(login_path,:notice => "صوره غير مسموح لك  بها")
        #    end

      end

      def create
        
        @file_name = image_upload
        @upload = Upload.create(:title => params[:upload][:title],:user_id => current_user.id.to_i)
        @upload.avatar.attach(io: File.open(@file_name), filename: @file_name, content_type: "image/jpg")

        # @upload = Upload.new(upload_params)
        respond_to do |format|

          if @upload
            format.html { redirect_to @upload, notice: 'Upload was successfully created.' }
            format.json { render :show, status: :created, location: @upload }
          else
            format.html { render :new }
            format.json { render json: @upload.errors, status: :unprocessable_entity }
          end

        end

      end

      # PATCH/PUT /uploads/1
      # PATCH/PUT /uploads/1.json
      def update

        @file_name = image_upload
        @upload_update = @upload.update(:title => params[:upload][:title])
        @upload.avatar.attach(io: File.open(@file_name), filename: @file_name, content_type: "image/jpg")

        respond_to do |format|
          if @upload_update
            format.html { redirect_to @upload, notice: 'Upload was successfully updated.' }
            format.json { render :show, status: :ok, location: @upload }
          else
            format.html { render :edit }
            format.json { render json: @upload.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /uploads/1
      def destroy
        @upload.destroy
        respond_to do |format|
          flash[:success] = "Upload was successfully deleted"
          format.json { render json: flash }
        end
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_upload
          @upload = Upload.find(params[:id])
        end

        def image_upload
          @avatar = params[:upload][:avatar]
          @image_data = ActiveSupport::JSON.decode(@avatar)
          @data_im = @image_data['output']['image']
      
          require 'mime/types'
          @REGEXP = /\Adata:([-\w]+\/[-\w\+\.]+)?;base64,(.*)/m
      
          data_uri_parts = @data_im.match(@REGEXP) || []
          extension = MIME::Types[data_uri_parts[1]].first.preferred_extension
          rand_id = rand 100000..999999
          @file_name = "#{rand_id}.#{extension}"
          @bb = File.open(@file_name, 'wb') do |file|
            @aa= file.write(Base64.decode64(data_uri_parts[2]))
          end
          
          return @file_name
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def upload_params
          params.require(:upload).permit(:title,:user_id)
        end
    end
  end
end
