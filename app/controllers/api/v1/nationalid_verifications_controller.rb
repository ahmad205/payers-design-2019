class Api::V1::NationalidVerificationsController < ApplicationController
  before_action :set_nationalid_verification, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_request!
  skip_before_action :verify_authenticity_token
    attr_reader :current_user

      # GET /nationalid_verifications/1
      # GET /nationalid_verifications/1.json
      # =begin
      # @api {get} /api/nationalid_verifications/{:id} Get National id Verification Data
      # @apiVersion 0.3.0
      # @apiName GetNationalidVerification
      # @apiGroup Verification
      # @apiDescription get details of specific National id Verification.
      # @apiExample Example usage:
      # curl -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ.lKv0AacisCZz_YNhA5YrOtTftPTtEqUZuRe1td-wmoM" -i http://localhost:3000/api/v1/nationalid_verifications/1 
      #
      # @apiSuccess {Integer}       id                         Nationalid Verifications-ID.
      # @apiSuccess {Integer}       user_id                    User's-ID.
      # @apiSuccess {String}        legal_name                 User's Legal Name
      # @apiSuccess {Integer}       national_id                User's National ID
      # @apiSuccess {Integer}       document_type              National ID Type
      # @apiSuccess {Date}          issue_date                 National ID Issue Date
      # @apiSuccess {Date}          expire_date                National ID Expiration Date
      # @apiSuccess {Blob}          attachment1                National ID front picture
      # @apiSuccess {Blob}          attachment2                National ID back picture
      # @apiSuccess {Integer}       status                     National ID Verification status
      # @apiSuccess {datetime}      created_at                 National ID Verification created at
      # @apiSuccess {datetime}      updated_at                 National ID Verification updated at
      #
      # @apiError NoAccessRight Only authenticated users can access the data.
      # @apiError CountryNotFound   The <code>id</code> of the Country was not found.
      #
      # @apiErrorExample Response (example):
      #     HTTP/1.1 401 Unauthorized
      #     {
      #       "error": "NoAccessRight"
      #    }
      # @apiErrorExample Error-Response:
      #     HTTP/1.1 404 Not Found
      #     {
      #       "error": "CountryNotFound"
      #    }
      # =end
  def show
    @nationalid_verification = NationalidVerification.where("id =? " ,params[:id]).first
    respond_to do |format|
      if current_user.id != @nationalid_verification.user_id.to_i
        format.json { render json: "Not allowed"}
      else    
        format.json { render json: @nationalid_verification }
      end
    end
  end


  # create new National Id Verification
  
    # POST /nationalid_verifications
    # POST /nationalid_verifications.json
    # =begin
    # @api {post} /nationalid_verifications/ Create new nationalid_verification
    # @apiVersion 0.3.0
    # @apiName postNationalidVerification
    # @apiGroup Verification
    # @apiDescription create new nationalid_verification.
    # @apiParam {String}      legal_name           User's Legal Name 
    # @apiParam {Integer}     national_id          User's National ID
    # @apiParam {Integer}     document_type        National ID Type
    # @apiParam {Date}        issue_date           National ID Issue Date
    # @apiParam {Date}        expire_date          National ID Expiration Date
    # @apiParam {Blob}        attachment1          National ID front picture
    # @apiParam {Blob}        attachment2          National ID back picture
    #
    # @apiExample Example usage:  
    # curl -X POST  http://localhost:3000/api/v1/nationalid_verifications -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ.lKv0AacisCZz_YNhA5YrOtTftPTtEqUZuRe1td-wmoM" -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{"nationalid_verification": {"legal_name": "amiraasseraaa","national_id": 123456789,"document_type": "Passport", "issue_date": "1/1/2017","expire_date": "1/1/2020"}}' 
    # @apiSuccess {Integer}       id                         Nationalid Verifications-ID.
    # @apiSuccess {Integer}       user_id                    User's-ID.
    # @apiSuccess {String}        legal_name                 User's Legal Name
    # @apiSuccess {Integer}       national_id                User's National ID
    # @apiSuccess {Integer}       document_type              National ID Type
    # @apiSuccess {Date}          issue_date                 National ID Issue Date
    # @apiSuccess {Date}          expire_date                National ID Expiration Date
    # @apiSuccess {Blob}          attachment1                National ID front picture
    # @apiSuccess {Blob}          attachment2                National ID back picture
    # @apiSuccess {Integer}       status                     National ID Verification status
    # @apiSuccess {datetime}      created_at                 National ID Verification created at
    # @apiSuccess {datetime}      updated_at                 National ID Verification updated at
    #
    # @apiSuccessExample Response (example):
    #     HTTP/ 200 OK
    #     {
    #       "success": "200 ok"
    #    }
    #
    # @apiError NoAccessRight Only authenticated users can create User.
    #
    # @apiError MissingToken invalid-token.
    # @apiErrorExample Error-Response:
    #     HTTP/1.1 400 Bad Request
    #     {
    #       "error": "Missing Token"
    #    }
    # =end
  def create
    @current_user_verification = NationalidVerification.where("user_id =?", current_user.id.to_i).first
    if @current_user_verification != nil
      render json: "Not allowed"
    else
      @nationalid_verification = NationalidVerification.new(nationalid_verification_params)
      @nationalid_verification.user_id = current_user.id
      respond_to do |format|
        if @nationalid_verification.save
          format.json { render json: "Nationalid verification was successfully created."}
          AuditlogJob.perform_async(user_id: current_user.id, user_name: current_user.uuid, action_type: "Upload Nationalid verifications", action_meta: "Nationalid verification was successfully created.", ip: request.env['REMOTE_ADDR'])
        else
          format.json { render json: @nationalid_verification.errors}
        end
      end
    end
  end

      
      # edit National Id Verification,
      # only admins can edit status and note.
      # =begin
      # @api {put} /nationalid_verifications/:id/edit Update nationalid verification
      # @apiVersion 0.3.0
      # @apiName PutNationalidVerification
      # @apiGroup Verification
      # @apiPermission admin
      # @apiDescription update data of a nationalid_verifications.
      #
      # @apiParam {String}      legal_name           User's Legal Name 
      # @apiParam {Integer}     national_id          User's National ID
      # @apiParam {Integer}     document_type        National ID Type
      # @apiParam {Date}        issue_date           National ID Issue Date
      # @apiParam {Date}        expire_date          National ID Expiration Date
      # @apiParam {Blob}        attachment1          National ID front picture
      # @apiParam {Blob}        attachment2          National ID back picture
      #
      # @apiExample Example usage:
      # curl -X PUT  http://localhost:3000/api/v1/nationalid_verifications/1 -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ.lKv0AacisCZz_YNhA5YrOtTftPTtEqUZuRe1td-wmoM" -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{"nationalid_verification": {"legal_name": "tessssssssssst","national_id": 77777777777777,"document_type": "Passport", "issue_date": "1/1/2017","expire_date": "1/1/2080"}}' 
      #
      # @apiSuccess {Integer}       id                         Users-ID.
      # @apiSuccess {String}        legal_name                 User's Legal Name
      # @apiSuccess {Integer}       national_id                User's National ID
      # @apiSuccess {Integer}       document_type              National ID Type
      # @apiSuccess {Date}          issue_date                 National ID Issue Date
      # @apiSuccess {Date}          expire_date                National ID Expiration Date
      # @apiSuccess {Blob}          attachment1                National ID front picture
      # @apiSuccess {Blob}          attachment2                National ID back picture
      # @apiSuccess {Integer}       status                     National ID Verification status
      # @apiSuccess {datetime}      created_at                 National ID Verification created at
      # @apiSuccess {datetime}      updated_at                 National ID Verification updated at
      #
      # @apiError NoAccessRight Only authenticated users can update the data.
      # @apiError NationalidVerificationNotFound   The <code>id</code> of the NationalidVerification was not found.
      #
      # @apiErrorExample Error-Response:
      #     HTTP/1.1 404 Not Found
      #     {
      #       "error": "NationalidVerificationNotFound"
      #    }
      # @apiErrorExample Response (example):
      #     HTTP/1.1 401 Not Authenticated
      #     {
      #       "error": "NoAccessRight"
      #    }
      # =end

  def update
    respond_to do |format|
      if (@nationalid_verification.status == "Verified") or (current_user.id != @nationalid_verification.user_id.to_i)
        format.json { render json: "Not allowed"}
      else
         if @nationalid_verification.update(nationalid_verification_params)
           format.json { render json: "Nationalid verification was successfully updated."}
           AuditlogJob.perform_async(user_id: current_user.id, user_name: current_user.uuid, action_type: "Edit Nationalid verifications", action_meta: "Nationalid verification was successfully updated.", ip: request.env['REMOTE_ADDR'])
         else
           format.json { render json: @nationalid_verification.errors}
         end      
      end 
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_nationalid_verification
      @nationalid_verification = NationalidVerification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def nationalid_verification_params
      params.require(:nationalid_verification).permit(:user_id, :legal_name, :national_id, :document_type, :issue_date, :expire_date, :status, :note, :attachment1, :attachment2, images: [])
    end
end
