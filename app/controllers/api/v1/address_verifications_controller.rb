class Api::V1::AddressVerificationsController < ApplicationController
  before_action :set_address_verification, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_request!
  skip_before_action :verify_authenticity_token
    attr_reader :current_user

  # GET /address_verifications/1
  # GET /address_verifications/1.json
  # =begin
  # @api {get} /api/address_verifications/{:id} Get details of Address Verification of specific user
  # @apiVersion 0.3.0
  # @apiName GetAddressVerification
  # @apiGroup Verification
  # @apiDescription get details of specific Address Verification.
  # @apiExample Example usage:
  # curl -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ.lKv0AacisCZz_YNhA5YrOtTftPTtEqUZuRe1td-wmoM" -i http://localhost:3000/api/v1/address_verifications/1 
  #
  # @apiSuccess {Integer}        id                         Address Verifications-ID.
  # @apiSuccess {Integer}        user_id                    User's-ID.
  # @apiSuccess {String}         country                    User's Country
  # @apiSuccess {String}         city                       User's City
  # @apiSuccess {String}         state                      User's State
  # @apiSuccess {String}         street                     User's Street
  # @apiSuccess {String}         apartment_number           User's apartment_number
  # @apiSuccess {Blob}           attachment1                attachment
  # @apiSuccess {Integer}        status                     Verification Status 
  # @apiSuccess {String}         note                       Admin note
  # @apiSuccess {datetime}       created_at                 Address Verification created at
  # @apiSuccess {datetime}       updated_at                 Address Verification updated at
  #
  # @apiError NoAccessRight Only authenticated users can access the data.
  # @apiError AddressVerificationNotFound   The <code>id</code> of the Address Verification was not found.
  #
  # @apiErrorExample Response (example):
  #     HTTP/1.1 401 Unauthorized
  #     {
  #       "error": "NoAccessRight"
  #    }
  # @apiErrorExample Error-Response:
  #     HTTP/1.1 404 Not Found
  #     {
  #       "error": "Address Verification NotFound"
  #    }
  # =end
  
  def show
    respond_to do |format|
      if current_user.id != @address_verification.user_id.to_i
        format.json { render json: "Not allowed"}
      else    
        format.json { render json: @address_verification }
      end
    end
  end


  # create new Address Verification
  # each user can create only one address verification
  # POST /address_verifications
  # POST /address_verifications.json
  # =begin
  # @api {post} /address_verifications/ Create new address verification
  # @apiVersion 0.3.0
  # @apiName post AddressVerification
  # @apiGroup Verification
  # @apiDescription create new address verification.
  #
  # @apiParam {Integer}      user_id               User's ID
  # @apiParam {Integer}      country_id            User's Country
  # @apiParam {String}       city                  User's City
  # @apiParam {String}       state                 User's State
  # @apiParam {String}       street                User's Street
  # @apiParam {String}       apartment_number      User's Apartment Number
  # @apiParam {Blob}         attachment1           Attachment
  #
  # @apiExample Example usage:  
  # curl -X POST  http://localhost:3000/api/v1/address_verifications -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ.lKv0AacisCZz_YNhA5YrOtTftPTtEqUZuRe1td-wmoM" -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{"address_verification": {"country_id": 1,"city": "damietta","state": "newdamietta", "street": "mahgoob","apartment_number": "A200120"}}' 
  # @apiSuccess {Integer}        id                         Address Verifications-ID.
  # @apiSuccess {Integer}        user_id                    User's-ID.
  # @apiSuccess {String}         country_id                 User's Country
  # @apiSuccess {String}         city                       User's City
  # @apiSuccess {String}         state                      User's State
  # @apiSuccess {String}         street                     User's Street
  # @apiSuccess {String}         apartment_number           User's apartment_number
  # @apiSuccess {Blob}           attachment1                attachment
  # @apiSuccess {Integer}        status                     Verification Status 
  # @apiSuccess {String}         note                       Admin note
  # @apiSuccess {datetime}       created_at                 Address Verification created at
  # @apiSuccess {datetime}       updated_at                 Address Verification updated at
  #
  # @apiSuccessExample Response (example):
  #     HTTP/ 200 OK
  #     {
  #       "success": "200 ok"
  #    }
  #
  # @apiError NoAccessRight Only authenticated users can create User.
  #
  # @apiError MissingToken invalid-token.
  # @apiErrorExample Error-Response:
  #     HTTP/1.1 400 Bad Request
  #     {
  #       "error": "Missing Token"
  #    }
  # =end
  
  def create
    @current_user_verification = AddressVerification.where("user_id =?", current_user.id.to_i).first
    if @current_user_verification != nil
      render json: "Not allowed"
    else
      @address_verification = AddressVerification.new(address_verification_params)
      @address_verification.user_id = current_user.id
      respond_to do |format|
        if @address_verification.save
          format.json { render json: "Address verification was successfully created."}
          AuditlogJob.perform_async(user_id: current_user.id, user_name: current_user.uuid, action_type: "Upload address verifications", action_meta: "Address verification was successfully created.", ip: request.env['REMOTE_ADDR'])
        else
          format.json { render json: @address_verification.errors}
        end
      end
    end
  end



      # edit Address Verification,
      # only admins can edit status and note.
      # =begin
      # @api {put} /address_verifications/:id/edit Update Address verification
      # @apiVersion 0.3.0
      # @apiName Put Address Verification
      # @apiGroup Verification
      # @apiDescription update data of an address_verifications.
      #
      # @apiParam {Integer}      user_id               User's ID
      # @apiParam {Integer}      country_id            User's Country
      # @apiParam {String}       city                  User's City
      # @apiParam {String}       state                 User's State
      # @apiParam {String}       street                User's Street
      # @apiParam {String}       apartment_number      User's Apartment Number
      # @apiParam {Blob}         attachment1           Attachment
      #
      # @apiExample Example usage:
      # curl -X PUT  http://localhost:3000/api/v1/address_verifications/1 -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ.lKv0AacisCZz_YNhA5YrOtTftPTtEqUZuRe1td-wmoM" -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{"address_verification": {"country_id": 1,"city": "damietta","state": "newdamietta", "street": "mahgoob","apartment_number": "A200120"}}' 
      #
      # @apiSuccess {Integer}        id                         Address Verifications-ID.
      # @apiSuccess {Integer}        user_id                    User's-ID.
      # @apiSuccess {String}         country_id                 User's Country
      # @apiSuccess {String}         city                       User's City
      # @apiSuccess {String}         state                      User's State
      # @apiSuccess {String}         street                     User's Street
      # @apiSuccess {String}         apartment_number           User's apartment_number
      # @apiSuccess {Blob}           attachment1                attachment
      # @apiSuccess {Integer}        status                     Verification Status 
      # @apiSuccess {String}         note                       Admin note
      # @apiSuccess {datetime}       created_at                 Address Verification created at
      # @apiSuccess {datetime}       updated_at                 Address Verification updated at
      #
      # @apiError NoAccessRight Only authenticated users can update the data.
      # @apiError Address Verification NotFound   The <code>id</code> of the Address Verification was not found.
      #
      # @apiErrorExample Error-Response:
      #     HTTP/1.1 404 Not Found
      #     {
      #       "error": "Address Verification NotFound"
      #    }
      # @apiErrorExample Response (example):
      #     HTTP/1.1 401 Not Authenticated
      #     {
      #       "error": "NoAccessRight"
      #    }
      # =end

  def update
    respond_to do |format|
      if (@address_verification.status == "Verified") or (current_user.id != @address_verification.user_id.to_i)
          format.json { render json: "Not allowed"}
      else
          if @address_verification.update(address_verification_params)
            format.json { render json: "Address verification was successfully updated."}
            AuditlogJob.perform_async(user_id: current_user.id, user_name: current_user.uuid, action_type: "Edit address verifications", action_meta: "Address verification was successfully updated.", ip: request.env['REMOTE_ADDR'])
          else
            format.json { render json: @address_verification.errors}
          end
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_address_verification
      @address_verification = AddressVerification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def address_verification_params
      params.require(:address_verification).permit(:user_id, :country_id, :city, :state, :street, :apartment_number, :status, :note, :attachment)
    end
end
