module Api
  module V1
    class CardsCategoriesController < ApplicationController
      before_action :set_cards_category, only: [:show, :edit, :update, :destroy]
      skip_before_action :verify_authenticity_token

      # GET /cards_categories
      def index

        @cards_categories = CardsCategory.all
        respond_to do |format|
          format.json { render json: @cards_categories }
        end

        # =begin
        # @api {get} /api/v1/cards_categories 1-Request cards categories List
        # @apiVersion 0.3.0
        # @apiName GetCardsCategories
        # @apiGroup Cards Categories
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/cards_categories
        # @apiSuccess {Number} id category unique ID.
        # @apiSuccess {Number} card_value card category value.
        # @apiSuccess {Number} active control card category display(1 for display & 0 for hide).
        # @apiSuccess {Number} order category order(high order for high category value).
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # [
        # {
        #     "id": 1,
        #     "card_value": 100,
        #     "active": 1,
        #     "order": 1,
        #     "created_at": "2018-07-26T14:41:50.506Z",
        #     "updated_at": "2018-07-28T08:03:16.400Z"
        # },
        # {
        #   "id": 3,
        #   "card_value": 300,
        #   "active": 0,
        #   "order": 3,
        #   "created_at": "2018-07-26T14:49:06.665Z",
        #   "updated_at": "2018-07-28T08:38:59.747Z"
        # }
        # ]
        # @apiError MissingToken invalid Token.
        # @apiErrorExample Error-Response:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end

      end

      # GET /cards_categories/1
      # GET /cards_categories/1.json
      def show

        respond_to do |format|
          format.json { render json: @cards_category }
        end
        
        # =begin
        # @api {get} /api/v1/cards_categories/{:id} 3-Request Specific card category
        # @apiVersion 0.3.0
        # @apiName GetSpecificCardsCategories
        # @apiGroup Cards Categories
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/cards_categories/3
        # @apiParam {Number} id Card Category unique ID.
        # @apiSuccess {Number} card_value card category value.
        # @apiSuccess {Number} active control card category display(1 for display & 0 for hide).
        # @apiSuccess {Number} order category order(high order for high category value).
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # {
        #   "id": 3,
        #   "card_value": 300,
        #   "active": 0,
        #   "order": 3,
        #   "created_at": "2018-07-26T14:49:06.665Z",
        #   "updated_at": "2018-07-28T08:38:59.747Z"
        # }
        # @apiError MissingToken invalid Token.
        # @apiErrorExample Error-Response:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # @apiError CategoryNotFound The id of the Card Category was not found. 
        # @apiErrorExample Error-Response:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "CategoryNotFound"
        #   }
        # =end

      end

      def display_cards_categories

        @categories = CardsCategory.where(:active => 1).all.order(card_value: :desc)
        respond_to do |format|
          format.json { render json: @categories }
        end

        # =begin
        # @api {get} /api/v1/display_cards_categories 2-Request displayed cards categories List
        # @apiVersion 0.3.0
        # @apiName GetDisplayedCardsCategories
        # @apiGroup Cards Categories
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/display_cards_categories
        # @apiSuccess {Number} id category unique ID.
        # @apiSuccess {Number} card_value card category value.
        # @apiSuccess {Number} active control card category display(1 for display & 0 for hide).
        # @apiSuccess {Number} order category order(high order for high category value).
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # [
        # {
        #     "id": 1,
        #     "card_value": 100,
        #     "active": 1,
        #     "order": 1,
        #     "created_at": "2018-07-26T14:41:50.506Z",
        #     "updated_at": "2018-07-28T08:03:16.400Z"
        # },
        # {
        #     "id": 2,
        #     "card_value": 200,
        #     "active": 1,
        #     "order": 2,
        #     "created_at": "2018-07-26T14:48:50.514Z",
        #     "updated_at": "2018-07-28T08:38:53.795Z"
        # }
        # ]
        # @apiError MissingToken invalid Token.
        # @apiErrorExample Error-Response:
        # HTTP/1.1 400 Bad Request
        #   {
        #     "error": "Missing token"
        #   }
        # =end

      end

      # GET /cards_categories/new
      def new
        @cards_category = CardsCategory.new
      end

      # GET /cards_categories/1/edit
      def edit
      end

      # POST /cards_categories
      def create
        @cards_category = CardsCategory.new(cards_category_params)

        respond_to do |format|
          if @cards_category.save
            format.json { render json: @cards_category, status: :created, location: @cards_category }
          else
            format.json { render json: @cards_category.errors, status: :unprocessable_entity }
          end
        end

        # =begin
        # @api {post} /api/v1/cards_categories 4-Create a new Card Category
        # @apiVersion 0.3.0
        # @apiName PostCardCategory
        # @apiGroup Cards Categories
        # @apiExample Example usage:
        # curl -X POST \
        # http://localhost:3000/api/v1/cards_categories \
        # -H 'cache-control: no-cache' \
        # -H 'content-type: application/json' \
        # -d '{
        #   "card_value": 500,
        #   "active": 1,
        #   "order": 5,
        # }'
        # @apiParam {Number} card_value card category value.
        # @apiParam {Number} active control card category display(1 for display & 0 for hide).
        # @apiParam {Number} order category unique order(high order for high category value).
        # @apiSuccess {Number} id category unique ID.
        # @apiSuccess {Number} card_value card category value.
        # @apiSuccess {Number} active control card category display(1 for display & 0 for hide).
        # @apiSuccess {Number} order category order(high order for high category value).
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # {
        #   "id": 5,
        #   "card_value": 500,
        #   "active": 0,
        #   "order": 5,
        #   "created_at": "2018-07-28T12:37:11.483Z",
        #   "updated_at": "2018-07-28T12:37:11.483Z"
        # }
        # @apiError MissingToken missing user name or password.
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 422 Missing token
        #   {
        #     "error": "Missing token"
        #   }
        # @apiError ExistingValue card category value has already been taken.
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 Existing Category Value
        #   {
        #     "error": "has already been taken"
        #   }
        # @apiError ExistingOrder card category order has already been taken.
        # @apiErrorExample Error-Response3:
        # HTTP/1.1 Invalid Order
        #   {
        #     "error": "has already been taken"
        #   }
        # =end
      end

      # PATCH/PUT /cards_categories/id
      def update

        respond_to do |format|
          if @cards_category.update(card_params)
            format.json { render json: @cards_category, status: :ok, location: @cards_category }
          else
            format.json { render json: @cards_category.errors, status: :unprocessable_entity }
          end
        end

        # =begin
        # @api {post} /api/v1/cards_categories/{:id} 5-Update an existing Card Category
        # @apiVersion 0.3.0
        # @apiName PutCardCategory
        # @apiGroup Cards Categories
        # @apiExample Example usage:
        # curl -X PUT \
        # http://localhost:3000/api/v1/cards_categories/5 \
        # -H 'cache-control: no-cache' \
        # -H 'content-type: application/json' \
        # -d '{
        #   "card_value": 400,
        #   "active": 1,
        #   "order": 5,
        # }'
        # @apiParam {Number} card_value card category value.
        # @apiParam {Number} active control card category display(1 for display & 0 for hide).
        # @apiParam {Number} order category unique order(high order for high category value).
        # @apiSuccess {Number} id category unique ID.
        # @apiSuccess {Number} card_value card category value.
        # @apiSuccess {Number} active control card category display(1 for display & 0 for hide).
        # @apiSuccess {Number} order category order(high order for high category value).
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # {
        #   "id": 5,
        #   "card_value": 400,
        #   "active": 1,
        #   "order": 5,
        #   "created_at": "2018-07-28T12:37:11.483Z",
        #   "updated_at": "2018-07-28T15:24:10.303Z"
        # }
        # @apiError MissingToken missing user name or password.
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 422 Missing token
        #   {
        #     "error": "Missing token"
        #   }
        # @apiError ExistingValue card category value has already been taken.
        # @apiErrorExample Error-Response2:
        # HTTP/1.1 Existing Category Value
        #   {
        #     "error": "has already been taken"
        #   }
        # @apiError ExistingOrder card category order has already been taken.
        # @apiErrorExample Error-Response3:
        # HTTP/1.1 Invalid Order
        #   {
        #     "error": "has already been taken"
        #   }
        # @apiError CategoryNotFound The id of the Card Category was not found. 
        # @apiErrorExample Error-Response4:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "CategoryNotFound"
        #   }
        # =end

      end

      # DELETE /cards_categories/1
      # DELETE /cards_categories/1.json
      def destroy

        @cards_category.destroy
        respond_to do |format|
          format.json { render json: @cards_category }
        end

        # =begin
        # @api {Delete} /api/v1/cards_categories/{:id} 6-Delete an existing Card Category
        # @apiVersion 0.3.0
        # @apiName DeleteCardCategory
        # @apiGroup Cards Categories
        # @apiExample Example usage:
        # curl -X DELETE \
        # http://localhost:3000/api/v1/cards_categories/5 \
        # @apiParam {Number} id card category unique ID.
        # @apiSuccess {Number} id category unique ID.
        # @apiSuccess {Number} card_value card category value.
        # @apiSuccess {Number} active control card category display(1 for display & 0 for hide).
        # @apiSuccess {Number} order category order(high order for high category value).
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # {
        #   "id": 5,
        #   "card_value": 400,
        #   "active": 1,
        #   "order": 5,
        #   "created_at": "2018-07-28T12:37:11.483Z",
        #   "updated_at": "2018-07-28T15:24:10.303Z"
        # }
        # @apiError MissingToken missing user name or password.
        # @apiErrorExample Error-Response1:
        # HTTP/1.1 422 Missing token
        #   {
        #     "error": "Missing token"
        #   }
        # @apiError CategoryNotFound The id of the Card Category was not found. 
        # @apiErrorExample Error-Response4:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "CategoryNotFound"
        #   }
        # =end
       
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_cards_category
          @cards_category = CardsCategory.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def cards_category_params
          params.permit(:card_value, :active, :order)
        end
    end
  end
end
