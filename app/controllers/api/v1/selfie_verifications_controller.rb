class Api::V1::SelfieVerificationsController < ApplicationController
  before_action :set_selfie_verification, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_request!
  skip_before_action :verify_authenticity_token
    attr_reader :current_user


  # GET /selfie_verifications/1
  # GET /selfie_verifications/1.json
  # =begin
  # @api {get} /api/selfie_verifications/{:id} Get details of Selfie Verification of specific user
  # @apiVersion 0.3.0
  # @apiName GetSelfie Verification
  # @apiGroup Verification
  # @apiDescription get details of specific selfie Verification.
  # @apiExample Example usage:
  # curl -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ.lKv0AacisCZz_YNhA5YrOtTftPTtEqUZuRe1td-wmoM" -i http://localhost:3000/api/v1/selfie_verifications/1 
  #
  # @apiSuccess {Integer}        id                         Selfie Verifications-ID.
  # @apiSuccess {Integer}        user_id                    User's-ID.
  # @apiSuccess {Blob}           avatar                     User's Selfie with his national id
  # @apiSuccess {Integer}        status                     Verification Status 
  # @apiSuccess {String}         note                       Admin note
  # @apiSuccess {datetime}       created_at                 Selfie Verification created at
  # @apiSuccess {datetime}       updated_at                 Selfie Verification updated at
  #
  # @apiError NoAccessRight Only authenticated users can access the data.
  # @apiError selfieVerificationNotFound   The <code>id</code> of the Selfie Verification was not found.
  #
  # @apiErrorExample Response (example):
  #     HTTP/1.1 401 Unauthorized
  #     {
  #       "error": "NoAccessRight"
  #    }
  # @apiErrorExample Error-Response:
  #     HTTP/1.1 404 Not Found
  #     {
  #       "error": "Selfie Verification NotFound"
  #    }
  # =end

  def show
    respond_to do |format|
      if current_user.id != @selfie_verification.user_id.to_i
        format.json { render json: "Not allowed"}
      else    
        format.json { render json: @selfie_verification }
      end
    end
  end


  # create new Selfie Verification
  # @param [Blob] avatar
  # @return [Integer] id
  # @return [String] username
  # @return [Blob] avatar
  # @return [Integer] status
  # @return [datetime] created_at
  # @return [datetime] updated_at


  # create new Selfie Verification
  # each user can create only one Selfie verification
  # POST /selfie_verifications
  # POST /selfie_verifications.json
  # =begin
  # @api {post} /selfie_verifications/ Create new selfie verification
  # @apiVersion 0.3.0
  # @apiName post Selfie Verification
  # @apiGroup Verification
  # @apiDescription create new selfie verification.
  #
  # @apiParam {Integer}      user_id               User's ID
  # @apiParam {Blob}         avatar                User's Selfie with his national id
  #
  # @apiExample Example usage:  
  # curl -X POST  http://localhost:3000/api/v1/selfie_verifications -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ.lKv0AacisCZz_YNhA5YrOtTftPTtEqUZuRe1td-wmoM" -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{"selfie_verification": {"avatar": }}' 
  # @apiSuccess {Integer}        id                         Selfie Verifications-ID.
  # @apiSuccess {Integer}        user_id                    User's-ID.
  # @apiSuccess {Blob}           avatar                     User's Selfie with his national id
  # @apiSuccess {Integer}        status                     Verification Status 
  # @apiSuccess {String}         note                       Admin note
  # @apiSuccess {datetime}       created_at                 Selfie Verification created at
  # @apiSuccess {datetime}       updated_at                 Selfie Verification updated at

  # @apiSuccessExample Response (example):
  #     HTTP/ 200 OK
  #     {
  #       "success": "200 ok"
  #    }
  #
  # @apiError NoAccessRight Only authenticated users can create User.
  #
  # @apiError MissingToken invalid-token.
  # @apiErrorExample Error-Response:
  #     HTTP/1.1 400 Bad Request
  #     {
  #       "error": "Missing Token"
  #    }
  # =end

  def create
    @current_user_verification = SelfieVerification.where("user_id =?", current_user.id.to_i).first
    if @current_user_verification != nil
      redirect_to root_path , notice: "not allowed"
    else
      @selfie_verification = SelfieVerification.new(selfie_verification_params)
      @selfie_verification.user_id = current_user.id
      respond_to do |format|
        if @selfie_verification.save
          format.json { render json: "Selfie verification was successfully created."}
          AuditlogJob.perform_async(user_id: current_user.id, user_name: current_user.uuid, action_type: "Upload Selfie verifications", action_meta: "Selfie verification was successfully created.", ip: request.env['REMOTE_ADDR'])
        else
          format.json { render json: @selfie_verification.errors}
        end
      end
    end
  end



      # edit Selfie Verification,
      # only admins can edit status and note.
      # =begin
      # @api {put} /selfie_verifications/:id/edit Update Selfie verification
      # @apiVersion 0.3.0
      # @apiName Put Selfie Verification
      # @apiGroup Verification
      # @apiDescription update data of a selfie_verification.
      #
      # @apiParam {Integer}      user_id               User's ID
      # @apiParam {Blob}         avatar                User's Selfie with his national id
      #
      # @apiExample Example usage:
      # curl -X PUT  http://localhost:3000/api/v1/selfie_verifications/1 -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ.lKv0AacisCZz_YNhA5YrOtTftPTtEqUZuRe1td-wmoM" -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{"selfie_verification": {"avatar": }}' 
      #
      # @apiSuccess {Integer}        id                         Selfie Verifications-ID.
      # @apiSuccess {Integer}        user_id                    User's-ID.
      # @apiSuccess {Blob}           avatar                     User's Selfie with his national id
      # @apiSuccess {Integer}        status                     Verification Status 
      # @apiSuccess {String}         note                       Admin note
      # @apiSuccess {datetime}       created_at                 Selfie Verification created at
      # @apiSuccess {datetime}       updated_at                 Selfie Verification updated at
      #
      # @apiError NoAccessRight Only authenticated users can update the data.
      # @apiError Selfie Verification NotFound   The <code>id</code> of the Selfie Verification was not found.
      #
      # @apiErrorExample Error-Response:
      #     HTTP/1.1 404 Not Found
      #     {
      #       "error": "Selfie Verification NotFound"
      #    }
      # @apiErrorExample Response (example):
      #     HTTP/1.1 401 Not Authenticated
      #     {
      #       "error": "NoAccessRight"
      #    }
      # =end

  def update
    respond_to do |format|
      if (@selfie_verification.status == "Verified") or (current_user.id != @selfie_verification.user_id.to_i)
        format.json { render json: "Not allowed"}
      else
        if @selfie_verification.update(selfie_verification_params)
          format.json { render json: "Selfie verification was successfully updated."}
          AuditlogJob.perform_async(user_id: current_user.id, user_name: current_user.uuid, action_type: "Edit Selfie verifications", action_meta: "Selfie verification was successfully updated.", ip: request.env['REMOTE_ADDR'])
        else
          format.json { render json: @selfie_verification.errors}
        end
      end
    end
  end

  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_selfie_verification
      @selfie_verification = SelfieVerification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def selfie_verification_params
      params.require(:selfie_verification).permit(:user_id, :note, :avatar, :status)
    end
end
