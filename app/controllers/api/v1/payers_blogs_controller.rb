module Api
  module V1
    class PayersBlogsController < ApplicationController
      before_action :set_payers_blog, only: [:show, :edit, :update, :destroy]


      # GET list of all payers blogs
      def users_blogs
        @payers_blogs = PayersBlog.where(active: 1).joins("INNER JOIN blogs_categories ON blogs_categories.id = payers_blogs.category_id").select("payers_blogs.*,blogs_categories.name as category_name").all      
        respond_to do |format|
          if @payers_blogs && @payers_blogs != []
            format.json { render json: @payers_blogs }
          else
            flash[:error] = "There are no categories."
            format.json { render json: flash }
          end
        end

        # =begin
        # @api {get} /api/v1/users_blogs 1-Request a list of all payers blogs
        # @apiVersion 0.3.0
        # @apiName GetPayersBlogs
        # @apiGroup PayersBlogs
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/users_blogs
        # @apiSuccess {Number} id blog unique ID. (Created automatically).
        # @apiSuccess {Number} author_id the user unique id.
        # @apiSuccess {String} title blog title.
        # @apiSuccess {String} slug blog slug.
        # @apiSuccess {String} content blog content.
        # @apiSuccess {String} keywords blog keywords.
        # @apiSuccess {Number} category_id the blog category unique id.
        # @apiSuccess {Number} active blog activity (1 for enable , 0 for disable).
        # @apiSuccess {String} category_name blog category name.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # [
        #   {
        #       "id": 1,
        #       "author_id": 1,
        #       "title": "اختبار المدونه",
        #       "slug": "مدونه",
        #       "content": "يجرى الان اختبار المدونه الخاصه بمهندسى بايرز",
        #       "keywords": "test_blog",
        #       "category_id": 2,
        #       "created_at": "2019-01-24T11:22:16.519Z",
        #       "updated_at": "2019-01-24T11:24:58.131Z",
        #       "active": 1,
        #       "category_name": "ROR"
        #   },
        #   {
        #       "id": 2,
        #       "author_id": 2,
        #       "title": "test blog",
        #       "slug": "مدونه",
        #       "content": "شرح كيفيه الاشتراك ف بايزر وتحويل الاموال بكل سهوله ويسر",
        #       "keywords": "new_account",
        #       "category_id": 1,
        #       "created_at": "2019-01-24T15:22:16.519Z",
        #       "updated_at": "2019-01-24T15:22:16.519Z",
        #       "active": 1,
        #       "category_name": "PHP"
        #   }
        # ]
        # @apiError NotFound no result found.
        # @apiErrorExample Error-Response:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "There are no Blogs."
        #   }
        # =end

      end 

      # GET list of payers blogs for a Specific author or category
      def catblogs
        if params[:user_id]
          @payers_blogs = PayersBlog.where(author_id: params[:user_id] , active: 1).all
        elsif params[:cat_id]
          @payers_blogs = PayersBlog.where(category_id: params[:cat_id] , active: 1).all
        end

        respond_to do |format|
          if @payers_blogs && @payers_blogs != []
            format.json { render json: @payers_blogs }
          else
            flash[:error] = "There are no results for this item."
            format.json { render json: flash }
          end
        end

        # =begin
        # @api {get} /api/v1/catblogs?user_id={:author_id}OR?cat_id={:category_id} 2-Request a list of payers blogs for a Specific author or category
        # @apiVersion 0.3.0
        # @apiName GetCatBlogs
        # @apiGroup PayersBlogs
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/catblogs?user_id=1
        # curl -i http://localhost:3000/api/v1/catblogs?cat_id=2
        # @apiParam {Number} author_id the user unique id.
        # @apiParam {Number} category_id the blog category unique id.
        # @apiSuccess {Number} id blog unique ID. (Created automatically).
        # @apiSuccess {Number} author_id the user unique id.
        # @apiSuccess {String} title blog title.
        # @apiSuccess {String} slug blog slug.
        # @apiSuccess {String} content blog content.
        # @apiSuccess {String} keywords blog keywords.
        # @apiSuccess {Number} category_id the blog category unique id.
        # @apiSuccess {Number} active blog activity (1 for enable , 0 for disable).
        # @apiSuccess {String} category_name blog category name.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # [
        #   {
        #       "id": 2,
        #       "author_id": 2,
        #       "title": "test blog",
        #       "slug": "مدونه",
        #       "content": "شرح كيفيه الاشتراك ف بايزر وتحويل الاموال بكل سهوله ويسر",
        #       "keywords": "new_account",
        #       "category_id": 1,
        #       "created_at": "2019-01-24T15:22:16.519Z",
        #       "updated_at": "2019-01-24T15:22:16.519Z",
        #       "active": 1
        #   }
        # ]
        # @apiError NotFound no result found.
        # @apiErrorExample Error-Response:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "There are no results for this item."
        #   }
        # =end
        
      end


      # GET a Specific Payers Blog
      def display_blog
        @payers_blog = PayersBlog.where(id: params[:id]).joins("INNER JOIN blogs_categories ON blogs_categories.id = payers_blogs.category_id").select("payers_blogs.*,blogs_categories.name as category_name").first
        
        respond_to do |format|
          if @payers_blog && @payers_blog != nil
            format.json { render json: @payers_blog }
          else
            flash[:error] = "This Blog Was Not Found"
            format.json { render json: flash }
          end
        end

        # =begin
        # @api {get} /api/v1/display_blog?id={:blog_id} 3-Request a Specific Blog
        # @apiVersion 0.3.0
        # @apiName GetSpecificBlog
        # @apiGroup PayersBlogs
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/catblogs?id=1
        # @apiParam {Number} id blog unique ID.
        # @apiSuccess {Number} id blog unique ID. (Created automatically).
        # @apiSuccess {Number} author_id the user unique id.
        # @apiSuccess {String} title blog title.
        # @apiSuccess {String} slug blog slug.
        # @apiSuccess {String} content blog content.
        # @apiSuccess {String} keywords blog keywords.
        # @apiSuccess {Number} category_id the blog category unique id.
        # @apiSuccess {Number} active blog activity (1 for enable , 0 for disable).
        # @apiSuccess {String} category_name blog category name.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # {
        #   "id": 1,
        #   "author_id": 1,
        #   "title": "اختبار المدونه",
        #   "slug": "مدونه",
        #   "content": "يجرى الان اختبار المدونه الخاصه بمهندسى بايرز",
        #   "keywords": "test_blog",
        #   "category_id": 2,
        #   "created_at": "2019-01-24T11:22:16.519Z",
        #   "updated_at": "2019-01-24T11:24:58.131Z",
        #   "active": 1,
        #   "category_name": "ROR"
        # }
        # @apiError NotFound no result found.
        # @apiErrorExample Error-Response:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "This Blog Was Not Found"
        #   }
        # =end
      end


      # GET a list of all payers blogs categories
      def payers_blogs_categories
        @categories = BlogsCategory.all
        respond_to do |format|
          if @categories && @categories != []
            format.json { render json: @categories }
          else
            flash[:error] = "There are no categories."
            format.json { render json: flash }
          end
        end
  
        # =begin
        # @api {get} /api/v1/payers_blogs_categories 4-Request a list of all payers blogs categories
        # @apiVersion 0.3.0
        # @apiName GetBlogsCategories
        # @apiGroup PayersBlogs
        # @apiExample Example usage:
        # curl -i http://localhost:3000/api/v1/payers_blogs_categories
        # @apiSuccess {Number} id category unique ID. (Created automatically).
        # @apiSuccess {String} name category name.
        # @apiSuccess {String} description category description.
        # @apiSuccess {Date} created_at  Date created.
        # @apiSuccess {Date} updated_at  Date Updated.
        # @apiSuccessExample Success-Response:
        # HTTP/1.1 200 OK
        # [
        #   {
        #       "id": 1,
        #       "name": "PHP",
        #       "description": "php laravel blog",
        #       "created_at": "2019-01-24T11:00:22.763Z",
        #       "updated_at": "2019-01-24T11:00:22.763Z"
        #   },
        #   {
        #       "id": 2,
        #       "name": "ROR",
        #       "description": "ruby on rails blog",
        #       "created_at": "2019-01-24T11:01:01.304Z",
        #       "updated_at": "2019-01-24T11:01:01.304Z"
        #   }
        # ]
        # @apiError NotFound no result found.
        # @apiErrorExample Error-Response:
        # HTTP/1.1 404 Not Found
        #   {
        #     "error": "There are no categories."
        #   }
        # =end
      end

      private
        # Use callbacks to share common setup or constraints between actions.
        def set_payers_blog
          @payers_blog = PayersBlog.where(author_id: current_user.id).first
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def payers_blog_params
          params.require(:payers_blog).permit(:author_id, :title, :slug, :content, :keywords, :category_id, :active)
        end
    end
  end
end
