class Api::V1::UsersBanksController < ApplicationController
  before_action :set_users_bank, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_request!
  skip_before_action :verify_authenticity_token
  attr_reader :current_user

  # GET /users_banks
  # GET /users_banks.json
  # =begin
  # @api {get} /api/users_banks 1-Request List of bank accounts
  # @apiVersion 0.3.0
  # @apiName GetBank
  # @apiGroup UsersBanks
  # @apiExample Example usage:
  # curl -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ.lKv0AacisCZz_YNhA5YrOtTftPTtEqUZuRe1td-wmoM" -i http://localhost:3000/api/v1/users_banks 
  # @apiSuccess {Number} id Bank unique ID.
  # @apiSuccess {Number} bank_id the bank id from banks model.
  # @apiSuccess {Number} user_id id of bank account's owner.
  # @apiSuccess {string} account_name Name of the owner of the acount.
  # @apiSuccess {string} account_number Bank account number.
  # @apiSuccess {boolean} active bank account status.
  # @apiSuccess {boolean} is_iban bank type swift or iban.
  # @apiSuccess {Date} created_at  Date created.
  # @apiSuccess {Date} updated_at  Date Updated.
  # @apiSuccessExample Success-Response:
  # HTTP/1.1 200 OK
  # [
  #      {
  #         "id": 1,
  #         "bank_id": 1,
  #         "user_id": 4,
  #         "account_name": "amira",
  #         "account_number": "123456",
  #         "active": true,
  #         "is_iban": false,
  #         "created_at": "2018-02-05T11:07:13.000Z",
  #         "updated_at": "2018-02-05T11:07:13.000Z"
  #     },
  # ]
  # @apiError MissingToken invalid Token.
  # @apiErrorExample Error-Response:
  # HTTP/1.1 400 Bad Request
  #   {
  #     "error": "Missing token"
  #   }
  # =end
  def index
    @users_banks = UsersBank.where(user_id: current_user.id).includes(:bank).all
    respond_to do |format|
      format.json { render json: @users_banks }
    end
  end

  # GET /users_banks/1
  # GET /users_banks/1.json
  # =begin
  # @api {get} /api/users_banks/{:id} 3-Request Specific User's Bank account
  # @apiVersion 0.3.0
  # @apiName GetSpecificUser'sBank
  # @apiGroup UsersBanks
  # @apiExample Example usage:
  # curl -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo0fQ.gpHtVLylE7gho3pfkXv6wG-EaSjUudkWTYzLo5rp8zg" -i http://localhost:3000/api/v1/users_banks/1 
  # @apiParam {Number} id Bank ID.
  # @apiSuccess {Number} id Bank unique ID.
  # @apiSuccess {Number} bank_id the bank id from banks model.
  # @apiSuccess {Number} user_id id of bank account's owner.
  # @apiSuccess {string} account_name Name of the owner of the acount.
  # @apiSuccess {string} account_number Bank account number.
  # @apiSuccess {boolean} active bank account status.
  # @apiSuccess {boolean} is_iban bank type swift or iban.
  # @apiSuccess {Date} created_at  Date created.
  # @apiSuccess {Date} updated_at  Date Updated.
  # @apiSuccessExample Success-Response:
  # HTTP/1.1 200 OK
  #     {
  #       "id": 1,
  #       "bank_id": 1,
  #       "user_id": 4,
  #       "account_name": "amira",
  #       "account_number": "123456",
  #       "active": true,
  #       "is_iban": false,
  #       "created_at": "2018-02-05T11:07:13.000Z",
  #       "updated_at": "2018-02-05T11:07:13.000Z"
  #      }
  # @apiError BankAccountNotFound The id of the User's Bank was not found. 
  # @apiErrorExample Error-Response1:
  # HTTP/1.1 404 Not Found
  #   {
  #     "error": "BankAccountNotFound"
  #   }
  # @apiError MissingToken invalid token.
  # @apiErrorExample Error-Response2:
  # HTTP/1.1 400 Bad Request
  #   {
  #     "error": "Missing token"
  #   }
  # =end

  def show
    respond_to do |format|
      format.json { render json: @users_bank }
    end
  end

  # POST /users_banks
  # POST /users_banks.json
  # =begin
  # @api {post} /api/users_banks 2-Create a new user's bank account
  # @apiVersion 0.3.0
  # @apiName PostBank
  # @apiGroup UsersBanks
  # @apiExample Example usage:
  # curl -X POST  http://localhost:3000/api/v1/users_banks -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo0fQ.gpHtVLylE7gho3pfkXv6wG-EaSjUudkWTYzLo5rp8zg" -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{"users_bank": {"bank_id": 1,"user_id": 4,"account_name": "amira", "account_number": "123456","active": 1, "is_iban": "swift"}}' 
  # @apiParam {Number}  bank_id  the general bank id.
  # @apiParam {string}  bank_name the name of the general bank, if create new general bank.
  # @apiParam {string}  branch_name the branch name of the bank, if create new general bank.
  # @apiParam {string}  bank_code the bank code, if create new general bank. .
  # @apiParam {Number}  country_id the bank country id, if create new general bank.
  # @apiParam {boolean} verified the bank status, default false until admin approve it, if create new general bank.
  # @apiParam {boolean} is_iban the generalbank status, if create new general bank.
  # @apiParam {Number}  user_id id of bank account's owner.
  # @apiParam {string}  account_name the bank account name.
  # @apiParam {string}  account_number the bank account number.
  # @apiParam {boolean} active the bank status.
  # @apiParam {boolean} is_iban the bank type 1 for iban 0 for swift.

  # @apiSuccess {Number}  id User's Bank unique ID.
  # @apiSuccess {Number}  bank_id the general bank id.
  # @apiSuccess {string}  account_name Name of the owner of the acount.
  # @apiSuccess {string}  account_number Bank account number.
  # @apiSuccess {boolean} is_iban the bank type 1 for iban 0 for swift.
  # @apiSuccess {boolean} active the bank status.
  # @apiSuccess {Date}    created_at  Date created.
  # @apiSuccess {Date}    updated_at  Date Updated.
  # @apiSuccessExample Success-Response:
  # HTTP/1.1 200 OK
  #     {
  #       "id": 1,
  #       "bank_id": 1,
  #       "user_id": 4,
  #       "account_name": "amira",
  #       "account_number": "123456",
  #       "active": true,
  #       "is_iban": false,
  #     }
  # @apiError MissingToken missing user name or password.
  # @apiErrorExample Error-Response1:
  # HTTP/1.1 422 Missing token
  #   {
  #     "error": "Missing token"
  #   }
  # @apiError InvalidSwiftCode bank swift code is invalid.
  # @apiErrorExample Error-Response2:
  # HTTP/1.1 Invalid Swift Code
  #   {
  #     "error": "is invalid"
  #   }
  
  def create
    @users_bank = UsersBank.new(users_bank_params)
    @users_bank.user_id = current_user.id
    if (@users_bank.bank_id == nil or @users_bank.bank_id == "")
      @bank = Bank.create(:bank_name => params[:users_bank][:bank_name], :branch_name => params[:users_bank][:branch_name], :bank_code => params[:users_bank][:bank_code], :country_id => params[:users_bank][:country_id], :is_iban => params[:users_bank][:is_iban_bank], :verified => 0)
      @users_bank.bank_id = @bank.id
    end
    respond_to do |format|
      if @users_bank.save
        format.json { render json: @users_bank, status: :created, location: @users_bank }
      else
        format.json { render json: @users_bank.errors, status: :unprocessable_entity }
      end
    end
  end


  # PATCH/PUT /users_banks/1
  # PATCH/PUT /users_banks/1.json
  # =begin
  # @api {put} /api/users_banks/{:id} 4-Update an existing User'sBank
  # @apiVersion 0.3.0
  # @apiName PutUser'sBank
  # @apiGroup UsersBanks
  # @apiExample Example usage:
  # curl -X PUT  http://localhost:3000/api/v1/users_banks/24 -H "Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo0fQ.gpHtVLylE7gho3pfkXv6wG-EaSjUudkWTYzLo5rp8zg" -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{"users_bank": {"account_name": "amiraaaaaaa", "account_number": "123456789","active": 0, "is_iban": true}}' 
  # @apiParam   {string}  account_name Bank Account Name.
  # @apiParam   {string}  account_number Bank Account Number.
  # @apiParam   {boolean} active User's Bank status.
  # @apiParam   {boolean} is_iban User's Bank type.
  # @apiSuccess {Number}  id User's Bank unique ID.
  # @apiSuccess {Number}  bank_id the general bank id.
  # @apiSuccess {string}  account_name Name of the owner of the acount.
  # @apiSuccess {string}  account_number Bank account number.
  # @apiSuccess {boolean} is_iban the bank type 1 for iban 0 for swift.
  # @apiSuccess {boolean} active the bank status.
  # @apiSuccess {Date}    created_at  Date created.
  # @apiSuccess {Date}    updated_at  Date Updated.
  # @apiSuccessExample Success-Response:
  # HTTP/1.1 200 OK
  #     {
  #       "id": 1,
  #       "bank_id": 1,
  #       "user_id": 4,
  #       "account_name": "amira",
  #       "account_number": "123456",
  #       "active": true,
  #       "is_iban": false,
  #     }
  # @apiError MissingToken missing user name or password.
  # @apiErrorExample Error-Response1:
  # HTTP/1.1 422 Missing token
  #   {
  #     "error": "Missing token"
  #   }
  # @apiError BankNotFound The id of the Bank was not found. 
  # @apiErrorExample Error-Response2:
  # HTTP/1.1 404 Not Found
  #   {
  #     "error": "BankNotFound"
  #   }
  # =end

  def update
    respond_to do |format|
      if @users_bank.update(users_bank_params)
          format.json { render json: @users_bank, status: :ok, location: @users_bank }
      else
      format.json { render json: @users_bank.errors, status: :unprocessable_entity }
      end
    end
  end

  

  # DELETE /users_banks/1
  # DELETE /users_banks/1.json
  def destroy
    @users_bank.destroy
    respond_to do |format|
      format.html { redirect_to users_banks_url, notice: 'Users bank was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_users_bank
      @users_bank = UsersBank.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def users_bank_params
      params.require(:users_bank).permit(:bank_id, :user_id, :account_name, :account_number, :active, :is_iban)
    end
end
