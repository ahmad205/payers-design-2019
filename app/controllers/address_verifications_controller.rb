class AddressVerificationsController < ApplicationController
  before_action :set_address_verification, only: [:show, :edit, :update, :destroy]
  before_action :require_login


  # create new Address Verification
  # @param [Integer] country_id
  # @param [String] city
  # @param [String] state
  # @param [String] street
  # @param [String] apartment_number
  # @param [Blob] attachment
  # @return [Integer] id
  # @return [String] username
  # @return [Integer] country_id
  # @return [String] state
  # @return [String] street
  # @return [String] apartment_number
  # @return [Blob] attachment
  # @return [Integer] status
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def create
    begin
      @address_verification = AddressVerification.new(address_verification_params)
      @address_verification.user_id = current_user.id
      if params[:address_verification][:address] != "" and params[:address_verification][:issue_date] != "" and params[:address_verification][:document_type] != "" and params[:address_verification][:name] != "" and params[:address_verification][:attachment] != nil 
        @address_verification.status = "Pending"
      else
        @address_verification.status = "UnVerified"
      end   
      respond_to do |format|
        if @address_verification.save
          if @address_verification.status == "Pending"
            format.html { redirect_to dashboard_path }
          else
            format.html { redirect_to dashboard_path }
          end
          format.json { render :show, status: :created, location: @address_verification }
          AuditlogJob.perform_async(user_id: current_user.id, user_name: current_user.uuid, action_type: "Upload address verifications", action_meta: "Address verification was successfully created.", ip: request.env['REMOTE_ADDR'])
        else
          format.html { redirect_to activate_account_path, notice: @address_verification.errors  }
          format.json { render json: @address_verification.errors, status: :unprocessable_entity }
        end
      end
    rescue =>e
      @code = SecureRandom.hex(4)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end

  # edit Address Verification,
  # only admins can edit status and note.
  # @param [Integer] country_id
  # @param [String] city
  # @param [String] state
  # @param [String] street
  # @param [String] apartment_number
  # @param [Blob] attachment
  # @param [Integer] status
  # @param [String] note
  # @return [Integer] id
  # @return [String] username
  # @return [Integer] country_id
  # @return [String] state
  # @return [String] street
  # @return [String] apartment_number
  # @return [Blob] attachment
  # @return [Integer] status
  # @return [String] note
  # @return [datetime] created_at
  # @return [datetime] updated_at
  def update
    begin
      if params[:address_verification][:address] != "" and params[:address_verification][:issue_date] != "" and params[:address_verification][:document_type] != "" and params[:address_verification][:name] != "" and ((params[:address_verification][:attachment] != nil) or (@address_verification.attachment.attached? == true)) 
        @address_verification.status = "Pending"
      else
        @address_verification.status = "UnVerified"
      end 
      respond_to do |format|
        if @address_verification.update(address_verification_params)
          if @address_verification.status == "Pending"
            format.html { redirect_to dashboard_path }
          else
            format.html { redirect_to dashboard_path }
          end
          format.json { render :show, status: :ok, location: @address_verification }
          AuditlogJob.perform_async(user_id: current_user.id, user_name: current_user.uuid, action_type: "Edit address verifications", action_meta: "Address verification was successfully updated.", ip: request.env['REMOTE_ADDR'])
        else
          format.html { redirect_to activate_account_path, notice: @address_verification.errors }
          format.json { render json: @address_verification.errors, status: :unprocessable_entity }
        end
      end
    rescue =>e
      @code = SecureRandom.hex(6)
      @log =  ErrorsLog.create(:user_id => current_user.id,:user_type => 2,:message => e.message.to_s,:code => @code.to_s) 
      redirect_to controller: 'errors_logs', action: 'error', id: @log.id    
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_address_verification
      @address_verification = AddressVerification.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def address_verification_params
      params.require(:address_verification).permit(:user_id, :document_type, :address, :issue_date, :name, :status, :note, :attachment)
    end
end
