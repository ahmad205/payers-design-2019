class PayersBlogsController < ApplicationController
  before_action :set_payers_blog, only: [:show, :edit, :update, :destroy]

  # GET list of payers blogs for a Specific user
  # @return [id] blog unique ID.
  # @return [author_id] the user unique id.
  # @return [title] blog title.
  # @return [slug] blog slug.
  # @return [content] blog content.
  # @return [keywords] blog keywords.
  # @return [category_id] the blog category unique id.
  # @return [active] blog activity (1 for enable , 0 for disable).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def index
    @payers_blogs = PayersBlog.where(author_id: current_user.id).all
  end

  # GET list of all payers blogs
  # @return [id] blog unique ID.
  # @return [author_id] the user unique id.
  # @return [title] blog title.
  # @return [slug] blog slug.
  # @return [content] blog content.
  # @return [keywords] blog keywords.
  # @return [category_id] the blog category unique id.
  # @return [active] blog activity (1 for enable , 0 for disable).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def users_blogs
    @payers_blogs = PayersBlog.where(active: 1).all.paginate(:page => params[:page], :per_page => 3)
    @categories = BlogsCategory.where(id: @payers_blogs.pluck(:category_id)).all
    render :layout => false
  end 

  # GET list of payers blogs for a Specific author or category
  # @param [Integer] user_id the user unique id.
  # @param [Integer] cat_id the blog category unique id.
  # @return [id] blog unique ID.
  # @return [title] blog title.
  # @return [slug] blog slug.
  # @return [content] blog content.
  # @return [keywords] blog keywords.
  # @return [active] blog activity (1 for enable , 0 for disable).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def catblogs
    if params[:user_id]
      @payers_blogs = PayersBlog.where(author_id: params[:user_id] , active: 1).all.paginate(:page => params[:page], :per_page => 3)
      @user = User.where(id: params[:user_id]).first
      @categories = BlogsCategory.where(id: @payers_blogs.pluck(:category_id)).all
      @info = 1
    elsif params[:cat_id]
      @payers_blogs = PayersBlog.where(category_id: params[:cat_id] , active: 1).all.paginate(:page => params[:page], :per_page => 3)
      @category = BlogsCategory.where(id: params[:cat_id]).first
      @info = 2
    end
  end

  def sitemap
    @base_url = "http://#{request.host_with_port}/"
    @blogs = PayersBlog.where(active: 1).all
    respond_to do |format|
      format.xml
    end
  end
  
  def search_blog
    @search = params[:search]
    @search_filter = params[:search_filter]

    @payers_blogs = PayersBlog.where(active: 1).all
    @payers_blogs = @payers_blogs.where("payers_blogs.keywords LIKE ? OR payers_blogs.title LIKE ? OR payers_blogs.content LIKE ? ","%#{@search}%","%#{@search}%","%#{@search}%").paginate(:page => params[:page], :per_page => 3).all  if  @search.present?

    if @search_filter &&  @search_filter.to_i == 2
      @payers_blogs = @payers_blogs.where("payers_blogs.keywords LIKE ?","%#{@search}%").paginate(:page => params[:page], :per_page => 3).all  if  @search.present?
    end
    
    if @search_filter && @search_filter.to_i == 3
      @payers_blogs = @payers_blogs.where("payers_blogs.title LIKE ?","%#{@search}%").paginate(:page => params[:page], :per_page => 3).all if  @search.present?
    end

    if @search_filter && @search_filter.to_i == 4
      @payers_blogs = @payers_blogs.where("payers_blogs.content LIKE ?","%#{@search}%").paginate(:page => params[:page], :per_page => 3).all   if  @search.present?
    end
    
    @categories = BlogsCategory.where(id: @payers_blogs.pluck(:category_id)).all

  end

  def search_blog_post
  end

  # GET /payers_blogs/1
  # GET /payers_blogs/1.json
  def show
  end

  def display_blog
    @payers_blog = PayersBlog.where(id: params[:id]).first
    @category = BlogsCategory.where(id: @payers_blog.category_id).first
  end

  # GET /payers_blogs/new
  def new
    @payers_blog = PayersBlog.new
  end

  # GET /payers_blogs/1/edit
  def edit
  end

  # POST /payers_blogs
  # POST /payers_blogs.json
  def create
    @payers_blog = PayersBlog.new(payers_blog_params)
    # @payers_blog.author_id = current_user.id.to_i
    @payers_blog.author_id = 1

    respond_to do |format|
      if @payers_blog.save
        format.html { redirect_to @payers_blog, notice: 'Payers blog was successfully created.' }
        format.json { render :show, status: :created, location: @payers_blog }
      else
        format.html { render :new }
        format.json { render json: @payers_blog.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /payers_blogs/1
  # PATCH/PUT /payers_blogs/1.json
  def update
    respond_to do |format|
      if @payers_blog.update(payers_blog_params)
        format.html { redirect_to @payers_blog, notice: 'Payers blog was successfully updated.' }
        format.json { render :show, status: :ok, location: @payers_blog }
      else
        format.html { render :edit }
        format.json { render json: @payers_blog.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /payers_blogs/1
  # DELETE /payers_blogs/1.json
  def destroy
    @payers_blog.destroy
    respond_to do |format|
      format.html { redirect_to payers_blogs_url, notice: 'Payers blog was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_payers_blog
      @payers_blog = PayersBlog.where(author_id: current_user.id).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def payers_blog_params
      params.require(:payers_blog).permit(:author_id, :title, :slug, :content, :keywords, :category_id, :active)
    end
end
