class UserAnnouncementsController < ApplicationController
  before_action :set_user_announcement, only: [:show, :edit, :update, :destroy]

  # GET list of user announcements and display it
  # @return [id] announcement unique ID.
  # @return [active] announcement activity (1 for enable , 0 for disable).
  # @return [title] announcement title.
  # @return [slug] announcement slug.
  # @return [description] announcement description.
  # @return [keywords] announcement keyword.
  # @return [availability] announcement available for (1 for all users , 2 for user group , for a specific user).
  # @return [user_group_id] the user group unique id.
  # @return [user_id] the user unique id.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def index
    @user_group_id = 1
    @user_announcement = UserAnnouncement.where(active: 1).all
    @user_announcements = @user_announcement.where("user_group_id = ? OR user_id = ? OR availability = ?", @user_group_id,current_user.id,1).all
  end

  # GET a Specific existing user announcement
  # @param [Integer] id announcement unique ID.
  # @return [active] announcement activity (1 for enable , 0 for disable).
  # @return [title] announcement title.
  # @return [slug] announcement slug.
  # @return [description] announcement description.
  # @return [keywords] announcement keyword.
  # @return [availability] announcement available for (1 for all users , 2 for user group , for a specific user).
  # @return [user_group_id] the user group unique id.
  # @return [user_id] the user unique id.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def show
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_announcement
      @user_announcement = UserAnnouncement.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_announcement_params
      params.require(:user_announcement).permit(:user_id, :active, :slug, :keyword, :title, :description, :availability, :user_group_id)
    end
end
