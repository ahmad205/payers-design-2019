class WithdrawsController < ApplicationController
  before_action :set_withdraw, only: [:show]

  # Get list of all the user withdraw operations
  # @return [id] withdraw unique ID (Created automatically).
  # @return [user_id] the user unique id.
  # @return [bank_id] the user bank account id.
  # @return [operation_id] the money operation unique id.
  # @return [amount] withdraw amount.
  # @return [fees] company fees.
  # @return [status] withdraw status (1 for Pending, 2 for Accepted , 3 for Cancelled).
  # @return [withdraw_type] withdraw bank type (local_bank, electronic_bank , digital_bank).
  # @return [note] withdraw user note.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def index
    @withdraws = Withdraw.where(user_id: current_user.id.to_i).all
  end

  # GET a spacific withdraw operation and display it
  # @param [Integer] id withdraw unique ID (Created automatically).
  # @param [Integer] user_id the user unique id.
  # @return [bank_id] the user bank account id.
  # @return [operation_id] the money operation unique id.
  # @return [amount] withdraw amount.
  # @return [fees] company fees.
  # @return [status] withdraw status (1 for Pending, 2 for accepted , 3 for cancelled).
  # @return [withdraw_type] withdraw bank type (local_bank, electronic_bank , digital_bank).
  # @return [note] withdraw user note.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def show
    @amount = @withdraw.amount + @withdraw.fees
    @time = @withdraw.updated_at + 2.hours
    if @time.strftime("%p") == "PM"
      @mor = "مساءا"
    else 
      @mor = "صباحا"
    end
    @month = UsersWalletsTransfer.checkmonth(@time.strftime("%m"))
    @operation_notification = Notification.where(opid: @withdraw.id , controller:"withdraws").all.order('created_at DESC').limit(3)
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_withdraw
      @withdraw = Withdraw.where(user_id: current_user.id.to_i, id: params[:id]).first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def withdraw_params
      params.require(:withdraw).permit(:user_id, :bank_id, :operation_id, :amount, :fees, :status, :withdraw_type, :note)
    end
end
