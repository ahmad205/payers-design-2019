class QuickPaymentsController < ApplicationController
  before_action :set_quick_payment, only: [:show, :edit, :update, :destroy]
  include Clearance::Authentication

  # GET list of quick payments pages
  # @return [id] quick payment page unique ID.
  # @return [user_id] the user unique id.
  # @return [active] quick payment page activity (1 for enable , 0 for disable).
  # @return [availability] quick payment page availability (1 for public , 2 for payers users only).
  # @return [slug] quick payment page link name.
  # @return [title] quick payment page title.
  # @return [description] quick payment page description. 
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def index
    @quick_payments = QuickPayment.all
  end

  # GET a Specific existing quick payment page
  # @param [Integer] id quick payment page unique ID.
  # @return [user_id] the user unique id.
  # @return [active] quick payment page activity (1 for enable , 0 for disable).
  # @return [availability] quick payment page availability (1 for public , 2 for payers users only).
  # @return [slug] quick payment page link name.
  # @return [title] quick payment page title.
  # @return [description] quick payment page description.
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def show
  end

  # GET a new quick payment page
  # @param [Integer] id quick payment unique ID.
  # @param [Integer] user_id the user unique id.
  # @param [Integer] active quick payment page activity (1 for enable , 0 for disable).
  # @param [Integer] availability quick payment page availability (1 for public , 2 for payers users only).
  # @param [String] slug quick payment page link name.
  # @param [String] title quick payment page title.
  # @param [String] description quick payment page description.
  def new
    @quick_payment = QuickPayment.new
  end

  # GET an existing quick payment page to edit params
  # @param [Integer] user_id the user unique id.
  # @param [Integer] active quick payment page activity (1 for enable , 0 for disable).
  # @param [Integer] availability quick payment page availability (1 for public , 2 for payers users only).
  # @param [String] title quick payment page title.
  # @param [String] description quick payment page description.
  def edit
  end

  # POST a new quick payment page and save it
  # @return [id] quick payment page unique ID.
  # @return [user_id] the user unique id.
  # @return [active] quick payment page activity (1 for enable , 0 for disable).
  # @return [availability] quick payment page availability (1 for public , 2 for payers users only).
  # @return [slug] quick payment page link name.
  # @return [title] quick payment page title.
  # @return [description] quick payment page description. 
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def create
    @quick_payment = QuickPayment.new(quick_payment_params)
    @quick_payment.user_id = current_user.id.to_i
    @quick_payment.active = 1

    respond_to do |format|
      if @quick_payment.save
        format.html { redirect_to @quick_payment, notice: 'Quick payment was successfully created.' }
        format.json { render :show, status: :created, location: @quick_payment }
      else
        format.html { render :new }
        format.json { render json: @quick_payment.errors, status: :unprocessable_entity }
      end
    end
  end

  # Change an existing quick payment page params
  # @return [id] quick payment page unique ID.
  # @return [user_id] the user unique id.
  # @return [active] quick payment page activity (1 for enable , 0 for disable).
  # @return [availability] quick payment page availability (1 for public , 2 for payers users only).
  # @return [slug] quick payment page link name.
  # @return [title] quick payment page title.
  # @return [description] quick payment page description. 
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def update
    respond_to do |format|
      if @quick_payment.update(quick_payment_params)
        format.html { redirect_to @quick_payment, notice: 'Quick payment was successfully updated.' }
        format.json { render :show, status: :ok, location: @quick_payment }
      else
        format.html { render :edit }
        format.json { render json: @quick_payment.errors, status: :unprocessable_entity }
      end
    end
  end

  def link_check
    @quick_payment = QuickPayment.where(slug: params[:slug]).all
    if @quick_payment == []
      @note = "true"
    else
      @note = "false"
    end
    render :json => {'check': @note }
  end

  def trust_user
    @user = User.authenticate(params[:email],params[:password])
    if @user == nil
      @note = "false"
    else
      @note = "true"
    end
    render :json => {'validate': @note }
  end

  def fast_pay
    @quick_payment = QuickPayment.where(slug: params[:slug]).first
    @page_owner = User.where(id: @quick_payment.user_id).first
    render :layout => false
  end

  def fast_pay_post

    @charge_type = params[:fast_pay][:charge_type].to_i
    @card_number = params[:fast_pay][:card_number].delete('-')

    ActiveRecord::Base.transaction do
      @quick_payment = QuickPayment.where(id: params[:fast_pay][:id]).first
      @page_owner = User.where(id: @quick_payment.user_id).first
      @user_wallet = UserWallet.where(user_id: @page_owner.id.to_i).first
      @transfer_amount = params[:fast_pay][:value].to_f
      @expenses = (@transfer_amount * @page_owner.users_group.recieve_ratio/100).to_f + @page_owner.users_group.recieve_fees.to_f
      @net_balance  = @transfer_amount -  @expenses
      @new_balance = @user_wallet.amount.to_f + @net_balance.to_f 
      @user_auth = User.authenticate(params[:fast_pay][:sender_mail],params[:fast_pay][:sender_pass])
      if @user_auth == nil
        @verify = "false"
      else
        @verify = "true"
      end

      if @charge_type == 1 && @verify == "true"
        #@transfer_amount = params[:fast_pay][:value]
        @sender_user = User.where("email LIKE  ?" , "%#{params[:fast_pay][:sender_mail].downcase}%").first
        @user_wallet_from = UserWallet.where(user_id: @sender_user.id.to_i).first
        @message = UserWallet.checktransfer(@sender_user,@page_owner,@transfer_amount,0)

        if @message == ""
          if @user_wallet_from.transfer_amount.to_f >= @transfer_amount.to_f
            @new_balance_from = @user_wallet_from.transfer_amount.to_f - @transfer_amount.to_f
            @user_wallet_from.update(:transfer_amount => @new_balance_from )
          elsif @user_wallet_from.transfer_amount.to_f < @transfer_amount.to_f
            @remaining_amount = @transfer_amount.to_f - @user_wallet_from.transfer_amount.to_f
            @new_balance_from = @user_wallet_from.amount.to_f - @remaining_amount.to_f
            @user_wallet_from.update(:amount => @new_balance_from , :transfer_amount => 0)
          end

          @user_wallet.update(:amount => @new_balance)
          @smstext = "#{@transfer_amount.to_f} USD has been successfully transferred to user #{@page_owner.firstname} #{@page_owner.lastname} Using Quick Payment Page"
          @smstext_to = "#{@net_balance.to_f} USD has been successfully added to your wallet by user #{@sender_user.firstname} #{@sender_user.lastname} Using Quick Payment Page"
          @money_operation = MoneyOp.create(:optype => 1,:amount => @transfer_amount, :fees => @expenses ,:payment_gateway => "Payers" ,:status => 1,:payment_date => DateTime.now,:user_id => @sender_user.id.to_i)
          @transfer = UsersWalletsTransfer.create(user_id: @sender_user.id , user_to_id:  @page_owner.id.to_i, transfer_method:"FromWallet" ,
          transfer_type: "Quick Payment" ,amount:@transfer_amount, :ratio => @expenses, approve: 1 ,operation_id:@money_operation.opid, address_id:0, service_status:2)             
          AuditlogJob.perform_async(user_id: @sender_user.id, user_name: @sender_user.uuid, action_type: "Balance transfer", action_meta: @smstext, ip: request.env['REMOTE_ADDR'])
          @notification = access_notification(@smstext_to,@page_owner,"users_wallets_transfers",@transfer.id)
          @page_owner.update(:total_monthly_recieve => @page_owner.total_monthly_recieve.to_f + @transfer_amount, :total_daily_recieve => @page_owner.total_daily_recieve.to_f + @transfer_amount)
          @sender_user.update(:total_monthly_send => @sender_user.total_monthly_send.to_f + @transfer_amount, :total_daily_send => @sender_user.total_daily_send.to_f + @transfer_amount, :daily_send_number => @sender_user.daily_send_number.to_i + 1 )
          #EmailNotification.email_notification_setting(user_mail:params[:fast_pay][:sender_mail],subject:'Recharge Wallet Balance',text:@smstext)
          redirect_back fallback_location: root_path, notice: "#{@transfer_amount.to_f} USD was successfully Charged to #{@page_owner.firstname} #{@page_owner.lastname} wallet"
        else
          redirect_back fallback_location: root_path,:notice => @message
        end

      elsif @charge_type == 2
        @carddata = Card.where(number: @card_number).first
        if @carddata == nil
          redirect_back fallback_location: root_path, notice: "Sorry , This Card is Not Found"
        else
          @card_type = @carddata.card_type
          @user_id = @carddata.user_id.to_i
          if Date.today > @carddata.expired_at
            redirect_back fallback_location: root_path, notice: "Sorry , This Card is Expired"
          else
            if @carddata.value == params[:fast_pay][:value].to_i

                if (@carddata.number == @card_number && @user_id == @page_owner.id.to_i && @carddata.status.to_i == 1) ||
                  (@carddata.number == @card_number && @card_type == 2 && @carddata.status.to_i == 1 && @page_owner.country_id.to_i == @carddata.country_id.to_i)
                  if @card_type == 2
                  @money_operation = MoneyOp.create(:optype => 4,:amount => @carddata.value ,:payment_gateway => "Payers" ,:status => 1,:payment_date => DateTime.now,:user_id => @page_owner.id.to_i)
                  @carddata.update(operation_id: @money_operation.opid , user_id: @page_owner.id)
                  end
                  @user_wallet.update(:amount => @new_balance )
                  @carddata.update(:status => 2)
                  @smstext_to = "A new Card with value of #{@net_balance.to_f} USD was successfully charged to your wallet By Quick Payment Page"
                  @smstext = "#{@carddata.value.to_f} USD has been successfully charged to user #{@page_owner.firstname} #{@page_owner.lastname} Using Quick Payment Page"
                  @notification = access_notification(@smstext_to,@page_owner,"money_ops",@money_operation.id)
                  #EmailNotification.email_notification_setting(user_mail:params[:fast_pay][:sender_mail],subject:'Recharge Wallet Balance',text:@smstext)
                  redirect_back fallback_location: root_path, notice: "Card with value of #{@carddata.value.to_f} USD was successfully Charged to #{@page_owner.firstname} wallet"

                else
                  redirect_back fallback_location: root_path, notice: 'Sorry Invalid Card , Try another one'
                end
            elsif @carddata.value < params[:fast_pay][:value].to_i
              redirect_back fallback_location: root_path, notice: 'Sorry , The balance you entered is larger than the card value'
            elsif @carddata.value > params[:fast_pay][:value].to_i
              redirect_back fallback_location: root_path, notice: 'The balance you entered is less than the card value ,Sign in to make use of the rest of the card value'
            end
          end
        end
      else
        redirect_back fallback_location: root_path, notice: 'Sorry you are not allowed'
      end

    end

  end

  # DELETE /quick_payments/1
  # DELETE /quick_payments/1.json
  def destroy
    @quick_payment.destroy
    respond_to do |format|
      format.html { redirect_to quick_payments_url, notice: 'Quick payment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_quick_payment
      @quick_payment = QuickPayment.where(id: params[:id] , user_id: current_user.id).first
    end

    def access_notification(smstext,user,control,opid)

      @smstext = smstext
      @user = user
      @user_notification_setting = NotificationsSetting.where(user_id: @user.id.to_i).first
      
      Notification.create(user_id: @user.id ,title: "Recharge Wallet Balance", description: @smstext , notification_type: @user_notification_setting.money_transactions, controller: control , opid: opid)
   
      if @user_notification_setting.money_transactions == 3
        SMSNotification.sms_notification_setting(@user.telephone,@smstext)
        SmsLog.create(:user_id => @user.id, :pinid => @smstext,:status => 1,:sms_type => 3)
        EmailNotification.email_notification_setting(user_mail:@user.email,subject:'Recharge Wallet Balance',text:@smstext)
      elsif @user_notification_setting.money_transactions == 2
        SMSNotification.sms_notification_setting(@user.telephone,@smstext)
        SmsLog.create(:user_id => @user.id, :pinid => @smstext,:status => 1,:sms_type => 3)
      elsif @user_notification_setting.money_transactions == 1
        EmailNotification.email_notification_setting(user_mail:@user.email,subject:'Recharge Wallet Balance',text:@smstext)
      end

    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def quick_payment_params
      params.require(:quick_payment).permit(:user_id, :active, :availability, :title, :description, :slug)
    end
end
