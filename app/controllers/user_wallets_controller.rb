class UserWalletsController < ApplicationController
  before_action :set_user_wallet, only: [:show]

  include ActionView::Helpers::NumberHelper

  # GET a spacific User Wallet and display it
  # @param [Integer] id User Wallet unique ID.
  # @return [currency] Users Wallets Currency (default = USD).
  # @return [amount] User Wallet amount.
  # @return [user_id] Wallet User ID.
  # @return [status] User Wallet status (0 for Disabled & 1 for Enabled).
  # @return [uuid] User Wallet Secure random number (Created automatically and must be 12 digits and letters).
  # @return [created_at] Date created.
  # @return [updated_at] Date Updated.
  def user_wallet_balance
    @user_wallet = UserWallet.where(:user_id => current_user.id.to_i).first
    @fozen_balance = UsersWalletsTransfer.where(user_to_id: current_user.id.to_i , approve:0).pluck(:amount).sum
  end


  # GET a new pin code SMS
  # @param [Integer] tel User phone number preceded by country code.
  def send_sms
  end

  # POST a new pin code SMS and send it to user
  # @return [pinId] SMS return pinId that was coming from the SMS provider.
  def send_sms_post
    SMSJob.perform_async(tel:params[:tel],user_id: current_user.id)
    redirect_to(verifysms_path)
  end

  # GET verify SMS code
  # @param [Integer] code SMS code that was sent to the user.
  def verifysms
  end

  # POST verify SMS code
  # @return [verified] Code Verification status(ture , false).
  def verifysms_post

    @code = params[:code].to_i
    @applications_list = SMSService.new(code:@code,user_id: current_user.id).call
    @userinfo = UserInfo.where("user_id =?", current_user.id ).first

    if @applications_list["verified"] ==  true
      @userinfo = UserInfo.where("user_id =?", current_user.id ).first
      if @userinfo != nil 
        @userinfo.update(:mobile => 1 )
        if @userinfo.address_verification_id != 0 and @userinfo.nationalid_verification_id != 0 and @userinfo.selfie_verification_id != 0
          @userinfo.update(:status => "Verified")
          User.set_usergroup_for_verified_users(current_user)
        end
      else
        UserInfo.create(:user_id => current_user.id ,:mobile => 1, :address_verification_id => 0, :nationalid_verification_id => 0, :selfie_verification_id => 0, :status => "UnVerified")
      end
      redirect_to root_path, notice: 'Valid code, successfully activated'  
    else 
      redirect_to root_path, notice: 'Wrong code Try again later'
    end

  end

  def check_receiver
    @user = User.where("email = ? OR account_number = ?", params[:usermail].downcase, params[:usermail]).first
    @message = UserWallet.checktransfer(current_user,@user,params[:amount],"")
    if @message == ""
      @expenses = (params[:amount].to_f * @user.users_group.recieve_ratio/100) + @user.users_group.recieve_fees.to_f
      @net_balance  = params[:amount].to_f -  @expenses
      render json: {'result' => true , "username" => "#{@user.firstname + " " + @user.lastname + " - " + @user.email}",
      'net_balance' => @net_balance.round(2) , 'expenses' => @expenses.round(2)}
    else
      render json: {'result' => @message }
    end
  end

  # Transfer Balances between users wallets
  # @param [Integer] userfrom The sender user ID.
  # @param [Integer] userto The receiver user ID.
  # @param [Float] transferamount The transferred amount between two users.
  def transfer_balance
    @user_addresses = Address.where("user_id = ?", current_user.id.to_i).all
    @transfer_status = Setting.where(key: ["transfer_status","transfer_msg"]).all
    @aa = (current_user.user_wallet.amount + current_user.user_wallet.transfer_amount).round(2)
    @str = number_with_delimiter(@aa, :delimiter => ',')
    @user_country = Country.where(id:current_user.country_id).pluck(:short_code).first
    if @user_country == "EG"
      @currency_name = t('users.EGP')
    elsif @user_country == "SA"
      @currency_name = t('users.SAR')
    else
      @currency_name = t('users.dollar')
    end
    @currency_ratio = Setting.check_currency_ratio(@user_country)

    render layout:false
  end

  # Post a transferred balance between two users
  # @return [userfrom] The sender user ID.
  # @return [userto] The receiver user ID.
  # @return [transferamount] The transferred amount between two users.
  # @return [new_balance_from] The sender new balance after transfer.
  # @return [new_balance_to] The receiver new balance after transfer.
  def transfer_balancepost
    @user_from = current_user.id.to_i
    @user = User.where("email = ? OR account_number = ?", params[:userto].downcase,params[:userto]).first
    
    @transfer_amount = params[:transferamount].to_f
    @message = UserWallet.checktransfer(current_user,@user,@transfer_amount,params[:hold_period])
    if @message == ""
      @user_to = @user.id
      ActiveRecord::Base.transaction do

        @user_wallet_from = UserWallet.where(user_id: @user_from.to_i).first
        @user_wallet_to = UserWallet.where(user_id: @user_to.to_i).first
        
        # Calculate the balance of the sender's wallets after transfer
        if @user_wallet_from.transfer_amount.to_f >= @transfer_amount
          @new_balance_from = @user_wallet_from.transfer_amount.to_f - @transfer_amount
          @user_wallet_from.update(:transfer_amount => @new_balance_from )
        elsif @user_wallet_from.transfer_amount.to_f < @transfer_amount
          @remaining_amount = @transfer_amount - @user_wallet_from.transfer_amount.to_f
          @new_balance_from = @user_wallet_from.amount.to_f - @remaining_amount.to_f
          @user_wallet_from.update(:amount => @new_balance_from , :transfer_amount => 0)
        end

        @expenses = (@transfer_amount * @user.users_group.recieve_ratio/100) + @user.users_group.recieve_fees.to_f
        @net_balance  = @transfer_amount -  @expenses
        @new_balance_to = @user_wallet_to.amount.to_f + @net_balance.to_f

        if params[:hold_period] == "" || params[:hold_period].to_i == 0
          @approve = 1
          @service_status = 2
          @user_wallet_to.update(:amount => @new_balance_to )
          @smstext = "you_have_processed_anew_sending_of/#{@transfer_amount}/USD_to_user/#{@user.firstname} #{@user.lastname}/your_transaction_has_been_successfully_created/the_amount_has_been_added_to_the_user_wallet"
          @smstext_to = "user/#{current_user.firstname} #{current_user.lastname}/has_added/#{@net_balance.round(2).to_f}/USD_to_your_wallet/the_amount_has_been_successfully_added_to_your_account"
          @money_operation = MoneyOp.create(:optype => 1,:amount => @transfer_amount, :fees => @expenses ,:payment_gateway => "Payers" ,:status => 1,:payment_date => DateTime.now,:user_id => current_user.id.to_i)
          @user.update(:total_monthly_recieve => @user.total_monthly_recieve.to_f + @transfer_amount, :total_daily_recieve => @user.total_daily_recieve.to_f + @transfer_amount)

        else
          @approve = 0
          @service_status = 0
          @smstext = "you_have_processed_anew_sending_of/#{@transfer_amount}/USD_to_user/#{@user.firstname} #{@user.lastname}/your_transaction_is_inprogress/the_amount_will_be_added_to_the_user_the_holding_period"
          @smstext_to = "user/#{current_user.firstname} #{current_user.lastname}/has_added/#{@net_balance.round(2).to_f}/USD_to_your_wallet/it_will_be_added_to_your_wallet_after_the_holding_period"
          @money_operation = MoneyOp.create(:optype => 1,:amount => @transfer_amount, :fees => @expenses ,:payment_gateway => "Payers" ,:status => 0,:payment_date => DateTime.now,:user_id => current_user.id.to_i)
        end


        if params[:transfer_type] == "Service"
          @address = 0
        elsif params[:transfer_type] == "Product"
          @address = params[:address_id]
        end
        @transfer = UsersWalletsTransfer.create(user_id: current_user.id , user_to_id:  @user_to.to_i, transfer_method:params[:transfer_method] ,
        transfer_type: params[:transfer_type] ,hold_period: params[:hold_period] ,amount:@transfer_amount, ratio: @expenses ,note: params[:note], approve: @approve ,operation_id:@money_operation.opid, address_id:@address, service_status:@service_status)       
        current_user.update(:total_monthly_send => current_user.total_monthly_send.to_f + @transfer_amount, :total_daily_send => current_user.total_daily_send.to_f + @transfer_amount, :daily_send_number => current_user.daily_send_number.to_i + 1 )
        AuditlogJob.perform_async(user_id: current_user.id, user_name: current_user.uuid, action_type: "Balance transfer", action_meta: @smstext, ip: request.env['REMOTE_ADDR'])
        
        @notification = access_notification(@smstext,@smstext_to,@user,"users_wallets_transfers",@transfer.id)
        redirect_to("#{request.base_url}/users_wallets_transfers/#{@transfer.id}")

      end
    else
      redirect_to(transfer_balance_path,:notice => @message )
    end
  end

  def confirm_transfer
    ActiveRecord::Base.transaction do
      @WalletTransfer = UsersWalletsTransfer.where(id: params[:id]).first
      if @WalletTransfer.approve == 0
      @net_amount = @WalletTransfer.amount.to_f - @WalletTransfer.ratio.to_f
      @user_wallet_to = UserWallet.where(user_id: @WalletTransfer.user_to_id.to_i).first
      @money_op = MoneyOp.where(opid: @WalletTransfer.operation_id).first
      @user = User.where(id: @WalletTransfer.user_to_id.to_i).first
      @new_balance_to = @user_wallet_to.amount.to_f + @net_amount.to_f
      @WalletTransfer.update(:approve => 1 , :service_status => "Completed")
      @user_wallet_to.update(:amount => @new_balance_to)
      @money_op.update(:status => 1)
      @user.update(:total_monthly_recieve => @user.total_monthly_recieve.to_f + @WalletTransfer.amount.to_f, :total_daily_recieve => @user.total_daily_recieve.to_f + @WalletTransfer.amount.to_f)
      @smstext = "you_have_confirmed_sending/#{@WalletTransfer.amount.to_f}/USD_to_user/#{@user.firstname} #{@user.lastname}/the_amount_was_successfully_sent"
      @smstext_to = "user/#{current_user.firstname} #{current_user.lastname}/has_confirmed/#{@net_amount.round(2).to_f}/USD_to_your_wallet/the_amount_has_been_successfully_added_to_your_account"
      AuditlogJob.perform_async(user_id: current_user.id, user_name: current_user.uuid, action_type: "Balance transfer confirm", action_meta: @smstext, ip: request.env['REMOTE_ADDR'])
      access_notification(@smstext,@smstext_to,@user,"users_wallets_transfers",@WalletTransfer.id)
      end
      redirect_to("#{request.base_url}/users_wallets_transfers/#{params[:id]}")
    end
  end
  
  def refund_transfer
    ActiveRecord::Base.transaction do
      @WalletTransfer = UsersWalletsTransfer.where(id: params[:id]).first
      @user_wallet = UserWallet.where(user_id: current_user.id.to_i).first
      @user_wallet_to = UserWallet.where(user_id: @WalletTransfer.user_id.to_i).first
      @money_op = MoneyOp.where(opid: @WalletTransfer.operation_id).first
      @sender_user = User.where(id: @WalletTransfer.user_id.to_i).first
      @refunded =  @WalletTransfer.amount.to_f - @WalletTransfer.ratio.to_f 
      @balance_after_refund = @user_wallet.amount.to_f - @refunded
      @new_balance_to = @user_wallet_to.amount.to_f + @WalletTransfer.amount.to_f
      if @WalletTransfer.approve == 1
        @user_wallet.update(:amount => @balance_after_refund)
      end
      if @WalletTransfer.created_at < 1.day.ago
        @sender_user.update(:total_monthly_send => @sender_user.total_monthly_send.to_f - @WalletTransfer.amount.to_f)
        if @WalletTransfer.approve == 1
          current_user.update(:total_monthly_recieve => current_user.total_monthly_recieve.to_f - @WalletTransfer.amount.to_f)
        end
      else
        @sender_user.update(:total_monthly_send => @sender_user.total_monthly_send.to_f - @WalletTransfer.amount.to_f, :total_daily_send => @sender_user.total_daily_send.to_f - @WalletTransfer.amount.to_f, :daily_send_number => @sender_user.daily_send_number.to_i - 1 )
        if @WalletTransfer.approve == 1
          current_user.update(:total_monthly_recieve => current_user.total_monthly_recieve.to_f - @WalletTransfer.amount.to_f, :total_daily_recieve => current_user.total_daily_recieve.to_f - @WalletTransfer.amount.to_f)
        end
      end

      @user_wallet_to.update(:amount => @new_balance_to)
      @WalletTransfer.update(:approve => 2 , :service_status => "Cancelled")
      @money_op.update(:status => 2)
      @smstext = "you_have_refunded/#{@refunded.round(2).to_f}/USD_to_user/#{@sender_user.firstname} #{@sender_user.lastname}/the_amount_has_been_deleted_from_your_wallet"
      @smstext_to = "user/#{current_user.firstname} #{current_user.lastname}/has_refunded/#{@WalletTransfer.amount.round(2).to_f}/USD_to_your_wallet/the_amount_has_been_successfully_added_to_your_account"
      AuditlogJob.perform_async(user_id: current_user.id, user_name: current_user.uuid, action_type: "Balance transfer refund", action_meta: @smstext, ip: request.env['REMOTE_ADDR'])
      
      access_notification(@smstext,@smstext_to,@sender_user,"users_wallets_transfers",@WalletTransfer.id)
      redirect_to("#{request.base_url}/users_wallets_transfers/#{params[:id]}")
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user_wallet
      @user_wallet = UserWallet.find(params[:id])
    end

    def access_notification(smstext,smstext_to,user,control,opid)

      @tel = current_user.telephone
      @user_mail = current_user.email
      @user_from = current_user.id.to_i
      @user = user
      @user_to = @user.id
      @smstext = smstext
      @smstext_to = smstext_to
      @splitxt_to = @smstext_to.split('/')
      @splitxt = @smstext.split('/')
      if @splitxt[5]
        @smstext_translation = t("mails.#{@splitxt[0]}") + "#{@splitxt[1]}" + t("mails.#{@splitxt[2]}") + "#{@splitxt[3]}" + t("mails.#{@splitxt[4]}") + t("mails.#{@splitxt[5]}")
      else
        @smstext_translation = t("mails.#{@splitxt[0]}") + "#{@splitxt[1]}" + t("mails.#{@splitxt[2]}") + "#{@splitxt[3]}" + t("mails.#{@splitxt[4]}")
      end

      if @splitxt_to[5]
        @smstext_to_translation = t("mails.#{@splitxt_to[0]}") + "#{@splitxt_to[1]}" + t("mails.#{@splitxt_to[2]}") + "#{@splitxt_to[3]}" + t("mails.#{@splitxt_to[4]}") + t("mails.#{@splitxt_to[5]}")
      else
        @smstext_to_translation = t("mails.#{@splitxt_to[0]}") + "#{@splitxt_to[1]}" + t("mails.#{@splitxt_to[2]}") + "#{@splitxt_to[3]}" + t("mails.#{@splitxt_to[4]}")
      end
      @user_notification_setting = NotificationsSetting.where(user_id: @user_from).first
      @user_notification_setting_to = NotificationsSetting.where(user_id: @user_to).first
      @mail_text = "#{opid}/#{current_user.id}"
      @mail_text_to = "#{opid}/#{@user.id}"
      Notification.create([{user_id: @user_from ,title: "Balance transfer", description: @smstext , notification_type: @user_notification_setting.money_transactions,controller: control,opid: opid},
      {user_id: @user_to ,title: "Balance transfer", description: @smstext_to , notification_type: @user_notification_setting_to.money_transactions,controller: control,opid: opid}])

      if @user_notification_setting.money_transactions == 3
        SMSNotification.sms_notification_setting(@tel,@smstext_translation)
        SmsLog.create(:user_id => @user_from, :pinid => @smstext_translation,:status => 1,:sms_type => 3)
        EmailNotification.email_notification_setting(user_mail:@user_mail,subject:'wallet transfer balance',text:@mail_text)
      elsif @user_notification_setting.money_transactions == 2
        SMSNotification.sms_notification_setting(@tel,@smstext_translation)
        SmsLog.create(:user_id => @user_from, :pinid => @smstext_translation,:status => 1,:sms_type => 3)
      elsif @user_notification_setting.money_transactions == 1
        EmailNotification.email_notification_setting(user_mail:@user_mail,subject:'wallet transfer balance',text:@mail_text)
      end

      if @user_notification_setting_to.money_transactions == 3
        SMSNotification.sms_notification_setting(@user.telephone,@smstext_to_translation)
        SmsLog.create(:user_id => @user_to, :pinid => @smstext_to_translation,:status => 1,:sms_type => 3)
        EmailNotification.email_notification_setting(user_mail:@user.email,subject:'wallet transfer balance',text:@mail_text_to)
      elsif @user_notification_setting_to.money_transactions == 2
        SMSNotification.sms_notification_setting(@user.telephone,@smstext_to_translation)
        SmsLog.create(:user_id => @user_to, :pinid => @smstext_to_translation,:status => 1,:sms_type => 3)
      elsif @user_notification_setting_to.money_transactions == 1
        EmailNotification.email_notification_setting(user_mail:@user.email,subject:'wallet transfer balance',text:@mail_text_to)
      end

    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_wallet_params
      params.require(:user_wallet).permit(:currency, :amount, :user_id, :status, :uuid, :transfer_amount)
    end
end
