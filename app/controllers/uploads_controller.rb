class UploadsController < ApplicationController
  before_action :set_upload, only: [:show, :edit, :update, :destroy]

  # GET /uploads
  # GET /uploads.json
  def index
    @uploads = Upload.where(user_id: current_user.id.to_i).all
  end

  # GET /uploads/1
  # GET /uploads/1.json
  def show
  end

  # GET /uploads/new
  def new
    @upload = Upload.new
  end

  # GET /uploads/1/edit
  def edit
  end

  def testmail
    render layout:false
  end

  def create
    
    @file_name = image_upload
    @upload = Upload.create(:title => params[:upload][:title],:user_id => current_user.id.to_i)
    @upload.avatar.attach(io: File.open(@file_name), filename: @file_name, content_type: "image/jpg")

    # @upload = Upload.new(upload_params)
    respond_to do |format|

      if @upload
        format.html { redirect_to @upload, notice: 'Upload was successfully created.' }
        format.json { render :show, status: :created, location: @upload }
      else
        format.html { render :new }
        format.json { render json: @upload.errors, status: :unprocessable_entity }
      end

    end

  end

  # PATCH/PUT /uploads/1
  # PATCH/PUT /uploads/1.json
  def update

    @file_name = image_upload
    @upload_update = @upload.update(:title => params[:upload][:title])
    @upload.avatar.attach(io: File.open(@file_name), filename: @file_name, content_type: "image/jpg")

    respond_to do |format|
      if @upload_update
        format.html { redirect_to @upload, notice: 'Upload was successfully updated.' }
        format.json { render :show, status: :ok, location: @upload }
      else
        format.html { render :edit }
        format.json { render json: @upload.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /uploads/1
  def destroy
    @upload.destroy
    respond_to do |format|
      format.html { redirect_to uploads_url, notice: 'Upload was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_upload
      @upload = Upload.where(user_id: current_user.id.to_i, id: params[:id]).first
    end

    def image_upload
      @avatar = params[:upload][:avatar]
      @image_data = ActiveSupport::JSON.decode(@avatar)
      @data_im = @image_data['output']['image']
  
      require 'mime/types'
      @REGEXP = /\Adata:([-\w]+\/[-\w\+\.]+)?;base64,(.*)/m
  
      data_uri_parts = @data_im.match(@REGEXP) || []
      extension = MIME::Types[data_uri_parts[1]].first.preferred_extension
      rand_id = rand 100000..999999
      @file_name = "#{rand_id}.#{extension}"
      @bb = File.open(@file_name, 'wb') do |file|
        @aa= file.write(Base64.decode64(data_uri_parts[2]))
      end
      
      return @file_name
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def upload_params
      params.require(:upload).permit(:title,:user_id)
    end
end
