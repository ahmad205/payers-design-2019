class SMSNotification < ApplicationRecord

    # Manage SMS notification setting
    # @param [Integer] length The SMS characters length.
    # @param [String] site_signature the sender name(Payers).
    # @param [Integer] tel The receiver user phone number.
    # @param [String] smstext Text SMS content.
    # @param [String] SMSService_Provider The SMS provider name.
    # @param [String] Localized_String The SMS sending language.
    def self.sms_notification_setting(tel,smstext)

        # settingssms
        @length = 220 #chars
        @site_signature = "Payers"

        # content
        @text = smstext
        @tel = tel
        @SMSService_Provider = "Infobip"     
        # @SMSTemplates = smstemplate.passwordchanged

        # msgContent
        @Localized_String = "english"

        if @text.length <= @length
          SMSJob.perform_async(tel:@tel,smstext:@text)
        end

    end

end