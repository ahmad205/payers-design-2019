class Setting < ApplicationRecord
    has_one_attached :attachment

    def self.check_currency_ratio(country)
        @country = country
        if @country == "EG"
            @ratio = Setting.where(key:"usd_to_egp").pluck(:value).first
        elsif @country == "SA"
            @ratio = Setting.where(key:"usd_to_sar").pluck(:value).first
        else
            @ratio = "1"
        end

        return @ratio
    end
end
