class UserWallet < ApplicationRecord
    validates_uniqueness_of :uuid
    validates_uniqueness_of :user_id
    before_save :default_values
    belongs_to :user ,:foreign_key => "user_id"

    # Add default values to currency and uuid before save
    def default_values
        self.currency ||= "USD"
        self.uuid ||= SecureRandom.hex(6)
    end

    # Check before confirming transfer balance between users
    # @param [Integer] userfrom The sender user ID.
    # @param [Integer] userto The receiver user ID.
    # @param [Float] transferamount The transferred amount between two users.
    # @return [message] error message if there is any error occurs while checking process.
    def self.checktransfer(user_from ,user_to ,transfer_amount,hold_period)
        @from_user = user_from
        @to_user = user_to

        @user_from_exist = UserWallet.where(user_id: @from_user.id.to_i).first
        if (@to_user != nil)
          @user_to_exist = UserWallet.where(user_id: @to_user.id.to_i).first
        end
        @max_transfer = WalletsTransferRatio.where(key: "max_transfer").pluck(:value).first
        @min_transfer = WalletsTransferRatio.where(key: "min_transfer").pluck(:value).first
        @total_exist_amount = @user_from_exist.amount.to_f + @user_from_exist.transfer_amount.to_f

        
        

        @message = ""

        if @user_from_exist != nil
            if @from_user.total_daily_send.to_f + transfer_amount.to_f  > @from_user.users_group.maximum_daily_send   
                @message  = @message + I18n.t("user_wallets.exceeded_the_maximum_send_day")
            end 
            if @from_user.total_monthly_send.to_f + transfer_amount.to_f > @from_user.users_group.maximum_monthly_send.to_f 
                @message  = @message + I18n.t("user_wallets.exceeded_the_maximum_send_month")
            end 
            if transfer_amount.to_f < @from_user.users_group.minimum_send_value
                @message  = @message + I18n.t("user_wallets.maximum_transfer") + "#{@from_user.users_group.minimum_send_value}" + I18n.t("users.USD")
            end 
            if transfer_amount.to_f > @from_user.users_group.maximum_send_value
                @message  = @message + I18n.t("user_wallets.minimum_transfer") + "#{@from_user.users_group.maximum_send_value}" + I18n.t("users.USD")
            end 
            if @from_user.daily_send_number.to_i >= @from_user.users_group.daily_send_number
                @message  = @message + I18n.t("user_wallets.exceeded_the_allowed_sending_day")
            end

            
            if @total_exist_amount < transfer_amount.to_f
                @message  = @message + I18n.t("user_wallets.you_dont_have_enough_money")
            end

            #if transfer_amount.to_f < @min_transfer.to_f
                #@message  = @message + "Sorry, The transferd balance must be greater than #{@min_transfer} USD"
            #end

            #if transfer_amount.to_f > @max_transfer.to_f
                #@message  = @message + "Sorry, The transferd balance must be less than or equal #{@max_transfer} USD"
            #end

            if hold_period.to_i < 0
                @message  = @message + I18n.t("user_wallets.holding_period_cant_be_less_than_zero")
            end

            if @user_from_exist.status == 0
                @message  = @message + I18n.t("user_wallets.Your_Wallet_was_Disabled")
            end
        else
            @message  = @message + I18n.t("user_wallets.sender_not_found")
        end

        if @to_user == nil
            @message  = @message + I18n.t("user_wallets.receiver_not_found")
        else
            if @user_to_exist.status == 0
                @message  = @message + I18n.t("user_wallets.Receiver_Wallet_was_Disabled")
            end
        end      

        if (@to_user and @from_user and @from_user.id.to_i == @to_user.id.to_i)
            @message  = @message + I18n.t("user_wallets.transfer_to_yourself")
        end

        return   @message 
    end

end
