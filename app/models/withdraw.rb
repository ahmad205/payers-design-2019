class Withdraw < ApplicationRecord
    belongs_to :users_bank ,:foreign_key => "bank_id"
    validates :amount, presence: true
    validates :bank_id, presence: true
    validates :withdraw_type, presence: true

    # Check before confirming withdraw balance operation
    # @param [Integer] user_id The user unique ID.
    # @param [Float] withdraw_amount The withdraw amount.
    # @return [message] error message if there is any error occurs while checking process.
    def self.checkwithdraw(user_id,withdraw_amount,withdraw_type,bank_id)

        @current_user = User.where(id: user_id.to_i).first

        @user_wallet = UserWallet.where(user_id: user_id.to_i).first
        @user_group = UsersGroup.where(id: @current_user.users_group_id).first
        @group_bank_limits = GroupsBanksLimit.where(bank_type: "local_bank", group_id: @current_user.users_group_id).first
        @user_bank = UsersBank.where(id: bank_id.to_i).first
        @bank = Bank.where(id: @user_bank.bank_id.to_i).first
        # @max_withdraw = WalletsTransferRatio.where(key: "max_withdraw").pluck(:value).first
        # @min_withdraw = WalletsTransferRatio.where(key: "min_withdraw").pluck(:value).first
        
        @message = ""

        if @user_wallet != nil
            if @user_wallet.amount.to_f < withdraw_amount.to_f
                @message  = @message + I18n.t("user_wallets.you_dont_have_enough_money")
            end

            if withdraw_amount.to_f <= 0
                @message  = @message + I18n.t("withdraws.cannot_withdraw_zero")
            end

            if @user_wallet.status == 0
                @message  = @message + I18n.t("user_wallets.Your_Wallet_was_Disabled")
            end
        else
            @message  = @message + I18n.t("user_wallets.sender_not_found")
        end

        if withdraw_type == nil || withdraw_type == ""
            @message  = @message + I18n.t("withdraws.must_choose_withdraw_method")
        elsif bank_id == nil || bank_id == ""
            @message  = @message + I18n.t("withdraws.must_choose_withdraw_bank")
        end

        if @user_bank.active == false
            @message  = @message + I18n.t("withdraws.bank_is_disabled_by_you")
        end

        if @bank.verified == false
            @message  = @message + I18n.t("withdraws.bank_is_disabled_by_admin")
        end

        if withdraw_amount.to_f < @user_group.minimum_withdraw_per_time
            @message  = @message + I18n.t("withdraws.withdraw_greater_than") + "#{@user_group.minimum_withdraw_per_time}" + " " + I18n.t("users.USD")
        end

        if (@current_user.total_daily_withdraw.to_f + withdraw_amount.to_f) > (@group_bank_limits.maximum_daily_withdraw.to_f)
            @message  = @message + I18n.t("withdraws.exceeded_daily_withdraw_amount")
        end

        if (@current_user.total_monthly_withdraw.to_f + withdraw_amount.to_f) > (@group_bank_limits.maximum_monthly_withdraw.to_f)
            @message  = @message + I18n.t("withdraws.exceeded_monthly_withdraw_amount")
        end

        if (@current_user.daily_withdraw_number.to_i) >= (@user_group.daily_withdraw_number.to_i)
            @message  = @message + I18n.t("withdraws.exceeded_daily_withdraw_requests")
        end

        return   @message 
    end

end
