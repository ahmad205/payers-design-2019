class UsersBank < ApplicationRecord
    belongs_to :bank , foreign_key: "bank_id", optional: true
    belongs_to :user , foreign_key: "user_id"
    has_many :withdraw , foreign_key: "bank_id"
    attr_accessor :bank_name, :bank_code, :country_id, :is_iban_bank
    validates :bank_id, presence: true

end
