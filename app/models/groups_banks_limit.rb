class GroupsBanksLimit < ApplicationRecord
    enum bank_type: { local_bank: 1, electronic_bank: 2, digital_bank: 3 }
    validates_numericality_of :maximum_daily_withdraw, :maximum_monthly_withdraw, :maximum_daily_deposite, :maximum_monthly_deposite, :greater_than_or_equal_to => 0.0    

end
