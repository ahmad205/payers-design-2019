class AuditLog < ApplicationRecord
    validates_uniqueness_of :trace_id
    before_save :default_values

    def default_values
        self.trace_id ||= [*('0'..'9'),*('a'..'z')].to_a.shuffle[0,16].join
    end
end
