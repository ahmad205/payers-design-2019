class UsersWalletsTransfer < ApplicationRecord
    belongs_to :user ,:foreign_key => "user_id"
    validates :user_id, presence: true
    validates :user_to_id, presence: true
    validates :amount, presence: true
    validates :transfer_type, presence: true
    enum transfer_method: { FromWallet: 1, FromVisa: 2 }
    enum transfer_type: { Product: 1, Service: 2 , "Quick Payment": 3}
    enum service_status: { Pending: 0 ,"In processing":1 ,Completed: 2 ,"Cancelled":3 }


    def self.checkmonth(month)
        @time_month = month
        if @time_month == "01"
        @month = "يناير"
        elsif @time_month == "02"
        @month = "فبراير"
        elsif @time_month == "03"
        @month = "مارس"
        elsif @time_month == "04" 
        @month = "أبريل"
        elsif @time_month == "05" 
        @month = "مايو"
        elsif @time_month == "06" 
        @month = "يونيو"
        elsif @time_month == "07" 
        @month = "يوليو"
        elsif @time_month == "08"
        @month = "أغسطس"
        elsif @time_month == "09"
        @month = "سبتمبر"
        elsif @time_month == "10"
        @month = "أكتوبر"
        elsif @time_month == "11" 
        @month = "نوفمبر"
        elsif @time_month == "12"
        @month = "يسمبر"
        end

        return @month 
    end
end
