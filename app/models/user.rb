class User < ApplicationRecord
  include Clearance::User
  
  has_many :users_bank , foreign_key: "user_id"

  has_many :watchdog , foreign_key: "user_id"
  has_many :login , foreign_key: "user_id"
  #has_many :user_verification , foreign_key: "user_id"
  has_one :user_info , foreign_key: "user_id"
  has_one :nationalid_verification , foreign_key: "user_id"
  has_one :selfie_verification , foreign_key: "user_id"
  has_one :address_verification , foreign_key: "user_id"
  has_one :affilate_program , foreign_key: "user_id"

  has_one :verification , foreign_key: "user_id"
  belongs_to :country ,:foreign_key => "country_id"
  belongs_to :users_group ,:foreign_key => "users_group_id"

  has_one_attached :avatar
  has_many :address , foreign_key: "user_id"
  has_one :user_wallet , foreign_key: "user_id"
  has_many :users_wallets_transfer , foreign_key: "user_id"
  has_many :money_op , foreign_key: "user_id"
  has_many :bank_account , foreign_key: "user_id"
  has_many :ticket , foreign_key: "user_id"
  has_many :payers_blog , foreign_key: "author_id"

  #validates :username,uniqueness: { case_sensitive: false }
  validates :email, uniqueness: true, presence: true, format: { with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, on: :create }
  validates :password, length: 8..20, presence: true, if: :should_validate_password?
  validate :check_password_format, if: :should_validate_password?
  validates :firstname, length: { maximum: 30 }, presence: true
  validates :lastname, length: { maximum: 30 }, presence: true
  #validates :firstname, length: { maximum: 30 } , if: :should_validate_firstname?
  #validates :lastname, length: { maximum: 30 }, if: :should_validate_lastname?
  validates :secret_code, length: {maximum: 10 }, :allow_blank => true
  validates :telephone, uniqueness: true, :numericality => true, :length => { :maximum => 20 }, if: :should_validate_phone?
  #validate :valid_phone_code, if: :should_validate_phone?
  validate :validate_country_id
  validates_inclusion_of :active_otp, :in => [1, 2, 3], :allow_nil => false
  validates_inclusion_of :account_currency, in: [true, false]
  # validates_inclusion_of :roleid, :in => [1, 2], :allow_nil => false
  validate :validate_refered_by, if: :should_validate_refered_by?
  def should_validate_refered_by?
    refered_by.present?
  end
  def validate_refered_by
      @refered_by = User.where("invitation_code =? ",self.refered_by).first
      if @refered_by == nil 
        errors.add(:refered_by, "is invalid")
      end
  end
  def should_validate_phone?
    telephone.present?
  end
  def should_validate_password?
    password.present?
  end
  #def should_validate_firstname?
    #firstname.present?
  #end
  #def should_validate_lastname?
    #lastname.present?
  #end
  
  def check_password_format
    regexps = {" must contain at least one lowercase letter" => /[a-z]+/, 
               " must contain at least one uppercase letter" => /[A-Z]+/, 
               " must contain at least one digit" => /\d+/}
    regexps.each do |rule, reg|
      errors.add(:password, rule) unless password.match(reg)
    end
  end

  def validate_country_id
    errors.add(:country_id, "is invalid") unless Country.exists?(self.country_id)
  end

  def valid_phone_code
      @phone_code = Country.where("id =? ",self.country_id).first
      errors.add(:telephone, "Must start with a valid country code") unless self.telephone.starts_with?(@phone_code.Phone_code)
  end

  before_create :set_user_data
  def set_user_data
    self.uuid = SecureRandom.hex(4)
    self.invitation_code = SecureRandom.hex(4)
    #self.auth_token = SecureRandom.hex(6)
    self.password_changed_at = Time.now
  end

  def self.before_sign_out()
    @user_sessions = Login.where(user_id: current_user.id).all
    if (@user_sessions  != nil)
       @user_sessions.all.each do |user_sessions|
          user_sessions.destroy
       end
    end
    cookies.delete(:auth_token)
    cookies.delete(:device_id)
    #session[:refered_by] = nil
    session[:last_visit] = nil
  end

  #validate :check_password_format, on: :create
  #validates :password, presence: true, format: { with: /\A(?=.*[a-zA-Z])(?=.*[0-9]).{8,}$\z/}
  #validates :password, length: 6..20, presence: true, on: :create 
  #validates_confirmation_of :password
  #validates :country_id, presence: true, numericality: { only_integer: true }
  # validates :telephone /\A\d{3}-\d{3}-\d{4}\z/
  #validates :active_otp, inclusion: %w(1 2)
  #validates_inclusion_of :active_otp, in: 1..2
  #validates :account_currency 
  #validates :roleid 
  #validates :secret_code, length: { minimum: 6, maximum: 10 }    
  
  attr_accessor :refered_by
  #attr_accessor :confirm_password

  has_one_time_password   
  
  def self.user_details(user)
    # @acc_num =  "PS1" + "%08d" % user.id.to_i 
    @user_country = user.country.short_code  
    @users_group = UsersGroup.where(:country_code => "Public" , :classification => 0 ).first.id
    user.update(:users_group_id => @users_group)
    return @users_group
  end

  def self.set_usergroup_for_verified_users(user)
    @user_country = user.country.short_code
    @related_group = UsersGroup.where(:country_code => @user_country ).order("classification DESC").first
    if @related_group != nil 
      @users_group = @related_group.id
    else
      @public_group = UsersGroup.where(:country_code => "Public").order("classification DESC").first
      @users_group = @public_group.id
    end
    user.update(:users_group_id => @users_group)
  end

  def self.statics(type)
    @users = User.all
    if type == "count_users"
      @result = @users
    elsif type == "confirmed"
      @result = @users.where("status =? ",1).all
    elsif type == "disabled"
      @result = @users.where("disabled =? ",1).all
    end
    return @result
  end

end
