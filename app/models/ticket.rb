class Ticket < ApplicationRecord
    has_many :ticket_replies
    validates_presence_of :number
    validates_presence_of :title
    validates_presence_of :content
    enum status: { Open: 0, OnHold: 1, Answered: 2, Closed: 3 }
    enum priority: { Low: 1, Normal: 2, High: 3, Urgent: 4, "Report to management": 5 }
    enum evaluation: { "Very Bad": 1, "Bad": 2, "Good": 3, "Very Good": 4 , "Excellent":5 }
    belongs_to :user ,:foreign_key => "user_id"
    before_create :set_random_number

    private

    def set_random_number
        if Ticket.where(number: number).exists?
        self.number = generate_number
        end
    end

    def generate_number
        loop do
        token = SecureRandom.rand(100000...999999)
        break token unless Ticket.where(number: token).exists?
        end
    end


end
