class QuickPayment < ApplicationRecord
    validates_uniqueness_of :user_id
    validates_uniqueness_of :slug
end
