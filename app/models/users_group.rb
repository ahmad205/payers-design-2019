class UsersGroup < ApplicationRecord
    has_many :user , foreign_key: "users_group_id"
    validate :maximum_recieve
    validates_numericality_of :upgrade_limit, :maximum_daily_send, :maximum_monthly_send, :maximum_daily_recieve, :maximum_monthly_recieve, :minimum_send_value, :maximum_send_value, :daily_send_number, :minimum_withdraw_per_time, :minimum_deposite_per_time, :daily_withdraw_number, :daily_deposite_number, :recieve_fees, :recieve_ratio, :withdraw_fees, :withdraw_ratio, :withdraw_ratio, :deposite_fees, :deposite_ratio, :greater_than_or_equal_to => 0.0
    attr_accessor :local_maximum_daily_withdraw, :local_maximum_monthly_withdraw, :local_maximum_daily_deposite, :local_maximum_monthly_deposite,
                  :online_maximum_daily_withdraw, :online_maximum_monthly_withdraw, :online_maximum_daily_deposite, :online_maximum_monthly_deposite,
                  :crypto_maximum_daily_withdraw, :crypto_maximum_monthly_withdraw, :crypto_maximum_daily_deposite, :crypto_maximum_monthly_deposite
        
    def maximum_recieve
        errors.add(:maximum_monthly_recieve, "Should be greater than or equal to upgrade_limit") unless self.maximum_monthly_recieve >= self.upgrade_limit
    end

    def self.group_bank_limit(group_id)
      @groups_banks_limits = GroupsBanksLimit.where(group_id: group_id.to_i ).all
      @local_limits = @groups_banks_limits.where(bank_type: "local_bank").first
      @online_limits = @groups_banks_limits.where(bank_type: "electronic_bank").first
      @crypto_limits = @groups_banks_limits.where(bank_type: "digital_bank").first

      return @local_limits , @online_limits, @crypto_limits
    end

end
