class Card < ApplicationRecord
    has_one_attached :payment_receipt
    validates_uniqueness_of :number
    # validate :check_value
    # validate :check_expiration
    before_save :default_values
    # validates :value, numericality: { only_integer: true }
    validates :status, numericality: { only_integer: true }
    validates :user_id, numericality: { only_integer: true , greater_than: 0 }
    has_many :cards_log , foreign_key: "card_id"

    def default_values
        self.number ||= [*('0'..'9'),*('a'..'z')].to_a.shuffle[0,16].join
    end

    def self.checkdeposit(current_user,amount,bank_type,bank_key)
        @user = current_user
        @value = amount.to_f
        @bank_type = bank_type
        @bank_key = bank_key
        @user_group = UsersGroup.where(id: @user.users_group_id).first
        @group_bank_limits = GroupsBanksLimit.where(bank_type: @bank_type, group_id: @user.users_group_id).first
        @company_bank = CompanyBank.where(bank_key: @bank_key).first
        @user_account_status = UserInfo.where(user_id: @user.id).first

        @message = ""

        if @value < @user_group.minimum_deposite_per_time.to_f
            @message  = @message + I18n.t("cards.deposit_greater_than") + "#{@user_group.minimum_deposite_per_time}" + " "  + I18n.t("users.USD")
        end

        if (@user.daily_deposite_number.to_i) >= (@user_group.daily_deposite_number.to_i)
            @message  = @message + I18n.t("cards.exceeded_daily_deposit_requests")
        end

        if (@user.total_daily_deposite.to_f + @value) > (@group_bank_limits.maximum_daily_deposite.to_f)
            @message  = @message + I18n.t("cards.exceeded_daily_deposit_amount")
        end

        if (@user.total_monthly_deposite.to_f + @value) > (@group_bank_limits.maximum_monthly_deposite.to_f)
            @message  = @message + I18n.t("cards.exceeded_monthly_deposit_amount")
        end

        if !(@user_account_status) or (@user_account_status.status == "UnVerified")
          if (@company_bank.unverified_deposite == false)
            @message  = @message + I18n.t("cards.bank_disabled_for_unverified_users")
          end
        elsif (@company_bank.verified_deposite == false)
          @message  = @message + I18n.t("cards.bank_is_disabled_by_admin")         
        end

        if @user.allowed_bank_level < @company_bank.severity_level
          @message  = @message + I18n.t("cards.this_bank_is_blocked_for_you") 
        end

        return   @message 



    end

    def self.payers_deposit_expenses(users_group,value)
      @users_group = users_group.to_i
      @value = value.to_f
      @deposite_expenses = UsersGroup.where(:id => @users_group.to_i).first
      @expenses = @value * (@deposite_expenses.deposite_ratio/100) + @deposite_expenses.deposite_fees
      return   @expenses 

    end

    # def check_value
    #   errors.add(:value, "must be equal or greater than 5 USD") if value == nil || value < 5
    # end

    # def check_expiration
    #     errors.add(:expired_at, "Must be at least two days from now") if expired_at < Date.today + 2.days
    # end

end
