class Address < ApplicationRecord
    validates :user_id, numericality: { only_integer: true , greater_than: 0 }
    # validate :check_address
    validates :country, presence: true
    validates :governorate, presence: true
    validates :city, presence: true
    validates :street, presence: true
    before_save :check_default
    belongs_to :user ,:foreign_key => "user_id"
    #validate :check_default

    # check the length of the address (must be at least 5 letters)
    # @param [Integer] address the detailed address.
    # def check_address
    #     errors.add(:address, "This address is too short, must be more than 5 letters") if address == nil || address.length < 5
    # end

    # check the default address before save (only one address must be defualt)
    # @param [Boolean] default_address defualt address for a user(true , false).
    def check_default
        if default_address == true
            @user_addresses = Address.where(:user_id => user_id).where.not(:id => id).where(:default_address => true)
            @user_addresses.update_all(default_address: false)

        elsif default_address == false
            @user_addresses2 = Address.where(user_id: user_id,default_address: true).where.not(id: id).all
            if @user_addresses2.count < 1
                self.default_address ||= true
            end
        end
    end

end
