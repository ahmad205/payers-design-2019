class Country < ApplicationRecord
    has_many :user , foreign_key: "country_id"
    has_many :bank , foreign_key: "country_id"
    #has_many :address_verification , foreign_key: "country_id"
    validates_presence_of :short_code, :Phone_code, :Full_Name, :Currency, :language, :active
    validates_uniqueness_of :short_code, :Phone_code, :Full_Name
    validates_inclusion_of :active, :in => [0, 1]
    validates :Phone_code, :numericality => true
end
