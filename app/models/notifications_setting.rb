class NotificationsSetting < ApplicationRecord
    validates_uniqueness_of :user_id
    
    # check which of these notifications will be enabled (mail only , message only , both mail and message)
    # @param [Integer] money_transactions_mail enable mail notifications for money transactions(1 for mail).
    # @param [Integer] money_transactions_message enable message notifications for money transactions(2 for message).
    # @param [Integer] pending_transactions_mail enable mail notifications for pending transactions(1 for mail).
    # @param [Integer] pending_transactions_message enable message notifications for pending transactions(2 for message).
    # @param [Integer] transactions_updates_mail enable mail notifications for current transactions updates(1 for mail).
    # @param [Integer] transactions_updates_message enable message notifications for current transactions updates(2 for message).
    # @param [Integer] help_tickets_updates enable mail notifications for help tickets updates(1 for mail).
    # @param [Integer] help_tickets_updates enable message notifications for help tickets updates(2 for message).
    # @param [Integer] tickets_replies enable mail notifications for new ticket replies(1 for mail).
    # @param [Integer] tickets_replies enable message notifications for new ticket replies(2 for message).
    # @param [Integer] account_login enable mail notifications for login to user account(1 for mail).
    # @param [Integer] account_login enable message notifications for login to user account(2 for message).
    # @param [Integer] change_password enable mail notifications for change password(1 for mail).
    # @param [Integer] change_password enable message notifications for change password(2 for message).
    # @param [Integer] verifications_setting enable mail notifications for two factor auth status(1 for mail).
    # @param [Integer] verifications_setting enable message notifications for two factor auth status(2 for message).
    # @return [money_transactions] Withdrawal,transfer and remittance transactions(0 for non ,1 for mail ,2 for message ,3 for mail & message).
    # @return [pending_transactions] pending transactions(0 for non ,1 for mail ,2 for message ,3 for mail & message).
    # @return [transactions_updates] current transactions updates(0 for non ,1 for mail ,2 for message ,3 for mail & message).
    # @return [help_tickets_updates] help tickets updates(0 for non ,1 for mail ,2 for message ,3 for mail & message).
    # @return [tickets_replies] new ticket replies(0 for non ,1 for mail ,2 for message ,3 for mail & message).
    # @return [account_login] login to user account(0 for non ,1 for mail ,2 for message ,3 for mail & message).
    # @return [change_password] change password(0 for non ,1 for mail ,2 for message ,3 for mail & message).
    # @return [verifications_setting] when enable or disable two factor auth(0 for non ,1 for mail ,2 for message ,3 for mail & message).
    def self.usernotification(money_transactions_mail,money_transactions_sms,pending_transactions_mail,pending_transactions_sms,
      transactions_updates_mail,transactions_updates_sms,help_tickets_updates_mail,help_tickets_updates_sms,tickets_replies_mail,
      tickets_replies_sms,account_login_mail,account_login_sms,change_password_mail,change_password_sms,verifications_setting_mail,
      verifications_setting_sms)
  
      if money_transactions_mail == 1 && money_transactions_sms == 1
        @money_transactions = 3
      elsif money_transactions_sms == 1
        @money_transactions = 2
      elsif money_transactions_mail == 1
        @money_transactions = 1 
      else
        @money_transactions = 0
      end

      if pending_transactions_mail == 1 && pending_transactions_sms == 1
      @pending_transactions = 3
      elsif pending_transactions_sms == 1
      @pending_transactions = 2
      elsif pending_transactions_mail == 1
      @pending_transactions = 1 
      else
      @pending_transactions = 0
      end

      if transactions_updates_mail == 1 && transactions_updates_sms == 1
      @transactions_updates = 3
      elsif transactions_updates_sms == 1
      @transactions_updates = 2
      elsif transactions_updates_mail == 1
      @transactions_updates = 1 
      else
      @transactions_updates = 0
      end

      if help_tickets_updates_mail == 1 && help_tickets_updates_sms == 1
      @help_tickets_updates = 3
      elsif help_tickets_updates_sms == 1
      @help_tickets_updates = 2
      elsif help_tickets_updates_mail == 1
      @help_tickets_updates = 1 
      else
      @help_tickets_updates = 0
      end


      if tickets_replies_mail == 1 && tickets_replies_sms == 1
      @tickets_replies = 3
      elsif tickets_replies_sms == 1
      @tickets_replies = 2
      elsif tickets_replies_mail == 1
      @tickets_replies = 1 
      else
      @tickets_replies = 0
      end

      if account_login_mail == 1 && account_login_sms == 1
      @account_login = 3
      elsif account_login_sms == 1
      @account_login = 2
      elsif account_login_mail == 1
      @account_login = 1 
      else
      @account_login = 0
      end

      if change_password_mail == 1 && change_password_sms == 1
      @change_password = 3
      elsif change_password_sms == 1
      @change_password = 2
      elsif change_password_mail == 1
      @change_password = 1
      else
      @change_password = 0 
      end

      if verifications_setting_mail == 1 && verifications_setting_sms == 1
      @verifications_setting = 3
      elsif verifications_setting_sms == 1
      @verifications_setting = 2
      elsif verifications_setting_mail == 1
      @verifications_setting = 1 
      else
      @verifications_setting = 0
      end

      return @money_transactions,@pending_transactions,@transactions_updates,@help_tickets_updates,@tickets_replies,
      @account_login,@change_password,@verifications_setting
  
    end
  
  end
  