class AffilateProgram < ApplicationRecord
    belongs_to :user ,:foreign_key => "user_id"
    enum profit_source: { fb_referral: 1, tw_referral: 2, email_invite: 3, invitation_link: 4, commission_code: 5, insta_referral: 6  }

end
