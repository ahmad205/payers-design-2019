json.extract! payers_blog, :id, :author_id, :title, :slug, :content, :keywords, :category_id, :created_at, :updated_at
json.url payers_blog_url(payers_blog, format: :json)
