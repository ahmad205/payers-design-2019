json.extract! users_wallets_transfer, :id, :user_id, :user_to_id, :transfer_method, :transfer_type, :hold_period, :amount, :ratio, :approve, :note, :created_at, :updated_at
json.url users_wallets_transfer_url(users_wallets_transfer, format: :json)
