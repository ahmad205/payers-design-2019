json.extract! bank, :id, :bank_name, :branch_name, :bank_code, :country_id, :is_iban, :verified, :created_at, :updated_at
json.url bank_url(bank, format: :json)
