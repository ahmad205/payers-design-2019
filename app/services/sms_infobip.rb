# app/services/sms_infobip.rb

class SMSInfobip
    attr_accessor :tel, :smstext , :code
    require 'net/http'
    require 'net/https'

    # GET SMS params
    # @param [Integer] tel The receiver user phone number.
    # @param [String] smstext Text SMS content.
    # @param [Integer] code SMS Verification Pin Code.
    def initialize(tel:0,smstext:"",code:0,user_id:1)
      
      @phone = tel
      @sms_content = smstext
      @code = code
      @user_id = user_id
      @applicationId = "451C42313BC234ABF92206EEA0796C1B"
      @messageId = "9A9EDA35B0552EF4E942BF272ADA1955"
      @text_uri = URI.parse("https://api.infobip.com/sms/1/text/single")
      @pin_uri = URI.parse("https://api.infobip.com/2fa/1/pin")
      @voice_pin_uri = URI.parse("https://api.infobip.com/2fa/1/pin/voice")
      
    end
  
    # Send a notification text SMS to a specific user
    # @param [Integer] tel The receiver user phone number.
    # @param [String] smstext Text SMS content.
    def send_text_sms
        
        uri = @text_uri
        request = Net::HTTP::Post.new(uri)
        request["authorization"] = 'Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=='
        request["content-type"] = 'application/json'
        request["accept"] = 'application/json'
        request.body = JSON.dump({    
        "from":"Payers",
        "to": @phone,
        "text": @sms_content
        })
        req_options = {
          use_ssl: uri.scheme == "https",
        }
    
        response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
          @response = http.request(request)
          @res= @response.body
          @applications_list= ActiveSupport::JSON.decode(@res)
        end

    end

    # POST a new SMS pin code and send it to a specific user
    # @param [Integer] tel The receiver user phone number.
    # @return [pinId] SMS return pinId that was coming from the SMS provider(Infobip).
    def send_voice_pin_sms
        
      uri = @voice_pin_uri
      request = Net::HTTP::Post.new(uri)
      request["authorization"] = 'Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=='
      request["content-type"] = 'application/json'
      request["accept"] = 'application/json'
      request.body = JSON.dump({    
      "applicationId": @applicationId,
      "messageId": @messageId,
      "from":"Payers",
      "to": @phone,
      })
      req_options = {
        use_ssl: uri.scheme == "https",
      }
  
      response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
        @response = http.request(request)
        @res= @response.body
        @applications_list= ActiveSupport::JSON.decode(@res)
        @sms_log = SmsLog.create(:user_id => @user_id, :pinid => @applications_list["pinId"],:status => 0,:sms_type => 1)
      end
    end

    def send_pin_sms
        
      uri = @pin_uri
      request = Net::HTTP::Post.new(uri)
      request["authorization"] = 'Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=='
      request["content-type"] = 'application/json'
      request["accept"] = 'application/json'
      request.body = JSON.dump({    
      "applicationId": @applicationId,
      "messageId": @messageId,
      "from":"Payers",
      "to": @phone,
      })
      req_options = {
        use_ssl: uri.scheme == "https",
      }
  
      response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
        @response = http.request(request)
        @res= @response.body
        @applications_list= ActiveSupport::JSON.decode(@res)
        @sms_log = SmsLog.create(:user_id => @user_id, :pinid => @applications_list["pinId"],:status => 0,:sms_type => 1)
      end

    end

    def resend_pin_sms

      @log = SmsLog.where(:user_id => @user_id,:sms_type => 1).last
      @pin_id = @log.pinid        
      uri = URI.parse("https://api.infobip.com/2fa/1/pin/#{@pin_id}/resend")
      request = Net::HTTP::Post.new(uri)
      request["authorization"] = 'Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=='
      request["content-type"] = 'application/json'
      request["accept"] = 'application/json'
      
      req_options = {
        use_ssl: uri.scheme == "https",
      }
  
      response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
        @response = http.request(request)
        @res= @response.body
        @applications_list= ActiveSupport::JSON.decode(@res)
      end

      return @applications_list

    end

    # Verify the SMS pin code
    # @param [Integer] code SMS code that was sent to the user.
    # @param [String] pinId SMS return pinId that was coming from Infobip.
    # @return [verified] Code Verification status(ture , false).
    def verify_pin_sms
      
      @log = SmsLog.where(:user_id => @user_id,:sms_type => 1).last
      @pin_id = @log.pinid
      uri = URI.parse("https://api.infobip.com/2fa/1/pin/#{@pin_id}/verify")
      request = Net::HTTP::Post.new(uri)
      request["authorization"] = 'Basic cGF5ZXJzOk02Q2lNNGtSSmNsbQ=='
      request["content-type"] = 'application/json'
      request["accept"] = 'application/json'
      request.body = JSON.dump({
      "pin": @code
      })     
       req_options = {
         use_ssl: uri.scheme == "https",
       }
       
      response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
        @response = http.request(request)
        @res= @response.body
        @applications_list= ActiveSupport::JSON.decode(@res)      
      end

      if @applications_list["verified"] ==  true
        @log.update(:status => 1)
      end
      
      return @applications_list
      
    end

end