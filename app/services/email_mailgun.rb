# app/services/email_mailgun.rb

class EmailMailgun

    attr_accessor :user_mail, :subject , :text , :via
    require 'mailgun'

    # GET Email params
    # @param [Integer] user_mail The receiver user email address.
    # @param [String] subject The subject of the email.
    # @param [String] text The email content text.
    # @param [String] via the sender email address(payers@payers.com).
    def initialize(user_mail:'client@payers.com',subject:'Welcome To Payers',text:'Welcome',via:'payers@payers.com')
      
      @user_mail = user_mail
      @subject = subject
      @text = text
      @from = via

    end

    # send text email through mailgun
    def send_email
   
        mg_client = Mailgun::Client.new 'key-1173f0da990305a7bcf06f4d2ef45d09'
        message_params = {:from    => @from,
                          :to      => @user_mail,
                          :subject => @subject,
                          :text    => @text}
        mg_client.send_message 'clients.payers.net', message_params
      
    end

    def new_deposit
   
        @type = "new_deposit"
        @params = {:text => @text , :operation => @subject}
        ac = ActionController::Base.new()    
        mg_client = Mailgun::Client.new 'key-1173f0da990305a7bcf06f4d2ef45d09'
        message_params = {:from    => @from,
                        :to      => @user_mail,
                        :subject => @subject,
                        :html    =>  ac.render_to_string("user_mailer/#{@type}.html", locals:  { :params => @params })}
        mg_client.send_message 'clients.payers.net', message_params

    end

    def transfer_money
   
        @type = "transfer_money"
        @params = {:text => @text}
        ac = ActionController::Base.new()    
        mg_client = Mailgun::Client.new 'key-1173f0da990305a7bcf06f4d2ef45d09'
        message_params = {:from    => @from,
                        :to      => @user_mail,
                        :subject => @subject,
                        :html    =>  ac.render_to_string("user_mailer/#{@type}.html", locals:  { :params => @params })}
        mg_client.send_message 'clients.payers.net', message_params

    end

    def confirmsignin
       
        @subject = 'Please Confirm your account'
        @type = "confirmsignin"
        @params = {:token => @text,:email => @user_mail}
        ac = ActionController::Base.new()    
        mg_client = Mailgun::Client.new 'key-1173f0da990305a7bcf06f4d2ef45d09'
        message_params = {:from    => @from,
                        :to      => @user_mail,
                        :subject => @subject,
                        :html    =>  ac.render_to_string("uploads/testmail.html", locals:  { :params => @params })}
        mg_client.send_message 'clients.payers.net', message_params
    end

    def confirmmail
       
        @subject = 'Please Confirm your account'
        @type = "confirmmail"
        @params = {:token => @text,:email => @user_mail.email,:id => @user_mail.id}
        ac = ActionController::Base.new()
        mg_client = Mailgun::Client.new 'key-1173f0da990305a7bcf06f4d2ef45d09'
        message_params = {:from    => @from,
                        :to      => @user_mail.email,
                        :subject => @subject,
                        :html    =>  ac.render_to_string("user_mailer/#{@type}.html", locals:  { :params => @params })}
        mg_client.send_message 'clients.payers.net', message_params

    end

    def invitation
       
        @subject = 'Invitation to payers'
        @type = "invitation"
        @invited_by = @user_mail.firstname + " " + @user_mail.lastname
        @params = {:invited_by => @invited_by, :email => @text, :invitation_code => @user_mail.invitation_code}
        ac = ActionController::Base.new()
        mg_client = Mailgun::Client.new 'key-1173f0da990305a7bcf06f4d2ef45d09'
        message_params = {:from    => @from,
                        :to      => @text,
                        :subject => @subject,
                        :html    =>  ac.render_to_string("user_mailer/#{@type}.html", locals:  { :params => @params })}
        mg_client.send_message 'clients.payers.net', message_params

    end

    def unlockaccount
       
        @subject = 'Your account has been locked'
        @type = "unlockaccount"
        @params = {:email => @user_mail.email, :token => @text, :id => @user_mail.id}
        ac = ActionController::Base.new()
        mg_client = Mailgun::Client.new 'key-1173f0da990305a7bcf06f4d2ef45d09'
        message_params = {:from    => @from,
                        :to      => @user_mail.email,
                        :subject => @subject,
                        :html    =>  ac.render_to_string("user_mailer/#{@type}.html", locals:  { :params => @params })}
        mg_client.send_message 'clients.payers.net', message_params

    end

    def differentip
       
        @subject = 'sign in from different ip address'
        @type = "differentip"
        @params = {:email => @user_mail.email, :ip => @text}
        ac = ActionController::Base.new()
        mg_client = Mailgun::Client.new 'key-1173f0da990305a7bcf06f4d2ef45d09'
        message_params = {:from    => @from,
                        :to      => @user_mail.email,
                        :subject => @subject,
                        :html    =>  ac.render_to_string("user_mailer/#{@type}.html", locals:  { :params => @params })}
        mg_client.send_message 'clients.payers.net', message_params

    end

end