# app/jobs/auditlog_job.rb
class AuditlogJob
    include SuckerPunch::Job
  
    # The perform method is in charge of our code execution when enqueued.
    def perform(params)
        AuditLog.create(user_id: params[:user_id], user_name: params[:user_name], action_type: params[:action_type], action_meta: params[:action_meta], ip: params[:ip])
    end
  
end