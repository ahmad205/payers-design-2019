require 'rake'

# task :cron => :environment do
 
#     Cart.cornjob

# end





namespace :cron do
  desc "TODO"

  task test:  :environment do
    @unconfirmed_users = User.where("status =? ",0).all
    @timeago = (Time.now - 15.to_i.minutes)
    @unconfirmed_users.all.each do |user|       
      @user_created = user.created_at
        if @user_created < @timeago
           @user_log = Watchdog.where("user_id = ?",user.id).all
           if @user_log
              @user_log.all.each do |log|
               log.destroy
              end
           end
           @invitation_record = AffilateProgram.where("user_id =?" ,user.id).first
           if @invitation_record
                @invitation_record.destroy
           end
           @user_verification = Verification.where("user_id =?" ,user.id).first
           if @user_verification
              @user_verification.destroy
           end
           user.destroy 
        end
    end
  end 

  task wallets_transfers:  :environment do
    @frozen_operations = UsersWalletsTransfer.where(approve: 0).all

    @frozen_operations.all.each do |operation|
      @confirm_time = operation.created_at + operation.hold_period.days

      if Time.now.getutc.strftime("%m/%d/%Y %I:%M %p")  >= @confirm_time.strftime("%m/%d/%Y %I:%M %p")
        @user_wallet_to = UserWallet.where(user_id: operation.user_to_id.to_i).first
        @money_op = MoneyOp.where(opid: operation.operation_id).first
        @transfer_amount = operation.amount.to_f
        @new_balance_to = @user_wallet_to.amount.to_f + @transfer_amount
        @user_wallet_to.update(:amount => @new_balance_to )
        operation.update(:approve => 1 , :service_status => "Completed")
        @money_op.update(:status => 1)
        @user = User.where("id =?", operation.user_to_id.to_i).first
        @sender_user = User.where("id =?", operation.user_id.to_i).first
        @sender_user_name = @sender_user.firstname + " " + @sender_user.lastname
        @tel = @user.telephone
        @user_mail = @user.email
        @smstext = "#{@transfer_amount} USD has been successfully added to your wallet by #{@sender_user_name} after the holding period"
        @smstext2 = "#{@transfer_amount} USD has been successfully transferred to user #{@user.firstname} #{@user.lastname} after the holding period"
        @user_notification_setting_sender = NotificationsSetting.where(user_id: @sender_user.id).first
        @user_notification_setting = NotificationsSetting.where(user_id: @user.id).first
        Notification.create([{user_id: @user.id ,title: "Balance transfer", description: @smstext , notification_type: @user_notification_setting.money_transactions},
        {user_id: @sender_user.id ,title: "Balance transfer", description: @smstext2 , notification_type: @user_notification_setting_sender.money_transactions}])
        if @user_notification_setting.money_transactions == 3
          SMSNotification.sms_notification_setting(@tel,@smstext)
          SmsLog.create(:user_id => @user.id, :pinid => @smstext,:status => 1,:sms_type => 3)
          EmailNotification.email_notification_setting(user_mail:@user_mail,subject:'wallet transfer balance',text:@smstext)
        elsif @user_notification_setting.money_transactions == 2
          SMSNotification.sms_notification_setting(@tel,@smstext)
          SmsLog.create(:user_id => @user.id, :pinid => @smstext,:status => 1,:sms_type => 3)
        elsif @user_notification_setting.money_transactions == 1
          EmailNotification.email_notification_setting(user_mail:@user_mail,subject:'wallet transfer balance',text:@smstext)
        end

        if @user_notification_setting_sender.money_transactions == 3
          SMSNotification.sms_notification_setting(@sender_user.telephone,@smstext2)
          SmsLog.create(:user_id => @sender_user.id, :pinid => @smstext2,:status => 1,:sms_type => 3)
          EmailNotification.email_notification_setting(user_mail:@sender_user.email,subject:'wallet transfer balance',text:@smstext2)
        elsif @user_notification_setting_sender.money_transactions == 2
          SMSNotification.sms_notification_setting(@sender_user.telephone,@smstext2)
          SmsLog.create(:user_id => @sender_user.id, :pinid => @smstext2,:status => 1,:sms_type => 3)
        elsif @user_notification_setting_sender.money_transactions == 1
          EmailNotification.email_notification_setting(user_mail:@sender_user.email,subject:'wallet transfer balance',text:@smstext2)
        end

      end
    end
  end

  task close_tickets:  :environment do
    @ticket_max_days = WalletsTransferRatio.where(key: "ticket_max_days").pluck(:value).first
    @tickets = Ticket.where("status =?",2).all

    @tickets.all.each do |ticket|
      @period = Time.now.getutc.day - ticket.updated_at.day
      if @period >= @ticket_max_days.to_i
        ticket.update(:status => 3)
        @user = User.where("id =?", ticket.user_id.to_i).first
        @tel = @user.telephone
        @user_mail = @user.email
        @smstext = "Your ticket with number #{ticket.number} has been automatically closed by payers system"
        @user_notification_setting = NotificationsSetting.where(user_id: @user.id).first
        Notification.create(user_id: @user.id ,title: "Tickets Status Updates", description: @smstext , notification_type: @user_notification_setting.help_tickets_updates)
        if @user_notification_setting.help_tickets_updates == 3
          SMSNotification.sms_notification_setting(@tel,@smstext)
          SmsLog.create(:user_id => @user.id, :pinid => @smstext,:status => 1,:sms_type => 3)
          EmailNotification.email_notification_setting(user_mail:@user_mail,subject:'Tickets Status Updates',text:@smstext)
        elsif @user_notification_setting.help_tickets_updates == 2
          SMSNotification.sms_notification_setting(@tel,@smstext)
          SmsLog.create(:user_id => @user.id, :pinid => @smstext,:status => 1,:sms_type => 3)
        elsif @user_notification_setting.help_tickets_updates == 1
          EmailNotification.email_notification_setting(user_mail:@user_mail,subject:'Tickets Status Updates',text:@smstext)
        end

      end
    end
  end

  task ticket_evaluation:  :environment do
    @tickets = Ticket.where("status =?",3).all

    @tickets.all.each do |ticket|
      @period = Time.now.getutc.day - ticket.updated_at.day
      if @period >= 1 && ticket.evaluation == nil
        @user = User.where("id =?", ticket.user_id.to_i).first
        @tel = @user.telephone
        @user_mail = @user.email
        @smstext = "Please help us to improve our service by evaluating your ticket with number #{ticket.number}"
        @user_notification_setting = NotificationsSetting.where(user_id: @user.id).first
        Notification.create(user_id: @user.id ,title: "Tickets Status Updates", description: @smstext , notification_type: @user_notification_setting.help_tickets_updates)
        if @user_notification_setting.help_tickets_updates == 3
          SMSNotification.sms_notification_setting(@tel,@smstext)
          SmsLog.create(:user_id => @user.id, :pinid => @smstext,:status => 1,:sms_type => 3)
          EmailNotification.email_notification_setting(user_mail:@user_mail,subject:'Tickets Status Updates',text:@smstext)
        elsif @user_notification_setting.help_tickets_updates == 2
          SMSNotification.sms_notification_setting(@tel,@smstext)
          SmsLog.create(:user_id => @user.id, :pinid => @smstext,:status => 1,:sms_type => 3)
        elsif @user_notification_setting.help_tickets_updates == 1
          EmailNotification.email_notification_setting(user_mail:@user_mail,subject:'Tickets Status Updates',text:@smstext)
        end

      end
    end
  end

end

