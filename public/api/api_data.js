define({ "api": [
  {
    "type": "post",
    "url": "/affilate_programs/",
    "title": "Send Invitation Email",
    "version": "0.3.0",
    "name": "postAffilateProgram",
    "group": "AffilateProgram",
    "description": "<p>Send Invitation Email.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Email of the invited user.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:       ",
        "content": "curl -X POST  http://localhost:3000/api/v1/send_invitation_email -H 'cache-control: no-cache' -H 'content-type: application/json' -H \"Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4\" -d '{\"email\": \"engamira333@gmail.com\"}'",
        "type": "json"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/ 200 OK\n {\n   \"result\": \"true\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>Only authenticated users can send invitation.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingData",
            "description": "<p>Email can not be empty.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 400 Bad Request\n {\n   \"result : false\"\n   \"error\": \"Not Authenticated\"\n   \"status\": 400- Bad Request\n}",
          "type": "json"
        },
        {
          "title": "Error-Response2:",
          "content": " HTTP/1.1 400 Bad Request\n {\n   \"result : false\"\n   \"error\": \"MissingData\"\n   \"status\": 400- Bad Request\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/affilate_programs_controller.rb",
    "groupTitle": "AffilateProgram"
  },
  {
    "type": "post",
    "url": "/authentication/",
    "title": "authenticate User",
    "version": "0.3.0",
    "name": "AuthenticateUser",
    "group": "Authentication",
    "description": "<p>User Authenticate.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User's Email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User's Password</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage: ",
        "content": "curl -X POST -d email=\"engamira333@gmail.com\" -d password=\"Amira123456\" http://localhost:3000/api/v1/auth_user",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>User's-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User's email.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "auth_token",
            "description": "<p>User's authentication token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/ 200 OK\n {\n   \"success\": \"200 ok\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Invalid Email/Password.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 400 Bad Request\n {\n   \"error\": \"Invalid Email/Password\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/authentication_controller.rb",
    "groupTitle": "Authentication"
  },
  {
    "type": "post",
    "url": "/unlockaccount/",
    "title": "unlock user account",
    "version": "0.3.0",
    "name": "unlockaccount",
    "group": "Authentication",
    "description": "<p>UnLock account after Number of failed attempts.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User's Email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "token",
            "description": "<p>User's token to unlock account</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage: ",
        "content": "curl -X POST -d email=\"engamira333@gmail.com\" -d token=\"65f152a6\" http://localhost:3000/api/v1/unlockaccount",
        "type": "json"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/ 200 OK\n {\n   \"success\": \"200 ok\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "required_parameter_missing",
            "description": "<p>wrong email.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "required_parameter_missing2",
            "description": "<p>wrong code.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 400 Bad Request\n {\n   \"error\": \"Invalid Email\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 400 Bad Request\n {\n   \"error\": \"Invalid Token\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/authentication_controller.rb",
    "groupTitle": "Authentication"
  },
  {
    "type": "delete",
    "url": "/countries/:id",
    "title": "Delete Country",
    "version": "0.3.0",
    "name": "DeleteCountry",
    "group": "Countries",
    "permission": [
      {
        "name": "admin"
      }
    ],
    "description": "<p>Delete a Country.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X DELETE http://localhost:3000/api/v1/countries/2 -H \"Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4\"",
        "type": "json"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/ 200 Success\n {\n   \"success\": \"200 ok\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only  authenticated admins can delete the country.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CountryNotFound",
            "description": "<p>The <code>id</code> of the Country was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Not Authenticated\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 404 Not Found\n {\n   \"error\": \"CountryNotFound\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/countries_controller.rb",
    "groupTitle": "Countries"
  },
  {
    "type": "get",
    "url": "/api/v1/countries",
    "title": "List all countries",
    "version": "0.3.0",
    "name": "GetCountries",
    "group": "Countries",
    "description": "<p>get details of all countries.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -H \"Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4\" -i http://localhost:3000/api/v1/countries",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Country-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "short_code",
            "description": "<p>Country Code.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Full_Name",
            "description": "<p>Country Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Phone_code",
            "description": "<p>Country Phone Code.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Currency",
            "description": "<p>Country Currency.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "language",
            "description": "<p>Country Language.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "active",
            "description": "<p>Country Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date of creating the Country.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date of updating the Country.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can access the data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Unauthorized\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/countries_controller.rb",
    "groupTitle": "Countries"
  },
  {
    "type": "get",
    "url": "/api/countries/{:id}",
    "title": "Get Country Data",
    "version": "0.3.0",
    "name": "GetCountry",
    "group": "Countries",
    "description": "<p>get details of specific country.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -H \"Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4\" -i http://localhost:3000/api/v1/countries/1",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Country-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "short_code",
            "description": "<p>Country Code.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Full_Name",
            "description": "<p>Country Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Phone_code",
            "description": "<p>Country Phone Code.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Currency",
            "description": "<p>Country Currency.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "language",
            "description": "<p>Country Language.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "active",
            "description": "<p>Country Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date of creating the Country.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date of updating the Country.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can access the data.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CountryNotFound",
            "description": "<p>The <code>id</code> of the Country was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Unauthorized\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 404 Not Found\n {\n   \"error\": \"CountryNotFound\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/countries_controller.rb",
    "groupTitle": "Countries"
  },
  {
    "type": "put",
    "url": "/countries/:id/edit",
    "title": "Update Country",
    "version": "0.3.0",
    "name": "PutCountry",
    "group": "Countries",
    "permission": [
      {
        "name": "admin"
      }
    ],
    "description": "<p>update data of a Country.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "short_code",
            "description": "<p>Country Code.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Full_Name",
            "description": "<p>Country Name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Phone_code",
            "description": "<p>Country Phone Code.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Currency",
            "description": "<p>Country Currency.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "language",
            "description": "<p>Country Language.</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "active",
            "description": "<p>Country Status.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X PUT http://localhost:3000/api/v1/countries/1 -H \"Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4\" -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{ \"short_code\": \"test edit\"}'",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Country-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "short_code",
            "description": "<p>Country Code.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Full_Name",
            "description": "<p>Country Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Phone_code",
            "description": "<p>Country Phone Code.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Currency",
            "description": "<p>Country Currency.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "language",
            "description": "<p>Country Language.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "active",
            "description": "<p>Country Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date of creating the Country.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date of updating the Country.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated admins can update the data.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "CountryNotFound",
            "description": "<p>The <code>id</code> of the Country was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 404 Not Found\n {\n   \"error\": \"CountryNotFound\"\n}",
          "type": "json"
        },
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Not Authenticated\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/countries_controller.rb",
    "groupTitle": "Countries"
  },
  {
    "type": "post",
    "url": "/countries/",
    "title": "Create new Country",
    "version": "0.3.0",
    "name": "postCountry",
    "group": "Countries",
    "permission": [
      {
        "name": "admin"
      }
    ],
    "description": "<p>create new Country.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "short_code",
            "description": "<p>Country Code.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Full_Name",
            "description": "<p>Country Name.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Phone_code",
            "description": "<p>Country Phone Code.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "Currency",
            "description": "<p>Country Currency.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "language",
            "description": "<p>Country Language.</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "active",
            "description": "<p>Country Status.</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:       ",
        "content": "curl -X POST  http://localhost:3000/api/v1/countries -H \"Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4\" -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{\"country\": {\"short_code\": \"SAU\",\"Full_Name\": \"Saudi Arabia\t\",\"Phone_code\": \"00966\", \"Currency\": \"SAR\", \"language\": \"arabic\",\"active\": 1}}'",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Country-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "short_code",
            "description": "<p>Country Code.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Full_Name",
            "description": "<p>Country Name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Phone_code",
            "description": "<p>Country Phone Code.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Currency",
            "description": "<p>Country Currency.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "language",
            "description": "<p>Country Language.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "active",
            "description": "<p>Country Status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date of creating the Country.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date of updating the Country.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/ 200 OK\n {\n   \"success\": \"200 ok\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated admin can create Countries.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid-token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 400 Bad Request\n {\n   \"error\": \"Missing Token\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/countries_controller.rb",
    "groupTitle": "Countries"
  },
  {
    "type": "get",
    "url": "/api/v1/active_sessions?id={:user-id}",
    "title": "show user's active sessions",
    "version": "0.3.0",
    "name": "Getactivesessions",
    "group": "Log",
    "description": "<p>show user's active sessions.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/active_sessions?id=1 -H \"Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjo5fQ.xM_NrnhjjJejtZFatkVZWAZS_BFxvtjBns50IdcakzU\"",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Users-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "ipaddress",
            "description": "<p>Ip address</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "user_agent",
            "description": "<p>Session Details\t.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "device_id",
            "description": "<p>device id</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date of creating the session.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date of updating the session.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can perform this action.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Unauthorized\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/logins_controller.rb",
    "groupTitle": "Log"
  },
  {
    "type": "get",
    "url": "/api/v1/user_log?id={:user-id}",
    "title": "show user log",
    "version": "0.3.0",
    "name": "Getuserlog",
    "group": "Log",
    "description": "<p>show user log.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -i http://localhost:3000/api/v1/user_log?id=1",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Users-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "ipaddress",
            "description": "<p>Ip address</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "operation_type",
            "description": "<p>Details\t.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date of creating the Log.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date of updating the Log.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can perform this action.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Unauthorized\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/watchdogs_controller.rb",
    "groupTitle": "Log"
  },
  {
    "type": "get",
    "url": "/api/v1/lock_unlock_account?id={:user-id}",
    "title": "change account status",
    "version": "0.3.0",
    "name": "GetAccountStatus",
    "group": "User",
    "description": "<p>change account status for user.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -H \"Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4\" -i http://localhost:3000/api/v1/lock_unlock_account?id=1",
        "type": "json"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n\"mseeage\": \"user has been enabled/disabled\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can perform this action.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Unauthorized\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/users_controller.rb",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/api/users/{:id}",
    "title": "Get User Data",
    "version": "0.3.0",
    "name": "GetUser",
    "group": "User",
    "description": "<p>get details of specific user.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -H \"Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4\" -i http://localhost:3000/api/v1/users/1",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Users-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Users username.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Users firstname.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Users lastname.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Users email.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "country_id",
            "description": "<p>Users country.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>date of creating the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>date of updating the User.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can access the data.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFound",
            "description": "<p>The <code>id</code> of the User was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Unauthorized\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        },
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 404 Not Found\n {\n   \"error\": \"UserNotFound\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/users_controller.rb",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/api/v1/users",
    "title": "List all users",
    "version": "0.3.0",
    "name": "GetUsers",
    "group": "User",
    "description": "<p>get details of all users.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -H \"Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMn0.HjzZehfkPaDES35pXYwdlXNydDMWhwayCjMBs5KUcOE\" -i http://localhost:3000/api/v1/users",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>User's-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "uuid",
            "description": "<p>User's-uuid.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "account_number",
            "description": "<p>User's account_number.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "telephone",
            "description": "<p>User's telephone.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "roleid",
            "description": "<p>User's role id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>User's username.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>User's first name.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>User's last name.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "disabled",
            "description": "<p>User's account status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "status",
            "description": "<p>Email confirmation status.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "loginattempts",
            "description": "<p>User's login attempts.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "failedattempts",
            "description": "<p>User's failed attempts.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "account_currency",
            "description": "<p>User's account currency.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "country_id",
            "description": "<p>User's country id.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User's email.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>date of creating the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>date of updating the User.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n  {\n\"id\": 1,\n \"uuid\": \"8dddd2c5\",\n \"roleid\": 1,\n \"username\": \"amira\",\n \"firstname\": \"amira\",\n \"lastname\": \"amira2\",\n \"disabled\": 1,\n \"loginattempts\": 7,\n \"status\": 1,\n \"created_at\": \"2018-07-30T13:19:38.031Z\",\n \"updated_at\": \"2018-08-02T13:02:55.156Z\",\n \"email\": \"amira.elfayhhome@servicehigh.com\",\n \"failedattempts\": 0,\n \"account_number\": \"PS100000001\",\n \"telephone\": \"00201007460059\",\n \"account_currency\": 0,\n \"country_id\": 1\n},\n{\n\"id\": 2,\n \"uuid\": \"dd89adb1\",\n \"roleid\": 1,\n \"username\": \"amira2\",\n \"firstname\": \"amira2\",\n \"lastname\": \"amira2\",\n \"disabled\": 1,\n \"loginattempts\": 0,\n \"status\": 0,\n \"created_at\": \"2018-07-30T13:19:58.031Z\",\n \"updated_at\": \"2018-08-02T13:02:55.156Z\",\n \"email\": \"amira.elfayhhome@servicehigh.com\",\n \"failedattempts\": 0,\n \"account_number\": \"PS100000002\",\n \"telephone\": \"0020102982852\",\n \"account_currency\": 0,\n \"country_id\": 1\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can access the data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Unauthorized\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/users_controller.rb",
    "groupTitle": "User"
  },
  {
    "type": "get",
    "url": "/api/v1/statistics?&account_status={:search_status}",
    "title": "Get Users Statistics",
    "version": "0.3.0",
    "name": "GetUsersstatistics",
    "group": "User",
    "description": "<p>get statistics of all users.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -H \"Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4\" -i 'http://localhost:3000/api/v1/statistics?&account_status=confirmed'",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Users-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Users username.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Users firstname.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Users lastname.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Users email.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "country_id",
            "description": "<p>Users country.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>date of creating the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>date of updating the User.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can access the data.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Unauthorized\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/users_controller.rb",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/api/v1/confirmmail",
    "title": "confirm user mail",
    "version": "0.3.0",
    "name": "POSTconfirmmail",
    "group": "User",
    "description": "<p>confirm user mail after registeration.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X POST  http://localhost:3000/api/v1/confirmmail -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{\"email\": \"engamira333@gmail.com\",\"token\": \"3aa4e716\"}'",
        "type": "json"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n  \"result : true\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "required_parameter_missing",
            "description": "<p>wrong code .</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "required_parameter_missing2",
            "description": "<p>wrong email .</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 400 Badrequest\n {\n   \"result : false\"\n   \"error\": \"MissingData\"\n   \"status\": 400- Bad Request\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/users_controller.rb",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/api/v1/resend_confirmmail",
    "title": "resend confirmation mail",
    "version": "0.3.0",
    "name": "POSTresend_confirmmail",
    "group": "User",
    "description": "<p>resend confirmation mail to user after registeration.</p>",
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X POST  http://localhost:3000/api/v1/resend_confirmmail -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{\"email\": \"engamira333@gmail.com\"}'",
        "type": "json"
      }
    ],
    "success": {
      "examples": [
        {
          "title": "Success-Response:",
          "content": "HTTP/1.1 200 OK\n{\n   \"result : true\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "required_parameter_missing",
            "description": "<p>wrong email .</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 400 Badrequest\n {\n   \"result : false\"\n   \"error\": \"MissingData\"\n   \"status\": 400- Bad Request\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/users_controller.rb",
    "groupTitle": "User"
  },
  {
    "type": "put",
    "url": "/users/:id/edit",
    "title": "Update User",
    "version": "0.3.0",
    "name": "PutUser",
    "group": "User",
    "description": "<p>update details of a user.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Users username.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Users firstname.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Users lastname.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Users email.</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "country_id",
            "description": "<p>Users country.</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "secret_code",
            "description": "<p>Users Secret Code</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "telephone",
            "description": "<p>Users telephone</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "account_currency",
            "description": "<p>Users currency</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "active_otp",
            "description": "<p>Users active two factor authentication</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:",
        "content": "curl -X PUT http://localhost:3000/api/v1/users/10 -H \"Authorization: eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxMX0.ikUiajq9UFTi4OJDDGR2Xqk79U5vtiRWJaa8HgneqC4\" -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{ \"email\": \"amira@ggggg.com\"}'",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Users-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Users username.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Users firstname.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Users lastname.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Users email.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "country_id",
            "description": "<p>Users country.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "secret_code",
            "description": "<p>Users Secret Code</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "telephone",
            "description": "<p>Users telephone</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "account_currency",
            "description": "<p>Users currency</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "active_otp",
            "description": "<p>Users active two factor authentication</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "created_at",
            "description": "<p>Date of creating the User.</p>"
          },
          {
            "group": "Success 200",
            "type": "Datetime",
            "optional": false,
            "field": "updated_at",
            "description": "<p>Date of updating the User.</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can update the data.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "UserNotFound",
            "description": "<p>The <code>id</code> of the User was not found.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 404 Not Found\n {\n   \"error\": \"UserNotFound\"\n}",
          "type": "json"
        },
        {
          "title": "Response (example):",
          "content": " HTTP/1.1 401 Not Authenticated\n {\n   \"error\": \"NoAccessRight\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/users_controller.rb",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "/users/",
    "title": "Create new User",
    "version": "0.3.0",
    "name": "postUser",
    "group": "User",
    "description": "<p>create new user.</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>User's First Name</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>User's Last Name</p>"
          },
          {
            "group": "Parameter",
            "type": "Integer",
            "optional": false,
            "field": "country_id",
            "description": "<p>User's Country</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>User's Email</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>User's Username</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "password",
            "description": "<p>User's Password</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "refered_by",
            "description": "<p>Invitation Code Of the Invitor</p>"
          }
        ]
      }
    },
    "examples": [
      {
        "title": "Example usage:       ",
        "content": "curl -X POST  http://localhost:3000/api/v1/users -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{\"user\": {\"firstname\": \"amira2\",\"lastname\": \"amira2\",\"password\": \"Amira123456\", \"country_id\": 1,\"email\": \"tessst@gmail.com\",\"username\": \"EngineeraAmira2\", \"refered_by\": \"71000e83\"}}'",
        "type": "json"
      }
    ],
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "id",
            "description": "<p>Users-ID.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "username",
            "description": "<p>Users username.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "firstname",
            "description": "<p>Users firstname.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "lastname",
            "description": "<p>Users lastname.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "email",
            "description": "<p>Users email.</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "country_id",
            "description": "<p>Users country.</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "secret_code",
            "description": "<p>Users Secret Code</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "telephone",
            "description": "<p>Users telephone</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "account_currency",
            "description": "<p>Users currency</p>"
          },
          {
            "group": "Success 200",
            "type": "Integer",
            "optional": false,
            "field": "active_otp",
            "description": "<p>Users active two factor authentication</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Response (example):",
          "content": " HTTP/ 200 OK\n {\n   \"success\": \"200 ok\"\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "NoAccessRight",
            "description": "<p>Only authenticated users can create User.</p>"
          },
          {
            "group": "Error 4xx",
            "optional": false,
            "field": "MissingToken",
            "description": "<p>invalid-token.</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": " HTTP/1.1 400 Bad Request\n {\n   \"error\": \"Missing Token\"\n}",
          "type": "json"
        }
      ]
    },
    "filename": "payers/app/controllers/api/v1/users_controller.rb",
    "groupTitle": "User"
  }
] });
