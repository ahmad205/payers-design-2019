
// User Menu Toggled

$(".user-menu .dropdown").click(function(){
  $(".user-menu .user-overlay").toggleClass('show');
  $("body").toggleClass("overflowbody");
});

$(window).click(function() {
  $(".user-menu .user-overlay").removeClass('show');
  $("body").removeClass("overflowbody");
});

// User Menu Toggled

$(".user-menu-mobile").click(function(){
  $("body").addClass("overflowbody-2");
  $(".mobile-user-menu").slideToggle();
});

$(".close-mobile-user-menu i").click(function(){
  $("body").removeClass("overflowbody-2");
  $(".mobile-user-menu").slideToggle();
});

$(".home-mobile-menu a").click(function(){
  $("body").addClass("overflowbody-2");
  $(".mobile-user-menu-2").slideToggle();
});
$(".close-mobile-user-menu-2 i").click(function(){
  $("body").removeClass("overflowbody-2");
  $(".mobile-user-menu-2").slideToggle();
});

// Notification Toggled

$('.notification-button , .close-notification-toggled').click(function() {
  event.stopPropagation();
  if($('.notification-list').hasClass('toggled-notification')) {
    $('.notification-list').removeClass('toggled-notification');
    $('.notification-overlay-2').css('visibility','hidden' , 'z-index' , '0' );
    $("body").removeClass("overflowbody-2");
  }else{
    $('.notification-list').addClass('toggled-notification');
    $('.notification-overlay-2').css('visibility','visible' , 'z-index' , '9' );
    $("body").addClass("overflowbody-2");
    if ($('.user-menu .dropdown').hasClass('open')) {
      $('.user-menu .dropdown').removeClass('open');
      $(".user-menu .user-overlay").removeClass('show');
    }
  }
});


// Flags

$('#select-c').flagStrap({
	countries: {
		"SA": "+966",
		"EG": "+2",
	},
	placeholder:{value:"",text:"Select country key:"},
	buttonSize: "btn-sm",
	buttonType: "btn-default",
	labelMargin: "10px",
	scrollable: false,
	scrollableHeight: "350px"
});
$('#select-c-2').flagStrap({
	countries: {
		"SA": "Kingdom of Saudi Arabia",
		"EG": "Egypt",
	},
	placeholder:{value:"",text:"Select Country"},
	buttonSize: "btn-sm",
	buttonType: "btn-default",
	labelMargin: "10px",
	scrollable: false,
	scrollableHeight: "350px"
});

// User Setting Collabs
$(".open-user-setting-sidebar").click(function() {
  $(this).toggleClass('active-sidebar');
  $('.user-setting-sidebar').slideToggle(100);
});

// Deposite Steps

$('.input-radio-group input').click(function () {
    $('.input-radio-group input:not(:checked)').parent().removeClass("selected-radio");
    $('.input-radio-group input:checked').parent().addClass("selected-radio");
}); 

// PayersWizard

function payersWizard(current){
  $("div[wizard]").show();
  $("div[wizard]").fadeTo("fast",1).addClass('current-steps');
  $("div[wizard]").css('z-index', 1).removeClass('c-steps');
  $("div[wizard]").not("div[wizard|='"+(current+1)+"'], div[wizard|='"+(current+2)+"']").hide().removeClass('current-steps');
  $("div[wizard|='"+(current+2)+"']").fadeTo("slow",0.3).removeClass('prev-button');
  $("div[wizard|='"+(current+2)+"']").css('z-index', 0).addClass('c-steps').removeClass('current-steps');
}
payersWizard(0);

$('.step-box').click(function(){
	if ($(".current-steps").find(".prev-step").length > 0 ) {
		$('.current-steps').addClass('prev-button');
	}
});

// Select Visa

$('.dropdown-menu li').click(function(){
  var currentOption = $('#selected-payment-method');
  currentOption.html($(this).html());
});

// Datepicker

$('.date').datepicker({
	language:'ar',
	format: "dd MM yyyy",
	todayHighlight: true
});

// Input File

$('input[type="file"]').change(function(){
  var label = $(this).parent().find('span');
  if(typeof(this.files) !='undefined'){ // fucking IE      
    if(this.files.length == 0){
      label.removeClass('withFile').text(label.data('default'));
    }
    else{
      var file = this.files[0]; 
      var name = file.name;
      var size = (file.size / 1048576).toFixed(3); //size in mb 
      label.addClass('withFile').text(name);
    }
  }
  else{
    var name = this.value.split("\\");
      label.addClass('withFile').text(name[name.length-1]);
  }
  return false;
});

// Height 100%

$('.all-height').css("min-height",$(document).height()+300);
