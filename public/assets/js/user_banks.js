$(document).ready(function() {
    $("#users_bank_bank_id").prop('required',true); 
    $("#users_bank_bank_id").change(function(){
      var option = $(this).find('option:selected').val();
        if ( option == "create a new bank")
          {
            $("#new_bank").show();
            $("#users_bank_bank_name").prop('required',true);
         }
        else
          {
            $("#new_bank").hide();
            $("#users_bank_bank_name").prop('required',false); 
          }
    });

    $('#add_acc').click(function()
      { 
        var form = $( "#add_new_bank" );             
        form.validate({
          rules: {
            accept_terms: {
              required: true
            }
          },
          //ignore: [],
          messages: {
          'users_bank[bank_id]': { required: "  البنك ( لا يمكن تركه فارغاً )"  },
          'users_bank[account_number]': { required: " رقم الحساب ( لا يمكن تركه فارغاً )"  },
          'users_bank[branch_name]': { required: " اسم الفرع ( لا يمكن تركه فارغاً )"  },
          'users_bank[bank_name]': { required: " اسم البنك ( لا يمكن تركه فارغاً )"  },
                },            
          errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
              $(placement).append(error)
              $(element).closest('div[class="inputstyle"]').addClass("inputstyle-span inputimportant");
              $(element).closest('div[class="inputstyle inputstyle-span selectstyle"]').addClass("inputimportant");
            }    
            else {
              error.insertAfter(element);
            }
          }            
        });
      });

  });