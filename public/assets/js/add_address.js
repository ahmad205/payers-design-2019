$('#submitaddress').click(function()
    {
        $( "#new_address" ).validate({
        rules: {
            accept_terms: {
            required: true,
            },
            'address[address]': {
            minlength: 5
            }
        },
        //ignore: [],
        messages: {
        'address[address]': { required:" <%= t('addresses.adress_can_not_be_left_empty') %>" ,
                                minlength:" <%= t('addresses.this_address_is_too_short_must_have_more_than_5_characters') %>"
            },
        'address[governorate]': { required:" <%= t('addresses.maintain_can_not_be_left_empty')%>" },
        'address[city]': { required: "<%= t('addresses.city_can_not_be_left_empty') %>" },
        'address[street]': { required: "<%= t('addresses.street_can_not_be_left_empty') %>" },
                },
        errorPlacement: function(error, element) {
            var placement = $(element).data('error');
            if (placement) {
            $(placement).append(error)
            $(element).closest('div[class="inputstyle"]').addClass("inputstyle-span inputimportant");
            }
            else {
            error.insertAfter(element);
            }
        }
        });
});
