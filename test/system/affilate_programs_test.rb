require "application_system_test_case"

class AffilateProgramsTest < ApplicationSystemTestCase
  setup do
    @affilate_program = affilate_programs(:one)
  end

  test "visiting the index" do
    visit affilate_programs_url
    assert_selector "h1", text: "Affilate Programs"
  end

  test "creating a Affilate program" do
    visit affilate_programs_url
    click_on "New Affilate Program"

    fill_in "Refered By", with: @affilate_program.refered_by
    fill_in "User", with: @affilate_program.user_id
    click_on "Create Affilate program"

    assert_text "Affilate program was successfully created"
    click_on "Back"
  end

  test "updating a Affilate program" do
    visit affilate_programs_url
    click_on "Edit", match: :first

    fill_in "Refered By", with: @affilate_program.refered_by
    fill_in "User", with: @affilate_program.user_id
    click_on "Update Affilate program"

    assert_text "Affilate program was successfully updated"
    click_on "Back"
  end

  test "destroying a Affilate program" do
    visit affilate_programs_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Affilate program was successfully destroyed"
  end
end
