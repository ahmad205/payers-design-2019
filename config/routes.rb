Rails.application.routes.draw do

  resources :errors_logs, :only => [:create]
  resources :settings
  resources :users_banks, :except => [:edit, :show]
  resources :banks, :only => [:create]
  resources :selfie_verifications, :only => [:create, :update]
  resources :address_verifications, :only => [:create, :update]
  resources :nationalid_verifications, :only => [:create, :update]
  resources :user_infos, :only => [:create]
  namespace :api, defaults: {format: 'json'} do
    namespace :v1 do
      resources :users_banks
      resources :watchdogs
      resources :users
      resources :countries
      resources :affilate_programs
      resources :selfie_verifications, :except => [:index, :destroy]
      resources :address_verifications, :except => [:index, :destroy]
      resources :nationalid_verifications, :except => [:index, :destroy]
      resources :translations
      post "users/create" => "users#create"
      get "homepage" => "users#homepage"
      post "confirmmail" => "users#confirmmail"
      post "resend_confirmmail" => "users#resend_confirmmail"
      get "two_factor" => "users#get_two_factor"
      post "two_factor" => "users#post_two_factor"
      get "send_confirmation_email" => "users#send_confirmation_email"
      get "generate_qr_code" => "users#generate_qr_code"
      get "confirm_google_code" => "users#confirm_google_code"
      get "user_log" => "watchdogs#user_log"
      get "active_sessions" => "logins#active_sessions"
      post 'auth_user' => 'authentication#authenticate_user'
      post "unlockaccount" => "authentication#unlockaccount"
      post "send_invitation_email" => "affilate_programs#send_invitation_email"
      post 'getEditToken' => 'authentication#getEditToken'
      # resources :cards
      #get "buy_card" => "cards#buy_card" 
      # resources :cards_categories
      get "display_cards_categories" => "cards_categories#display_cards_categories"
      resources :addresses, :except => [:new, :edit, :show]
      post "update_address" => "addresses#update_address"
      get "user_addresses" => "addresses#user_addresses"
      resources :user_wallets
      get "transfer_balance" => "user_wallets#transfer_balance"
      post "transfer_balance" => "user_wallets#transfer_balancepost"
      get "send_sms" => "user_wallets#send_sms"
      post "send_sms_post" => "user_wallets#send_sms_post"
      post "verifysms_post" => "user_wallets#verifysms_post"
      get "verifysms" => "user_wallets#verifysms" 
      get "user_wallet_balance" => "user_wallets#user_wallet_balance"
      resources :notifications
      resources :notifications_settings
      get "user_notifications" => "notifications#user_notifications"
      get "search_notifications" => "notifications#search"
      get "add_defualt_notifications" => "notifications_settings#add_defualt_notifications"
      resources :uploads
      resources :money_ops
      resources :ticket_departments
      resources :tickets, :except => [:new, :edit, :update]
      post 'tickets/reply' => 'tickets#create_reply'
      get "close_ticket" => "tickets#close_ticket"
      get "report_ticket" => "tickets#report_ticket"
      post "evaluate_ticket" => "tickets#evaluate_ticket"
      resources :payers_blogs
      get "users_blogs" => "payers_blogs#users_blogs"
      get "catblogs" => "payers_blogs#catblogs"
      get "display_blog" => "payers_blogs#display_blog"
      get "blogs_sitemap.xml" => "payers_blogs#sitemap", :format => "xml", :as => :sitemap
      get "payers_blogs_categories" => "payers_blogs#payers_blogs_categories"
      resources :settings
      get "mail_setting" => "settings#mail_setting"
      
    end
  end









  resources :affilate_programs, :only => [:create, :destroy]
  #resources :countries, :only => [:index, :show]
  resources :logins, :only => [:create, :destroy]
  resources :verifications, :only => [:create, :update, :destroy]
  resources :watchdogs, :only => [:create, :show, :destroy]
  resources :users, :except => [:index, :edit]
  # root "users#dashboard"
  root "users#homepage"
  get "confirmmail" => "users#confirmmail"
  get "unlockaccount" => "users#unlockaccount"
  get "two_factor" => "users#get_two_factor"
  post "two_factor" => "users#post_two_factor"
  get "resend_two_factor" => "users#resend_two_factor"
  get "dashboard" => "users#dashboard"
  get "destroy_all_sessions" => "clearance/sessions#destroy_all_sessions"
  get "destroy_one_session" => "clearance/sessions#destroy_one_session"
  
  match 'pass_to_security_settings', to: 'users#pass_to_security_settings', via: %i(get patch post)
  get "secure_my_account" => "users#secure_my_account"
  
  get "check_mail" => "users#check_mail"
  get "check_phone" => "users#check_phone"
  get "check_country_code" => "users#check_country_code"
  get "after_sign_up" => "users#after_sign_up"
  get "resend_confirmation_mail" => "users#resend_confirmation_mail"

  get "affiliate_program" => "affilate_programs#affiliate_program"
  get "affilate_records" => "affilate_programs#affilate_records"
  get "transfer_commission" => "affilate_programs#transfer_commission"
  


  



  get "homepage" => "users#homepage"
  get "activate_account" => "user_infos#activate_account"

  get "send2fasms" => "user_infos#send2fasms"
  get "send2favoice" => "user_infos#send2favoice"
  get "resendsms" => "user_infos#resendsms"
  get "verifysms_code" => "user_infos#verifysms_code"

  
  
  match 'reset_expired_password', to: 'users#reset_expired_password', via: %i(get patch post)

  get "user_log" => "watchdogs#user_log"
  get "active_sessions" => "logins#active_sessions"
  get "send_confirmation_email" => "users#send_confirmation_email"
  get "generate_qr_code" => "users#generate_qr_code"
  get "change_two_factor" => "users#change_two_factor"
  get "send_confirmation_mail" => "users#send_confirmation_mail"
  get "confirm_google_code" => "users#confirm_google_code"
  # get "set_otp_code" => "users#set_otp_code"
  get "affilate-program/:through/:refered_by" => "affilate_programs#set_invitation_code"
  post "send_invitation_email" => "affilate_programs#send_invitation_email"
  get "edit_password/:id" => "users#edit_password" , :as => "edit_password" 
  get "error_page" => "logins#error_page"
  get "error" => "errors_logs#error"
  get "lock_page" => "logins#lock_page"
  post "lock_page" => "logins#check_to_unlock"
  # resources :cards
  #get "buy_card" => "cards#buy_card"
  get "user_cards" => "cards#user_cards"
  get "remittance" => "cards#remittance"
  get "payment_method" => "cards#payment_method"
  get "payment_data" => "cards#payment_data"
  post "pay_operation" => "cards#pay_operation"
  get "paypalurl" => "cards#paypalurl"
  get "upload_payment_data" => "cards#upload_payment_data"
  post "upload_payment_data" => "cards#upload_payment_data"
  get "charging_card" => "cards#charging_card"
  post "charging_card" => "cards#charging_card_post"
  get "check_dosite" => "cards#check_dosite"
  
  
  
  # resources :cards_categories
  get "display_cards_categories" => "cards_categories#display_cards_categories"
  resources :addresses, :except => [:new, :edit, :show]
  post "update_address" => "addresses#update_address"
  get "user_addresses" => "addresses#user_addresses"
  resources :user_wallets
  get "transfer_balance" => "user_wallets#transfer_balance"
  post "transfer_balance" => "user_wallets#transfer_balancepost"
  get "send_sms" => "user_wallets#send_sms"
  post "send_sms_post" => "user_wallets#send_sms_post"
  post "verifysms_post" => "user_wallets#verifysms_post"
  get "verifysms" => "user_wallets#verifysms"
  get "user_wallet_balance" => "user_wallets#user_wallet_balance"
  get "confirm_transfer" => "user_wallets#confirm_transfer"
  get "refund_transfer" => "user_wallets#refund_transfer"
  get "check_receiver" => "user_wallets#check_receiver"
  resources :notifications
  resources :notifications_settings
  get "user_notifications" => "notifications#user_notifications"
  get "search_notifications" => "notifications#search"
  get "add_defualt_notifications" => "notifications_settings#add_defualt_notifications"
  get "user_notifications_setting" => "notifications_settings#user_notifications_setting"
  resources :uploads
  resources :money_ops
  get "paypal" => "money_ops#paypal"
  post "paypal_post" => "money_ops#paypal_post"
  get "paypalret" => "money_ops#paypalconfirm"
  post "paypal_ipn" => "money_ops#paypal_ipn"

  get "perfectmoney" => "money_ops#perfectmoney"
  post "perfectmoneyerror" => "money_ops#perfectmoneyerror"
  post "perfectmoney" => "money_ops#perfectmoneypost"

  post "call_coinmoney" => "money_ops#call_coinmoney"
  

  get "coinmoney" => "money_ops#coinmoney"
  post "coinmoney" => "money_ops#coinmoney_post"
  resources :users_wallets_transfers

  get "withdraw_op" => "money_ops#withdraw_op"
  post "withdraw_op" => "money_ops#withdrawpost"
  get "new_transaction"=>"money_ops#new_transaction"
  
  resources :ticket_departments
  resources :tickets, :except => [:new, :edit, :update]
  post 'tickets/reply' => 'tickets#create_reply'
  get "bank_data" => "money_ops#bank_data"
  get "close_ticket" => "tickets#close_ticket"
  get "report_ticket" => "tickets#report_ticket"
  post "evaluate_ticket" => "tickets#evaluate_ticket"
  
  resources :withdraws
  resources :audit_logs, :only => [:create]
  resources :quick_payments
  get "fast_pay/:slug" => "quick_payments#fast_pay"
  post "fast_pay" => "quick_payments#fast_pay_post"
  get "trust_user" => "quick_payments#trust_user"
  get "link_check" => "quick_payments#link_check"
  resources :user_announcements
  resources :blogs_categories
  resources :payers_blogs
  get "users_blogs" => "payers_blogs#users_blogs"
  get "catblogs" => "payers_blogs#catblogs"
  get "display_blog" => "payers_blogs#display_blog"
  get "blogs_sitemap.xml" => "payers_blogs#sitemap", :format => "xml", :as => :sitemap
  get "search_blog" => "payers_blogs#search_blog"
  post "search_blog_post" => "payers_blogs#search_blog_post"
  

  get "bank_details" => "users_banks#bank_details" 
  resources :translations
  get "change_locale" => "users#change_locale" 
  get "make_all_read" => "notifications#make_all_read"
  get "notification_read" => "notifications#notification_read"
  get "testmail" => "uploads#testmail"

  #match '*path', to: "logins#error_page", via: :all

  
  

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html


end
